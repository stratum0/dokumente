---
vim: ts=4 et sw=4 tw=90
build-me-with: make pdf
documentclass: s0minutes
papersize: a4
lang: de
toc: true
toc-depth: 2
links-as-notes: true
title: Mitgliederversammlung 2023-01-22
s0minutes:
  typeofmeeting: \generalassembly
  date: 2023-01-22
  place: "online; OpenSlides- und BigBlueButton-Instanzen des Stratum 0 e.V."
  startingtime: "14:15"
  endtime: "17:11"
  attendants: 36 akkreditierte ordentliche Mitglieder
  absentees:
  minutetaker: Chrissi\^, drc
---

\setcounter{section}{-3}
Vorbemerkungen
==============

Die Veranstaltung findet in einem Raum auf dem BigBlueButton-Videokonferenzssystem des
Vereins statt.
Abstimmungen werden über OpenSlides vorgenommen.
Beide Systeme werden auf Servern des Vereins von Mitgliedern betrieben und gewartet.

Zugänge zum OpenSlides wurden vor der Mitgliederversammlung an alle aktiven Mitgliedsentitäten
versendet.
Somit sind alle Mitgliedsentitäten, unabhängig von ihrer Stimmberechtigung, in der Lage, an der
Mitgliederversammlung teilnehmen.

Alle ordentlichen Mitgliedsentitäten wurden im Vorfeld der Mitgliederversammlung aufgefordert
eine aktuelle Post-Adresse abzugeben.
Diese Adressen werden später genutzt um Briefwahlunterlagen zu versenden.
Darüber hinaus haben alle ordentlichen Mitgliedsentitäten, die eine Adresse eingetragen haben
und deren Mitgliedskonten ausgeglichen sind, eine Stimmberechtigung im Openslides erhalten 
und können somit an Wahlen und Abstimmungen während der Mitgliederversammlung teilnehmen.

In diesem Protokoll wird bei jeder Wahl oder Abstimmung die Anzahl der
tatsächlich abgegebenen Stimmen angegeben.
Verlassen stimmberechtigte Mitgliedsentitäten die Versammlung oder kommen stimmberechtigte
hinzu, so verändert sich die Anzahl der möglichen Stimmen für weitere Abstimmungen.
Dazu kommt, dass stimmberechtigte Mitglieder eine Abstimmung aussetzen können ohne sich
explizit zu enthalten.

Technik-Test 
============

* Wir testen, ob alle Mitgliedsentitäten funktionierenden 
  Zugriff auf das OpenSlides haben.
* Dazu wird eine Test-Abstimmung im OpenSlides gestartet.
* Es stellt sich heraus, dass einzelne Personen Probleme haben
  die Tagesordnung zu sehen.
  Wir können diese Probleme lösen, indem die betroffenen Personen in ein
  "private Browsing"-Fenster wechseln.

|

* Beim Test-Antrag werden 34 Stimmen abgegeben:
    * 23 Ja 
    * 5 Nein
    * 6 Enthaltung

Protokoll Overhead
==================

Wir beginnen circa 14:15 mit diesem Teil der Mitgliederversammlung.
larsan (Lars Andresen) eröffnet als Vorsitzender des alten Vorstandes die Versammlung.

## Wahl der Versammlungsleitung

* larsan stellt sich zur Wahl.
* Es gibt keine weiteren Kandidaten.
* Es wird ein Wahlgang gestartet.

|

* Ergebnis des Wahlgangs:
    * Es werden 34 Stimmen abgegeben.
    * larsan erhält 34 Stimmen.
    * Bei dieser Wahl waren 36 stimmberechtige Mitglieder anwesend.

## Wahl der protokollführenden Entität

* Chrissi^ (Chris Fiege) stellt sich zur Wahl. 
  Chrissi^ freut sich aber über eine 2. Person freuen.
* drc (Philipp Specht) stellt sich ebenfalls zur Wahl.
* Es gibt keine weiteren Kandidaten.
* Ab diesem Wahlgang wird für alle weiteren Wahlen und Abstimmungen eine maximale Zeit 
  von 60 Sekunden eingesetzt.
  Nur in diesem Zeitfenster können Stimmen abgegeben werden.

|

* In diesem Wahlgang können zwei Stimmen (also für jede Person eine) abgegeben werden.
* Ergebnis des Wahlgangs:
    * 33 für Chrissi^
    * 33 für drc

## Zulassung von Gästen

* Bis hierher waren bei der MV keine Gäste anwesend.
* larsan beantragt die Zulassung von Gästen.
* Eine Ja/Nein/Enthaltung-Abstimmung wird im Openslides gestartet.

|

* Ergebnis des Wahlganges:
    * 32 dafür
    * 4 Enthaltungen

|

* Der Link zum BigBlueButton und zum Live-Protokoll wird im Matrix/IRC, sowie
  per E-Mail veröffentlicht.

## Wahl der Wahlleitung

* Die Aufgaben der Wahlleitung werden vorgestellt.
  Die Aufgaben sind: Moderation der Wahl im BBB, 
  Anfertigung und Versand der Briefwahlunterlagen,
  Auszählung und zuletzt die Kandidat*innen befragen, ob sie die Wahl annehmen.
* Zusätzlich kann die Wahlleitung Wahlhelfer*innen bestellen.
  Die Wahlhelfer werden direkt vor der Wahl in TOP 5 bestimmt.
* fiurin (Marie Mann) stellt sich zur Wahl.
* Es gibt keine weiteren Kandidaten.

|

* Ergebnis des Wahlgangs
    * fiurin erhält 35 von 37 Stimmen

## Feststellung der Beschlussfähigkeit

Chrissi^ berichtet:

* Aktuell hat der Verein 113 ordentliche Mitgliedsentitäten.
* Laut Satzung müssen 23% der ordentlichen Mitgliedsentitäten stimmberechtigt anwesend sein.
* Also reichen 26 stimmberechtigte ordentliche Mitgliedsentitäten, damit diese 
  MV beschlussfähig ist.
* Es haben 53 ordentliche Mitgliedsentitäten ihre Adresse eingetragen.
* Davon haben 52 ein ausgeglichenes Mitgliedskonto.
* Wir sollten bei relevanten Abstimmungen darauf achten, dass mindestens 26 
  stimmberechtige Mitgliedsentitäten abstimmen, um das Quorum zu erreichen.

Chrissi^ stellt die Beschlussfähigkeit der MV fest.

## Annahme der Geschäftsordnung

larsan schlägt vor, dass diese Mitgliederversammlung die Geschäftsordnung der 
letzten Mitgliederversammlung übernimmt.

Die Geschäftsordnung aus dem [Wiki](https://stratum0.org/mediawiki/index.php?title=Mitgliederversammlung/Gesch%C3%A4ftsordnung&oldid=27550) ist als Volltext in diesem Antrag enthalten.
Der Volltext lautet wie folgt:

		== §0 Wahlen ==
		1. Als Wahlmodus wird die Zustimmungswahl festgelegt.
		(Jede stimmberechtigte Entität kann für jede kandidierende Entität eine
		Stimme abgeben, Stimmenkumulation ist nicht möglich.)
		2. Es wird in geheimer Wahl auf Stimmzetteln gewählt.
		Dabei dürfen nur von der Versammlungsleitung genehmigte Stimmzettel
		genutzt werden.
		3. Die Kandidaten mit den meisten Stimmen sind gewählt, sofern sie
		mindestens die Stimmen von 50% der Stimmberechtigten erhalten haben,
		solange genug Posten für das entsprechende Amt zu besetzen sind.
		4. Falls mehrere Posten für ein Amt zu vergeben sind (z.B. Beisitzer,
		Rechnungsprüfer), findet die Besetzung absteigend nach Stimmenanzahl
		statt, bis alle Posten des entsprechenden Amtes besetzt sind.
		5. Falls sich durch Stimmengleichheit keine eindeutige Besetzung ergibt,
		findet eine Stichwahl zwischen den entsprechenden Kandidaten mit der
		gleichen Stimmenanzahl statt.
		Eine Nachwahl findet jeweils für die Posten Schatzmeister, stellv.
		Vorsitzender, oder Vorsitzender statt, falls das entsprechende Amt
		nicht besetzt wurde.
		6. Die Auswertung der Wahl erfolgt in der Reihenfolge Vorstandsvorsitzender,
		stellv. Vorsitzender, Schatzmeister, Beisitzer, Rechnungsprüfer.
		Kandidaten, die schon für ein Amt gewählt worden sind und es angenommen
		haben, werden bei der Auswertung der nachfolgenden Ämter nicht mehr
		berücksichtigt.
		7. Gewählte Kandidaten können von der Wahl zurücktreten.
		Die Annahme der Wahl oder der Rücktritt von der Wahl ist auch
		fernmündlich möglich.

* Niemand äußert den Wunsch einer Änderung an dieser GO.
* Es wird eine Abstimmung gestartet.

|

* Ergebnis der Abstimmung:
    * 33 Ja
    * 1 Enthaltung
    * 34 abgegebene Stimmen
    
* Die Geschäftsordnung wurde damit angenommen.


Berichte
========

## Finanzbericht

Der Finanzbericht wird von unserer Schatzmeisterin ktrask (Helena Schmidt) vorgestellt.
Zu diesem Finanzbericht gehören Folien, die im [Wiki](https://stratum0.org/wiki/Datei:Finanzbericht_Stratum_0_MV_Januar_2023.pdf) veröffentlicht wurden.

In diesem Protokoll werden nur die Anmerkungen zu den Folien wiedergegeben.
Die tatsächlichen Zahlen sind dem Finanzbericht zu entnehmen.

ktrask übernimmt das Wort.

#### Übersicht und Neues

|

* ktrask beginnt damit die aktuell genutzten Buchungskonten vorzustellen.

* Der Verein ist bereits mehr als 10 Jahre alt.
  Damit haben Unterlagen aus 2011 und 2012 die buchhalterische
  Aufbewahrungsfrist überschritten.
    * Personenbezogene Daten (Name und Zweck) wurden aus Buchungen von 2011 und 2012
      entfernt.
    * Wer schon so lange Mitglied ist kann dies auf seinem Mitgliedskontoauszug sehen.
    * Namen von Mitgliedern, die 2011 oder 2012 aus dem Verein ausgetreten sind
      wurden ebenfalls gelöscht.
* Die Matekasse soll nur noch digital genutzt werden.
  Diese ist im Space unter http://strichliste.s0 oder über das Tablet beim Kühlschrank
  zu erreichen.
* Steuererklärung für 2019 - 2022 wurde abgegeben und vom Finanzamt angenommen
  Wir sind damit weiterhin gemeinnützig \\o/ .
  
#### Spenden

|

* Es gab eine Spende über 1310 EUR von iserv, sonst kleinere Spenden von
  Privatpersonen
  und regelmäßige Spenden für Freifunk.

#### Gewinn- und Verlustrechnung

|

* Einnahmen geschehen über Mitgliedsbeiträge und Spenden.
* Der größte Ausgabeposten ist Miete.

|

* Wir haben seit 2023 eine neue Hausverwaltung.
  Wir gehen weiterhin davon aus schlecht nachvollziehbare Nebenkostenabrechnungen
  zu bekommen.
* Die Einnahmen in der Buchungsart "Miete / Nebenkosten" ist eine Rückzahlung
  für vorausgezahlte Abschläge für Storm.
  
|

* Wir machen keine Vermögensweraltung und führen keinen Zweckbetrieb.
  Daher sind die Buchungsarten leer.
  
|

* Matekasse weiterhin mit positivem Ergebnis
* Überweisung auf die Konten in der Strichliste sind möglich, aber umständlich:
     * Bei Bedarf Überweisung mit Betreff "Matekasse <Nickname>"
     * ktrask bucht das dann irgendwann in der Strichliste
     * Bitte den Betrag in der Strichliste **nicht** selber buchen
* Häufigste Quelle für Mankobuchugen in der Mate- und Erstattungskasse: Schreib-/ Lesefehler
    * Bei vermuteten Fehlern bitte an ktrask wenden und nicht versuchen, den Fehler
      selber zu korrigieren.
      
|

* Gesamtrechnung: Überschuss knapp 15k EUR
* Trend: Vereinskapital wächst (nicht stetig)
* Finanzsituation gut: Durchschnittliche Einnahmen ~2.6k EUR / Monat, Ausgaben ~1.1k EUR / Monat

### Mitgliederentwicklung
|

* Der Stratum 0 hat Stand 2023-01-17 127 Mitgliedsentitäten.
* ktrask vermutet, dass die Coronaflaute bei Neumitgliedschaften überwunden scheint.

### Rückstellungen

|

* Rückstellungen weiterhin für Zahlung der Miete falls Verein aufgelöst werden sollte.

### Allgemeines

|

* Betrachtungszeitraum geht bis zum 17.01.2023, nicht bis zum 15.01.2023, wie es während
  der MV auf den Folien stand.
* ktrask bietet an, weiterhin als Schatzmeisterin zur Verfügung zu stehen.

|

* Eine Einsicht in die Finanzdaten ist auch auf https://data.stratum0.org/finanz möglich.

|

* Amazon Smile wird eingestellt, entsprechende Spenden werden also entfallen.

## Bericht der Rechnungsprüfer

Shoragan (Jan Lübbe) und Sonnenschein (Angela Uschock) haben die Kasse geprüft.
Shoragan berichtet von den Prüfungen:

* Haben dieses Jahr zwei Zwischenprüfungen während des Jahres gemacht.
* Dazu kommt ein abschließender Termin circa zwei Wochen vor der MV.

|

* Bei den Prüfungen wurden keine groben Fehler gefunden.
* Keine Bedenken, die Entlastung wird empfohlen.

Weitere Ergänzungen der Kassenprüfer:

* Shoragan: Wir sollten nicht weiter Geld anhäufen; das wird sonst irgendwann zu einem Problem
  beim Finanzamt.
* Shoragan: Mitgliedsbeiträge sollten, wenn möglich im Quartal oder Halbjahr oder Jahr 
  überwiesen werden.
  Das spart Kontoführungsgebühren und Aufwand beim Prüfen.
* Angela: Gebt mehr Geld aus und macht tolle Projekte.
* Angela: Hat dem Vorstand den Vorschlag gemacht, eventuell eine Rückzahlung von Mitgliedsbeiträgen an die Mitglieder durchzuführen.


## Tätigkeitsbericht des Vorstandes

Der Tätigkeitsbericht wird von Tilman Stehr vorgestellt.
Zu diesem Tätigkeitsbericht gehören Folien, die im 
[Wiki](https://stratum0.org/wiki/Datei:Taetigkeitsbericht2022.pdf) 
veröffentlicht wurden.
In diesem Protokoll werden nur die Anmerkungen zu den Folien wiedergegeben.

Tilman übernimmt das Wort.

* Der Tätigkeitsbericht soll zeigen, was der Vorstand im letzten Jahr gemacht hat.
* Tilman beginnt von der MV bestellte Personen außerhalb des Vorstandes vorzustellen.
* Anschließend wird der Vorstand vorgestellt und aufgezeigt welche Vorstände welche
  Aufgaben übernehmen.

* Ergänzungen zu Folie 6: 
    * Der [Matrix-](https://matrix.to/#/#stratum0v:stratum0.org) bzw. 
      [IRC-Kanal](irc://irc.libera.chat/stratum0-v) #stratum0-v 
       ist öffentlich. 

      Dort geht es etwas fokussierter um Vereinsthemen als im allgemeinen Kanal.
    * Arbeitstreffen sind offen für alle.
    * Vorstandssitzungen sind generell öffentlich,
      wobei es einen nichtöffentlichen Teil gibt, bei dem es um Personen und Daten geht. 
* Ergänzung zu Folie 7: 
    * Ein Vergleich der Aktivitätszahlen ist nur bedingt möglich, 
      da coronabedingt Vorstandperioden unterschiedlich lang waren.
* Ergänzung zu Folie 8: 
    * Alle UBs werden in den Vorstandssitzungen noch einmal besprochen und dort in Person bestätigt.
* Ergänzung zu Folie 10: 
     * Die Inhaltsversicherung hat auch schon den vorherigen Vorstand beschäftigt. 
       Jetzt ist die Inhaltsversicherung aber auch abgeschlossen.
* Space 3.0
    * Wir hatten vor einigen Jahren von der Niebelungen Wohnbau ein Raum angeboten bekommen.
    * Wir haben da in diesem Jahr noch einmal nachgefragt. 
      Stellt sich aber raus: Die Nibelungen-Wohnbau baut das Gebäude jetzt nicht, sondern hat das abgegeben.
    * Der Vorstand fragen jetzt beim neuen Bauherren nach und schauen, ob sich dort was ergibt.
    
|

* Im Chat wird gefragt: Gibt es weitere Ideen für neue Räume?
    * larsan: Wir freuen uns über Vorschläge.
      Eventuell ergeben sich auch Räumlichkeiten im Schimmelhof?
    * Rohieb (Roland Hieber): Lohnt es sich eine Arbeitsgruppe dazu zu bilden?
    * larsan: Es gibt einen Matrix-Raum und auch ein Pad mit früheren Optionen:
        * https://pad.stratum0.org/p/space3
        * https://matrix.to/#/#space3:stratum0.org
        * irc://irc.libera.chat/stratum0-space3

## Jahresbericht

Der Jahresbericht wird von larsan vorgestellt.
Zu diesem Jahresbericht gehören Folien, die im 
[Wiki](https://stratum0.org/wiki/Datei:Jahresbericht2022.pdf) 
veröffentlicht wurden.

In diesem Protokoll werden nur die Anmerkungen zu den Folien wiedergegeben.

larsan übernimmt das Wort.

* Folie 2f:
    * Der Spaceöffnungsstatus erholt sich nach dem Einbruch während Corona langsam wieder.
    * Zeitweise war die Klingelanlage Ende 2022 defekt.
      Da war es ganz passend, dass im Space noch nicht wieder so viel los ist.
* Folie 4: 
     * Es gab 2022 9 Vortragsabende mit 19 Vorträgen. Einige Vorträge wurden von Externen gehalten.
     * Die Vorträge finden zunehmend auch wieder vor Ort statt.
     * Die Veranstaltung wird aber generell hybrid durchgeführt.
* Folie 5ff: Community-Events:
    * Es gab nach 5 Jahren wieder ein großes Sushi-Gelage. 
      Sehr spontan; aber trotzdem sehr gut besucht. 
      Und wir haben jetzt zwei Reiskocher.
    * GPN-Besuch mit Werbung für das HOA 2::22
    * HOA 2::22 (von uns veranstaltet, larsan zeigt Fotos); Planungen fürs nächste Jahr laufen
    * Halloweengeburtstagsfeier (larsan zeigt Fotos)
    * Hardwarespende: Es gab eine Firmenauflösung. Wir konnten dort viel Material mitnehmen.
      Ein Teil der Dinge steht im Space. Ein anderer Teil noch bei lichtfeind - der wühlt sich da langsam durch....
* Folie 18ff:
    * Es gab Spacebauabende mit dem Motto: Unser Space soll schöner werden.
      Für eine bessere Motivation zur Teilname gibt es dort nun eine Lebensmittelversorgung...
    * Das Resinlab in der "Toilette im Flur" nimmt Form an.
    * Es gab im Space ein Congressersatzprogramm:
      Es wurden Vorträge gehalten und Waffeln gebacken.
* Folie 22f:
    * Wir haben eine Shaper Origin angeschafft.
    * Diese wurde zu 1/4 durch Crowdfunding finanziert.
    * Das Gerät hat USB, Bilderkennung und WLAN. 
      Wir haben darin dann nach Aufschrauben ein Linux drin gefunden. 
      Nicht so GPLv2-konform, da Sourcen nicht öffentlich zugänglich sind. 
      Wir schreiben seitdem Mails mit dem Support hin und her. 
      Es wird besser; aber ist noch nicht so richtig, was wir erwarten würden.
* Folie 24ff:
    * Wir haben eine neue Hausverwaltung.
    * larsan rekapituliert die Geschichte der für uns zuständigen Hausverwaltungen...
* Folie 30: Ab hier werden Projekte vorstellt, die sich regelmäßig im Stratum treffen.
* Folie 31ff: Emantor (Rouven Czerwinski) stellt die Tätigkeit von Freifunk aus 
  dem letzten Jahr vor:
    * Freifunk hat Flüchtlingsunterkünfte mit WLAN ausgestattet.
         * Aktuell werden 3 Sporthallen betrieben.
         * Das Setup läuft jetzt seit >6 Monaten stabil.
    * Freifunk war wieder beim Fest der Kulturen am 1. Mai im Bürgerpark und 
      hat dort das Projekt vorgestellt und WLAN gemacht.
      Auch 2023 will Freifunk dort wieder dabei sein.
    * Rock im Rautheim: Es gab Rock und Metal und Freifunk.
    * Chrissi^ und Emantor waren bei Radio Okerwelle in der "Wunschkiste" und 
      haben dort das Projekt "Freifunk Braunschweig" vorgestellt. 
      Es gibt dazu einen [Blogpost](https://stratum0.org/blog/posts/2022/08/10/freifunk-okerwelle-wunschkiste/).
    * Es gibt wieder mehr Kontakt zur Stadt Braunschweig zum Besprechen möglicher Projekte.
      Mögliche Ideen sind:
        * Freifunk an allen BSVG-Haltestellen?
        * Freifunk an allen Spiel- und Jugendplätzen?
        * Freifunk im Hauptbahnhof?
        * Freifunk auf dem Skatepark Westand?
    * Neue Hardware: NUC mit Ryzen 5800
    * Haben Internet/WLAN auf dem HOA gemacht.
* Folie 46: Digital Courage
    * Trifft sich weiterhin alle 14 Tage, Donnerstags.
    * Am 28.01.2023 Stand auf dem Kohlmarkt zum Thema "Chatkontrolle".
* Folie 47:
    * Cyber taskforce zero: CTF-Team, Jannik berichtet
    * Neue CTF-Gruppe im Stratum 0
    * Reboot des Stratum 0 CTF-Teams
    * Beim Bambi CTF bereits mit 20 Personen dabei gewesen
    * Aktuell Platz 9 in Deutschland; 88 weltweit
    * Kontakt über https://matrix.to/#/#ctf_zero:stratum0.org
    * Weitere Personen willkommen
* Folie 48: Hey Alter 
    * Trifft sich jeden Donnerstag im Torhaus (und nicht im Space).
    * Aufbereitete Laptops mit Linux für SuS
    * Auch hier sind neue Leute/Mitarbeiter willkommen.
* Folie 49: Malkränzchen
    * Pecca berichtet
    * Wird jetzt 6 Jahre alt.
    * Jeden Mittwoch 18:00 Uhr im Space.
    * Kreative Projekte jeglicher Art.
    * Peccca würde gern möglichst viele Leute einladen. 
      Aber warnt davor, dass es aktuell häufig voll ist. 
      Man sollte also viele Menschen und etwas Lautstärke aushalten können.
* Folie 50: OpenStreetMap-Treffen
    * 2x in 2022
    * Eigentlich an jedem 17. des Monats.
    * Es gibt aber Stimmen, die das gern weiter machen wollen.
    * Mögliches Thema: Buslinien in BS mappen
    * Kontakt
         * https://matrix.to/#/#osm:stratum0.org
         * irc://irc.libera.chat/stratum0-osm
* Folie 51: Bierbrauen
    * Hat letztes Jahr 2x stattgefunden.
* Folie 52f:
    * larsan bedankt sich bei allen, die den Space irgendwie am Laufen halten:
      Einkaufen, Putzen, Aufräumen, ...
    * Und larsan bedankt sich bei allen, die Infrastruktur betreiben.
      Da sind aktuell so um die 15 Personen.
* Folie 54: Wünsche für 2023
    * "10"-Jahres-Feier nachholen?
    * Alte Projekte wieder reaktivieren, z.b. das CoderDojo
    * Mehr Blogposts über coole Projekte schreiben.
    
|

* Es kommt die Frage auf, wie es mit dem HOA-Gelände weitergeht.
    *  Aktuell sieht es so als, als wollte der aktuelle Betreiber das Gelände nicht weiter
       betreiben.
       Es stand die Option im Raum, dass das Gelände vom HOA übernommen werden könnte.
     * Es gibt zur Zeit keine finalen Informationen.
     * Es wird aber weiterhin daran gearbeitet.


## Bericht der Vertrauensperson

Es gibt keinen Bericht der Vertrauensperson, da die Vertrauensperson nicht anwesend ist.


# Entlastung des Vorstandes

* Shoragan beantragt die Entlastung des Vorstandes im Block.
* Es wird eine Abstimmung im OpenSlides vorberitet.
* Der Vorstand ist bei dieser Abstimmung nach [§34 GBG](https://www.gesetze-im-internet.de/bgb/__34.html) nicht stimmberechtigt.
* Niemand fordert die einzelne Entlastung des Vorstandes.
* Die Abstimmung im Opensilides wird gestartet.

|

* Ergebnis der Abstimmung: 
    * 31 Stimmen dafür
    * keine weiteren Stimmen abgegeben

|

* Es sind zu dieser Zeit 38 Personen im Openslides als "anwesend" gesetzt.
    * Chrissi weist auch noch darauf hin, dass es keine technische Maßnahme gibt, die das Abstimmen des Vorstands verhindern würde. 
      Wir sollten also auf ein ausreichendes Ergebnis achten.
    * Shoragan hat im OpenSlides die Stimmen überprüft: Es gab keine Stimmenabgabe durch den Vorstand.
    
|
    
* Der Vorstand ist somit entlastet.

# Änderungsanträge

Zum Zeitpunkt der Einladung zur MV lagen keine Änderungsanträge für die Satzung vor.
Es gibt keine weiteren Änderungsanträge für andere Dinge (z.B. die Beitragsordnung).

# Glaskugelei, Feedback und Input für den zukünftigen Vorstand

larsan fragt in die Runde, was sich die Mitglieder wünschen würden und gibt das Wort in die Runde.

* Rohieb: Findet die MV als Online-Veranstaltung gut.
    * Chrissi: Würden wir die Online-MV dann in die Satzung aufnehmen wollen?
    * larsan: Sieht keinen Hinderungsgrund in unserer Satzung, es online zu machen.
    * Rohieb: Man könnte es aufnehmen, um es für neue Mitglieder direkt offensichtlich zu machen
    * drc: Findet Hybrid-MVs eine nette Lösung
    * Rohieb: Hat das Gefühl, dass Hybrid zu einer schlechten Einbindung der Remote-Teilnehmer führt.
    * larsan: Reicht der Platz für Vor-Ort-MVs überhaupt noch?
    * Bluebrother: "Privatdiskussionen" vor Ort sind online nicht möglich.

|

* Jemand: Sollten wir uns um Glasfaser kümmern? Ausbau durch die Telekom steht bevor.

|

* Jannik: Findet die Spacebauabende zum Kennenlernen gut.

Es gibt keine weiteren Diskussionspunkte.

# Wahlen

Vor diesem TOP wird eine Pause von 16:05 bis 16:22 Uhr eingelegt.

fiurin (Marie Mann) übernimmt das Wort.

* Nach gemeinsamer Überlegung benennt fiurin die folgenden Wahlhelfer:
    * feya (Frauke Gellersen)
    * peace-maker (Jannik Hartung)
* Der Termin für die Auszählung wird auf den 05.02.2023 (15:00) festgelegt.
* larsan wird gebeten das Wahlverfahren zu erläutern:
    * Jetzt auf der MV werden die Kandidat*innen gesammelt.
    * Die Wahl selbst findet als Briefwahl statt.
    * Das Wahlverfahren ist Zustimmungswahl.
      Wobei eine Kandidat*in mindestens 50% der Stimmen erreichen muss.
    * Rohieb ergänzt:
      Man sollte jeder Person eine Stimme geben, die man sich in dem Amt vorstellen kann.

|

* Im Openslides werden Kandidatenlisten für die zu wählenden Ämter eröffnet.
  Nach einiger Zeit werden diese Listen geschlossen.
  Somit stehen folgende Kandidat*innen zur Wahl:
    * 1V
        * larsan (Lars Andresen)
        * (Keine weiteren Kandidaten)
    * 2V
        * Chrissi^ (Chris Fiege)
        * (Keine weiteren Kandidaten)
    * Schatzmeister*in
        * ktrask (Helena Schmidt)
        * (Keine weiteren Kandidaten)
    * Beisitzende Entitäten (3 Personen)
        * lf, lichtfeind (Jonas Martin)
        * fototeddy (Christopher Helberg)
        * mentalshirt (Tilman Stehr)
        * dadada (Tim Schubert)
        * simono41 (Simon Rieger)
        * NelienE (Nele Warneke)
        * marius (Marius Angelmann)
        * (Keine weiteren Kandidaten)
* Da die Rechnungsprüfer signalisiert haben, dass sie diese Aufgabe auch anderen Personen übergeben würden,
  wird für diese Rolle ebenfalls eine Kandidatenliste geöffnet.
    * Rechnungsprüfer*innen (2)
        * sonnenschein (Angela Uschock)
        * shoragan (Jan Lübbe)
        * Emantor (Rouven Czerwinski)
        * (Keine weiteren Kandidaten)

|

* Es wird jeder Kandidat*in das Wort für eine kurze Vorstellung übergeben:
    * **larsan:** Ist schon lange 1V und ist gerne bereit das Amt abzugeben; ist aber auch immer noch gerne Vorstand.
    * **Chrissi^:** Ist seit 2012 immer mal wieder im Vorstand. Macht im Vorstand die Protokolle, sonst HOA und Freifunk.
    * **ktrask:** Geldverwaltung seit 3 Jahren, automatisiert immer mehr. 
      Nächste Schritte: Mehr Buchungen z.b. bei der Matekasse automatisieren.
    * **lichtfeind**: Letztes Jahr beisitzende Entität. Hat im Vorstand Kontakt mit der Hausverwaltung gehalten.
    * **Fototeddy:** Macht jetzt seit zwei Jahren Vorstandsarbeit. 
      Ist im Space viel in der Werkstatt.
      Würde gern weiter machen.
    * **mentalshirt:** Beisitz im Vorstand seit 1 Jahr. Macht die HOA-Küche.
      Würde gern im Vorstand weiter machen 
      und kann sich auch vorstellen eines der Ämter mit weniger Kandidaten 
      zu übernehmen.
    * **dadada:** Macht beim HOA Öffentlichkeitsarbeit und Küche.
    * **Simono41:** Ist seit 1 Jahr Mitglied und seit 3 Jahren immer mal wieder da.
    * **NelienE:** (Die Vorstellung fällt aus, da ihr Mikrofon nicht geht.)
    * **Marius:** 18 Jahre und ist immer mal wieder im Space. 
      Verbringt gerade viel Zeit im Space und würde gern etwas zurück geben. 
      Hatte zuvor schon ein Amt als Schatzmeister bei einem anderen Verein und kann daher auf Erfahrungen zurückgreifen.
    * **sho:** Ist seit Anfang an Kassenprüfer. 
      Kümmert sich sonst um Administration des Vereins-Servers und ist jetzt wieder mehr im Space. 
      Kann sich vorstellen das Amt abzugeben, falls Emantor gewählt wird.
    * **sonnenschein:** War beim CoderDojo dabei. Ist schon länger Kassenprüferin.
    * **Emantor:** War mal Schatzmeister 2017–19 und musste damals auch eine Steuererklärung für den Verein machen.

|

* Der Versand der Briefwahlunterlagen wird von Wahlleiterin und Wahlhelfer*innen auf Montag 2023-01-23 gelegt.

# Sonstiges


## Terminfindung für die nächste MV

Als Termin wird der 21.01.2024 vorgeschlagen.
Dazu gibt es allgemeine Zustimmung.
Es gibt keine Gegenvorschläge.

## Weltherrschaft

larsan schlägt vor das Thema aufgrund der fortgeschrittenen Zeit auf die nächste MV zu vertagen.
Dazu gibt es allgemeine Zustimmung.

## Keysigning-Party

larsan schlägt vor die Keysigning-Party ebenfalls zu verschieben, da wir zur Zeit alle remote sind.
Dazu gibt es allgemeine Zustimmung.

## Sonstiges Sonstiges

larsan fragt, ob es noch weitere Themen gibt, die wir hier ansprechen sollten.

* Zoyo fragt, wie es mit den Mitgliedsbeiträgen weiter geht, wenn wir aktuell zu viel Geld einnehmen.
    * larsan: Findet anpassen von Mitgliedsbeiträgen schwierig, da das viel Aufwand bei allen erzeugt.
    * Zoyo: Für den Space ist das ständige Kaufen von Dingen aber kein Selbstzweck.
    * dadada schlägt eine langfristige Ausgabestrategie vor: 
      Mehr Räume anmieten oder einen Space 3 finden
      Die Beiträge sollten erst dann gesenkt werden, wenn der Finanzbedarf dafür bekannt ist.
    * chrissi^: Mitgliedsbeiträge anpassen ist mittelfristig möglich. Eventuell sind Rückzahlungen aber praktikabler?
    * Tilman schließt sich dadada an: 
      Erstmal mehr Räumlichkeiten finden, z.b. einen Lagerraum um die coolen Dinge unterzubringen. 
      Und wenn neue Räume finden nicht funktioniert kann Tilman sich auch ein Rückzahlungsmodell vorstellen.
    * rohieb: Ein angepasster Beitrag würde dazu führen, dass Personen zu viel zahlen, wenn der überwiesene Betrag nicht angepasst wird.
      Das könnte man einfach verrechnen.
      Den Beitrag nach oben zu ändern wäre problematischer.
      Wir sollten nur jetzt handeln, da sich die Diskussion sonst verlaufen könnte.

|

* Shoragan: Weit darauf hin, dass wir einen Stratum0-Mastodon-Server machen wollten. Wir haben auf unserem Server die Ressourcen dazu. Es müsste sich ein Team für die langfristige Betreuung und Moderation finden. Sho unterstützt auch anfangs gern technisch. Würde das aber ungern langfristig betreuen.

Es gibt keine weiteren Wortmeldungen.


# Auszählung der Wahlen

Dieser Teil des Protokolls wird nur von Chrissi^ geschrieben.

Die Auszählung findet am 12.02.2023 ab 15:17 im Space statt.

Anwesend die sind die Wahlleiterin Fiurin, die Wahlhelfer feya und peace-maker,
sowie die Gäste larsan und Chrissi^.
Die Öffentlichkeit wird über einen Stream im BigBlueButton hergestellt,
zu dem am Tag vorher eingeladen wurde.

## Vorbemerkung

Die Wahl hat dieses Jahr als Briefwahl stattgefunden.
Hierzu wurden im Vorfeld der Mitgliederversammlung ordentliche Mitglieder
aufgefordert eine aktuelle Post-Adresse in einem Online-Tool anzugeben.
Bei der Wahl stimmberechtigt waren dann all die ordentlichen Mitglieder,
die eine Adresse angegeben haben und zum Zeitpunkt der MV ein ausgeglichenes
Mitgliedskonto hatten.

Stimmberechtige Mitglieder haben nach der Mitgliederversammlung einen Brief mit
folgendem Inhalt zugesendet bekommen:

* Anschreiben mit Anleitung
* Wahlschein: Muss unterschrieben werden.
  Auf dem Wahlschein befindet sich ein für jede Person individuelles Geheimnis,
  welches bei der Auszählung überprüft wird.
* Stimmzettel in A5.
  Der Stimmzettel darf nicht besonders gekennzeichnet werden, da dieser sonst
  ungültig ist.
* Stimmzettelumschlag:
  da gehört der Stimmzettel rein.
* Frankierter Rückumschlag:
  Dort Stimmzettelumschlag und Wahlschein einlegen
  und zurücksenden.

## Fehlerhafte Stimmzettel

Beim Zusammenstellen der Briefwahlunterlagen ist es zu einem Fehler gekommen,
durch den ein Beisitzerkandidat nicht auf dem Stimmzettel gelistet wurde.
Dieser Fehler wurde nach mehreren Tagen erkannt.

Um den Fehler zu korrigieren wurden korrigierte Briefwahlunterlagen
versendet.
Diese enthalten neben den korrigierten Stimmzetteln auch neue
Geheimnisse auf den Wahlscheinen.

Die Auszählung wurde vom ursprünglichen Termin (05.02.2023)
auf den einen neuen Termin (12.02.2023) verschoben, um allen stimmberechtigten
Mitgliedern ausreichend Zeit für die Wahl zu lassen.


## Auszählung

### Fehlerhafte Briefwahlunterlagen
<!-- don't merge header and list -->

Zunächst werden die früheren, fehlerhaften Umschläge geöffnet und geprüft, ob darin wirklich 
fehlerhafte Wahlscheine sind.
In den früheren Umschlägen wurden keine korrekten Wahlscheine gefunden.
Diese werden nun zur Seite gelegt.

### Öffnen
<!-- don't merge header and list -->

Anschließend werden die vermutlichen V2-Umschläge zunächst geöffnet, ohne Wahlzettel oder Stimmzettelumschlag zu entnehmen.

* Danach werden die Geheimnisse der Wahlscheine mit den gespeichterten Geheimnissen abgeglichen.
* Es wurden keine doppelten oder ungültigen Geheimnisse gefunden. 
  Es werden Stimmzettelumschläge und Wahlscheine getrennt.
* Es sind 41 von 52 Briefwahlunterlagen zurückgekommen.

### Auszählung
<!-- don't merge header and list -->

* Die Stimmzettelumschläge werden geöffnet, danach direkt die Stimmen darauf vorgelesen und die Stimmen gezählt.
* Die Zählung wird erfolgreich abgeschlossen.

### Ergebnis der Wahl
<!-- don't merge header and list -->

* 1V:
    * larsan:
        * 41 Stimmen
        * Nimmt die Wahl an
* 2V:
    * Chrissi: 
        * 41 Stimmen
        * Nimmt die Wahl an
* Schatzmeisterin:
    * Helena: 
        * 41 Stimmen
        * Nimmt die Wahl an
* Beisitzer:
    * lf:
        * 36 Stimmen
        * Nimmt die Wahl an
    * NelienE:
        * 34 Stimmen
        * Nimmt die Wahl an
    * fototeddy:
        * 32 Stimmen
        * Nimmt die Wahl an
    * Mentalshirt
       * 28 Stimmen
       * Nicht gewählt
    * dadada :
        * 23 Stimmen
        * Nicht gewählt
    * Simon:
        * 13 Stimmen
        * Nicht gewählt
    * Marius:
        * 14 Stimmen
        * Nicht gewählt

|

* Kassenprüfer:
    * Jan: 31 Stimmen
    * Angela: 35 Stimmen
    * Emantor: 38 Stimmen

Auszählung beendet um 16:23.
