Beginn: 14:18

TOP -1: Technik-Test

* 33/33 Stimmen erfolgreich abgegeben

TOP 0: Begrüßung und Protokoll-Overhead
* chrissi^ begrüßt die Anwesenden und stellt die ersten TO-Punkte vor
* Wahl der Versammlungsleitung für diese MV
  * Suche nach Kandidaten und Kandidatinnen beginnt
    * larsan
  * Keine weiteren Einträge, Abstimmung beginnt 14:24
  * 27 Stimmen für Larsan → Larsan macht Versammlungsleitung

* Wahl der Protokollführung:
  * Kandidaten:
    * Chrissi 31 von 33 Stimmen
    * drc 33 von 33 Stimmen
  * Abstimmung beginnt 14:27
  * Beide gewählt :-)

* Abstimmung über die Zulassung von Gästen
  * Beginnt 14.29
  * Optionen: Ja / nein / Enthaltung
  * 28 ja, 3 Enthaltung → Gäste zugelassen
  * Entsprechende Links werden über IRC/Matrix/NV verteilt
  * BBB-Konferenz ohne weitere Zugangskontrolle

* Wahl zur Wahlleitung
  * Suche nach Kandidatinnen soll beginnen
  * Unterbrochen durch technische Probleme um 14:30, Sho untersucht
    * Sho: die vm reagiert nicht auf ssh, es bleibt spannend

~~~ Pausenmusik ~~~

    * Unzureichend RAM, sho korrigiert
    * 14:40 ist Openslides wieder verfügbar
    * Anwesenheitszustand wird zurückgesetzt, um Funktion sicherzustellen
    * Einzelne Verfügbarkeitsprobleme, vorerst 32 Personen anwesend
    * Chrissi erläutert Aufgaben der WL
      * Suche von Kandidatinnen für Vorstandsposten
      * Durchführung Briefwahl inkl. Auszählung
    * Verbindungsprobleme scheinen zwischenzeitig geklärt zu sein.
  * Kandidatinnen
    * …
  * Verschoben auf später

* Feststellung der Beschlussfähigkeit:
  * Chrissis Notizen von vor der MV: 139 Mitglieder,
    * davon 13 Fördermitglieder => 126 ordentliche Mitglieder
  * 23% für das Quorum: 29 ordentliche und stimmberechtigte Mitglieder
  * Um 14:45 sind 34 stimmberechtigte Personen im Openslides angemeldet → beschlussfähige MV

* Geschäftsordnung der MV
  * Vorschlag: Übernahme aus den vorherigen Jahren
    * Vollständiger Text: https://stratum0.org/wiki/Mitgliederversammlung/Gesch%C3%A4ftsordnung
  * Larsan liest die Geschäftsordnung vor
  * Abstimmung startet 14:49
    * Abstimmungsmodus: Ja / Nein / Enthaltung
  * 35 Stimmen dafür, 0 dagegen, 0 Enthaltungen → angenommen


* Finanzbericht von ktrask
  * Beginn: 14:51
  * (notizen hier nur von Dingen, die nicht auf den Folien stehen)
  * Das erste mal seit ein paar Jahren haben wir wieder Schlüssel ausgegeben
  * Getränkepreise: Wenn nichts dran steht 1,50€ ("default")
  * Matekasse hat zwischenzeitlich Verluste gemacht. Auch deshlab mussten wir erhöhen.
  * Erstattungen:
      * Matekasse für Getränke und Snacks
        * Helena erklärt, wie der Ablauf ist
        * Falls der Betrag in der Kasse nicht mit dem Kassenbuch übereinstimmt:
          Schatzmeisterin Bescheid geben. IdR wurde sich verrechnet oder falsch abgelesen.
      * Erstattungskasse:
        * Workflow steht auch auf der Erstattungskasse und im Wiki https://stratum0.org/wiki/Erstattungskasse
        * Es gibt auch ein Pad, in denen diskutiert wird, was man anschaffen könnte: https://pad.stratum0.org/p/nachschub
      * Große Beträge: (> 50€)
        * Es gibt eine Wiki-Seite dazu https://stratum0.org/wiki/How_To_Antrag
      * Wie funktionieren Anträge?
        * Anschaffungen nur in €
        * Wenn Anschaffung auf Rechnung: Wir wollen die Zahlungsfristen einhalten :-)
      * Erstattung:
        * Jeder Antrag bekommt eine Umlaufbeschluss-Nummer "[UB yyyy-mm-dd-nn]"
          Diese Nummern stehen auch im Wiki auf der jeweiligen Vorstandssitzungs-Seite. z.B. https://stratum0.org/wiki/N%C3%A4chste_Vorstandssitzung
        * E-Mails an vorstand@ sind am besten, dann sieht das der gesamte Vorstand.
        * Kontodaten für die Erstattung bitte in der Mail angeben, um Probleme zu vermeiden
        * Es kann dazu kommen, das man auf die Erstattung waren. idR wird ein Mal im Monat erstattet.
  * Ideeler Bereich: Projekte:
      * Helena stellt fest, dass Ausgaben für Filament nicht auf die richtige Buchungskostenstelle gebucht wurde.
      * Freifunk kann selbständig über Ausgaben entscheiden. Größter Posten: Server
      * Laser braucht viel Strom, der aber nicht getrennt abgerechnet wird
      * MakerVsVirus: Das sollte weiter gespendet werden. Das MakerVsVirus-Projekt hatte vorher festgelegt, an wen das gespendet werden sollte. Das haben wir entsprechend weiter gespendet.
  * Ideeller Bereich: Space:
     * Geräte und Einrichtung für ~6k€
     * 15k€ für Miete und Nebenkosten
  * Vermögensverwaltung: Sowas machen wir nicht
  * Zweckbetriebe: Sowas haben wir auch nicht
  * Geschäftsbetrieb
    * Matekasse macht 3.5k€ Umsatz p.a.
    * Leichter Verlust (<40€) im Jahr, daher war die Preiserhöhung wichtig und richtig. Wir wollen nicht aus Mitgliedsbeiträgen den privaten Konsum quer finanzieren.
  * Mankobuchungen
    * Helena: Wert ist nicht korrekt
    * Wird auf den Folien korrigiert
    * 9€ Mankobuchung, war nicht mehr nachvollziehbar woher der Fehler kam.
  * Umbuchungen
    * zb Geld von Matekasse auf Konto, Konto auf Erstattungskasse
  * Übersicht:
    * Einnahmen 38k€
    * Ausgaben: 35k€
    * → +3k€
  * Vorhandenes Kapital:
    * Zu Beginn des Jahres gibt es viele Mitgliedsbeiträge
    * Auf alle Jahre betrachtet aufsteigend
  * Helena zieht das Fazit: Gebt Geld für coole Projekte oder Space 3.0 aus
  * Monatliche Einnahmen: ~2.4k€
    * Mitgliedsbeiträge und kleinere Spenden
  * Monatliche Verpflichtungen: ~1.2k€
    * Miete, Strom
    * Versicherungen
    * Server
  * Mitgliederentwicklung:
    * Gezeigt nur die ordentlichen Mitglieder
    * Helena: Mitgliederentwicklung zeigt nur bis 2023, muss korrigiert werden.
  * Bestände:
    * Knapp über 30k€ frei verfügbares Kapital
    * Wie gehabt 3k€ Rückstellung für Miete
  * Ausblick:
    * Wir nehmen deutlich mehr Geld ein, als wir ausgeben. Wir brauchen gute Ideen, wie wir das ausgeben können.
  * Fragen?
    * (keine)
 
 * Tätigkeitsbericht des Vorstandes: lf
  * Begin 15:16
  * "Was hat der Vorstand 2023 gemacht?"
  * Vorstellung der AG:Geschäftsführung
    * Rechnungsprüfer
    * Vertrauensentität zum Ansprechen bei Problemen im Vereinsleben, wenn der Vorstand nicht involviert werden soll/kann
      * Aktuell schwer zu erreichen. MV sollte überlegen, ob sie eine neue Bestellen möchte.
    * Infos stehen im Fließtext
    * 1V, 2V, Schatzmeisterin, 3 Beisitzerinnen
    * Schlüsselverwaltung larsan (physisch und SSH)
    * Protokolle chrissi^
  * Arbeitsweise:
    * Im Chat-Raum (Matrix/IRC) ist aktuell wenig los. Ist aber offen für jede*n.
    * Geschäftsordnung: https://stratum0.org/wiki/Vorstand/Gesch%C3%A4ftsordnung
  * Zahlen:
    * Mehr Mails als damals™
    * Mehr UBs, viel davon kleine Dinge
  * Änderung an der Vorstands-GO
    * Wechsel zwischen normalen und ermäßigten Beitrag ist jetzt ohne Abstimmung möglich.
    * Dieses Jahr nur vier, anstatt 5 Vorstandssitzungen.
  * Hervorgehobene Projekte und Ereignisse
    * Glasfaser \o/
      * Jetzt eventuell möglicherweise vielleicht Glasfaser über 1&1, aktuell aber noch hin-und-her mit der Hausverwaltung zum Kabelweg
    * Mehr Licht in der Küche, damit hat der Space fast nur noch LEDs
      * Da fehlt noch jemand, der Lust hat sich zu kümmern.
    * Lebensmittelmotten bekämpft. Haben Schlupfwespen eingesetzt. Ist ein wiederkehrendes Problem.
      * Gelagerte Lebensmittel bitte luftdicht verschließen
    * Neue Fahrradkellerschlüssel
    * Solar:
      * Haben zwei Module für ein "Balkonkraftwerk" angeschafft. Soll "oben" an den Turm angebracht werden. Soll passieren, wenn es nicht mehr so kalt ist.
    * Größere Ausgaben
      * SLA-Drucker → Resin-Lab
      * Neue Switche
      * Gefrierschrank
      * PCBite
   * Sammlung von Links am Ende der Präsentation
     * data.stratum0.org: Zeigt auch welche UBs noch freie Finanzmittel haben.

* Bericht der Rechnungsprüfer
  * Emantor stellt vor
  * Weniger Zwischenprüfungen, hat aber gut funktioniert
  * Kasse gut geführt
  * Man findet Fehler, z.B. ein Vertipper zwischen "0" und "9" in der letzten Stelle. Aber nicht schwerwiegend.
  * Bitte an Mitgliedsentitäten
    * Mitgliedsbeiträge halbjährlich oder sogar jährlich überweisen wäre super
      * Das senkt die Kontoführungsgebühren. Wir zahlen auch für eingehende Überweisungen. Dann verdient auch die Bank weniger an uns.
    * Kassenzettel wenn möglich mit Zwischensumme oder komplett getrennt
      * Man findet Kassenzettel mit handschriftlichen Notizen, um private Dinge von Space-Einkäufen zu trennen
      * Zwischensumme hilft das zu trennen und macht weniger Arbeit bei der Kassenprüfung.
  * Empfehlung: Vorstand entlasten

* Jahresbericht 2023: larsan

~~~ Pausenmusik ~~~

  * Beginn: 15:34
  * Wieder mehr Uptime \o/
    * Fast auf Vor-Corona-Niveau: 85%
    * Letztes Jahr war man auch noch mal alleine im Space. Jetzt ist es voller.
  * Vorträge:
    * Es gab ein paar. Aber redet gern über eure coolen Projekte.
  * HOA
    * {{Fotos}}
    * Dieses Jahr in den Schulferien (in NDS). Damit sind jetzt andere Personen in der Lage dabei zu sein.
  * Helloweenparty
  * Resinlab
    * War das Flurklo unten, wenn man die 1. Treppe hoch geht. 2m². Haben da durchrenoviert und Sticker versteckt.
    * Fenster / Belüftung muss noch gemacht werden.
    * Das WC ist für Feiern weiterhin nutzbar.
  * The Gottschalk-Saga continues?
    * Ja, wir haben eine neue (4.!) Hausverwaltung. Sitzen jetzt sogar auf dem Gelände.
    * Antworten sogar auf E-Mails
  * Regelmäßiges:
    * FFBS-Bericht von Emantor
      * Während die Präsentation hochläd: ~~ Pausenmusik ~~
      * Jetzt ohne Pandemie
      * Wir haben coole Sticker!
      * Treffen jede Woche Mittwoch ab 19:00 Uhr
      * Fest der Kulturen 2023 mit Internet versorgt, 2024 wieder
      * Kontakt zur Stadt: FF für …
        * Sportstätte Rüningen
        * Jugendplätze
        * Marco hat die Stadt "wachgeküsst". (Der ist beim Freifunk aktiv. Aber nicht mitglied im Verein.)
        * Es gab ein Treffen anfang November. Jetzt gab es dazu das Protokoll der Stadt. Die Mühlen mahlen langsam weiter.
        * Dank an Marco.
      * {{ Fotos }}
      * Kontakt zur Stadt Wolfenbüttel:
        * Jemand ist von BS nach WF gewechselt. Macht dort in der Stadt aktiv Freifunk.
        * In der Innenstadt gab es schon einmal ein WLAN. Eventuell können wir das übernehmen.
        * Da wird in diesem Jahr was passieren. Fortschritt wird eher von uns, als von der Stadt gebremst. Das ist ungewohnt.
      * Waren auf dem HOA und haben Internet gemacht.
        * Starlink zuverlässiger als LTE mit Richtantenne
      * Parker-Wochenenden:
        * Wir betreiben unser Netz etwas anders als andere Communities.
        * Hilfe gerne gesehen, Ankündigung der Treffen im Stratum0-Wiki
        * Ist eigentlich nur ein Tag. Kein ganzes Wochenende :-)
        * Könnten wir noch besser ankündigen.
      * Ende 15:45
  * larsan übernimmt wieder
  * Digital Courage:
    * Trifft sich jeden 2. Donnerstag im Space und Online.
    * Vorträge
    * Citizen Science Workshop an der HBK
    * Aktuelle Themen: Chatkontrolle, Google "Privacy" Sandbox, IFG Niedersachsen (nur NDS und Bayern haben aktuell keins), Digitale Selbstverteidigung
      * Nur Bayern und NDS haben kein IFG :/
    * Weitere Leute immer willkommen
  * CTF-Team "CyberTaskForce Zero" weiterhin aktiv
    * peace-maker erzählt, larsan zeigt Folien
    * von 3..4..5 Leuten auf ~8 Leute gewachsen
    * >25 CTFs in 2023
    * Letztes Jahr ziemlich erfolgreich: DE: Platz 6, Global Platz 65
    * Treffen sich jetzt auch alle 14 Tage Dienstags ab 18:00 und nicht nur zu den CTFs
    * Vernetzen sich innerhalb von DE: Kooperatio mit RedRocket, PeaceMaker ist dort auch im Vorstand, Auch Teil der Sauercloud
    * Haben sich (mit Sauercloud) Qualifiziert nach Las Vegas zu fliegen und einen echten Satelliten im All zu hacken
    * Termine im Kalender, kommt vorbei (auch gerne mit wenig oder keinem Vorwissen)
  * Hey Alter!
    * Seit 2020 ongoing
    * Folie zeigt ein altes Foto ohne die neue Flagge :/
    * Nikolausaktion: Desktoprechner an Schülerinnen und Schüler
      * Es gab sogar eine Schlange vor dem Gebäude, 130 Rechner verteilt, 50 auf der Warteliste
  * Pecca: Malkränzchen
    * Mittwochs 18:00
    * Gibt es seit sieben Jahren
    * Regelmäßiges Treffen für kreative Tätigkeiten
    * Pecca bittet größere Treffen (z.B. Schrei-Wettbewerbe) und Küchenorgien an andere Tage zu planen.
    * Es ist auch schade, wenn Menschen zu den Treffen nicht kommen, weil es ihnen zu laut ist.
    * {{ Fotos }}
    * Auf die nächsten sieben Jahre!
  * larsan übernimmt wieder
  * Bierbrauen. Es wurde gebraut und es wird wieder gebraut werden.
  * Spieleabende im Kalender
  * Wir hatten für einen Tag eine Spacekatze.
  * Es waren Menschen von uns auf dem 37C3
  * Über das Jahr gab es viele neue Regenbogendinge.
  * LED-Panels getestet und verbaut
  * Es gab Veränderungen bei den Kühlschränken.
  * Solarstrom in Aussicht, es fehlt besseres Wetter
  * Silvester mit Dinovulkan.
  * Danke an alle, die: Müll rausbringen, sich um den Space kümmern, die Putzen, Getränke kaufen, Dinge nachkaufen, ...
  * Danke, die Vereinsinfrastruktur betreiben. Besonders Shoragan, aber auch die anderen 14 Admins
  * Wünsche für 2024:
    * Unsere 10-Jahres-Feier fehlt nich
    * Coderdojo wieder in Planung
    * Mehr Blogposts
    * Mehr Projekte!
  * Ende: 16:00

* Bericht der Vertrauensperson
  * Nicht anwesend?
  * Entfällt
  
* Entlastung des Vorstandes
  * Blockentlastung beantragt
  * Abstimmung ohne Beteiligung der Vorstandsmitglieder nicht technisch überprüfbar
    * 29 Stimmen notwendig bei 36 anwesenden Entitäten, 6 Vorstandsmitglieder enthalten sich
  * Abstimmung beginnt 16:03
  * Ergebnis 16:06
    * 29 dafür
    * 0 dagegen
    * 0 Enthaltungen
  * Vorstand entlastet
  
* Änderungsanträge
  * Es gibt keine SÄA und auch keine für die Beitragsordnung.

* Feedback, Input für den nächsten Vorstand
  * chrissi^: Finanzielle Möglichkeiten nutzen, um Vorträge mit Externen zu organisieren, immerhin ist Bildung auch Vereinszweck
    * Tim: Eventuell nicht nur Vorträge, sondern auch Workshops?
      Man braucht aber die passenden Ideen.
    * larsan: pretix steht bei Bedarf zur Verfügung
  * Dank aus der Runde an den Vorstand und alle aktiven Entitäten
  * Daniel: Eventuell das HOA von der UG zum Verein zu übertragen?
    * Tim: Wurde auf der letzten Orga-Runde angesprochen. Nichts für vor dem HOA. 
      * Chrissi als Vorstand findet das spannend. Muss man aber gut durchdenken.
  * Tim: Vegan Academy (vegane Kochrunde) wiederbeleben? -> Tim anschreiben.
    War irgendwann vor Jahren eingeschlafen.
  * Shoragan: Wir haben noch Kapazitäten auf dem Vereinsserver.
    Falls jemand Dienste für eine größere Gruppe betreiben möchte: Es gibt noch Möglichkeiten.
    Eventuell gibt es neben den aktuellen Diensten noch Nieschen, die man abdecken könnte.
    Sho hilft auch gern bei der Inbetriebnahme.
  * Neliene: Erste-Hilfe-Kurs
  * Fera per Chat: "Mental Health First Aid"
    * Fera bietet sich dan den vorzubereiten.
  * lf: Exkursionen
  * chrissi^: Feuerlöschen für Amateure
    Daniel: Haben intern vermutlich genug Feuerwehr-Menschen. Brauchen wir nicht extern anfragen.

* 10 Minuten Pause vor den Wahlen, Beginn: 16:18

~~~ Pausenmusik ~~~

* Ende der Pause um 16:28

* Wahlen:
  * Wahlleitung wird gesucht
    * Tooling vorhanden
  * Kandidat*innen
    * 53c70r
  * Abstimmung beginnt 16:31
    * Leichte technische Probleme
    * Ergebnis: 29 dafür, 3 Enthaltungen
  * 53c70r will Wahlleitung nicht übernehmen
  * Erneute Suche, Kandidat*innen:
    * Marie Wellhausen
  * Abstimmung beginnt 16:26
    * Es gibt weiterhin technische Probleme mit dem Openslides bei einzelnen Personen
    * Ergebnis: 29 dafür, 1 Enthaltung
  * Marie nimmt die Wahl an \o/
  * Wahlhelfer*innen
    * Kandidat*innen
      * Emantor
      * Frauke
      * Tim / dadada
      * Nux
    * Marie findet die Anzahl gut.

* Wahlordnung
  * hatte larsan bereits vorgestellt, siehe GO im Wiki
  
* Marie übernimmt
  * mjh fragt noch einmal nach, ob marie nicht fragen möchte, ob noch jemand unzufrieden wäre?
  * Marie fragt, ob jemand mit einem Wahlhelfer unzufrieden wäre?
    * Es werden keine Einwände geäußert
  * Suche nach Kandidat*innen für die offenen Posten
    * 1V
      * Lars Andresen "larsan"
      * lf
    * 2V
      * Chris Fiege "chrissi^"
      * lf
    * Schatzmeister*in
      * ktrask
    * Beisitzer*in
      * Jonas Martin "lf"
      * Nele Warnecke "NelienE"
      * Silvan Nagl "53c70r"
      * Oleh Paliy
      * Marie Goetz "EmJee13"
      * Chris Fiege "chrissi^"
      * Lars Andresen "larsan"
      
 * (Während der Suche) lf stellt Beisitzerposten kurz vor
   * Entlastung der anderen Vorstandmitglieder
   * Mails lesen, UBs bearbeiten
   * Arbeitsvolumen gut anpassbar
   * Gute Gelegenheit sich einzuarbeiten und das "Vorstand sein" auszuprobieren.
   * Aufwand
     * 4 Sitzungen im Jahr
     * 1x / Monat Arbeitstreffen 
     * Remote möglich

* Kandidat*innen stellen sich vor
  * lf 
    * lf lässt sich von anderen beschreiben...
  * Nele:
    * Nele hat Audio-Probleme und wird von den Anwesenden beschrieben
    * Nele: "Ich hatte das Gefühl gerade ein paar Aufgaben für mich gefunden zu haben und würde das noch mal weiter machen
  * 53c70r
    * mjh: sector ist schon länger dabei, hat auch andere ehrenamtliche Arbeiten. mjh hält ihn für geeignet andere Ansichten zu vertreten
  * Oleh:
    * Kommt gern in den Space. Ist dankbar für die Infrastruktur. Und möchte dafür etwas zurück geben.
  * Marie EmJee13:
    * Seit ~1.5 Jahren im Space
    * Viel HOA-Orga
    * Ist ge
  * Chris Fiege
    * Vorstand seit lange
    * Protokolle, Emails
    * Auch gerne bereit, keinen Posten zu haben
  * Larsan:
    * Macht das jetzt "Seit ein paar Jahren", hat mal als Beisitzer begonnen.
    * irgendwann mal 1V geworden und wurde dann immer wieder gewählt.
    * Ist auch gern bereit zu beraten und steht für Fragen zur Verfügung.
  * Helena "ktrask"
    * Würde das noch einmal machen. Muss dazu sagen, dass sie jetzt nach Hannover gezogen ist. wird auch noch in BS sein.
    * Bräuchte aber eine weitere Person im Vorstand, die vor Ort unterstützt. Am Besten eine beisitzende Entität.

  * Zur Schatzmeister*in:
      * Marie: Dieses Jahr die Gelegenheit das kennenzulernen.
      * Emantor: Aus Kassenprüfungssicht: Weniger Arbeit, als noch vor einigen Jahren. Viel Automatisiert.
      
  * chrissi^ stellt kurz Posten des 2V vor
    * Nicht mehr Aufwand als als Beisitzer
    * Für 1V/2V/Schatzmeister*in: Einmaliger Besuch bei Bank und Notar zur Eintragung
    
  * larsan stellt kurz Posten des 1V vor
    * Aufgaben wie Beisitzer*innen
    * Aktuell zusätzlich repräsentative Aufgaben, die könnten aber auch anders verteilt werden

  * Kassenprüfer*innen
    * Neuwahl/Neubestimmung notwendig?
    * Angela und Emantor stehen wieder zur Verfügung
    * Marie fragt noch einmal, ob jemand eine Wahl dazu haben möchte.
    * Niemand meldet sich.

  * Vertrauensperson kann auch gewählt werden, wenn gewünscht wird
    * Kein Vorstandsposten
    * Wird von MV ernannt/bestimmt
    * Mehrere Personen sind dafür die Vertrauenspersonen neu zu wählen, da die alten sich nicht gemeldet haben.
    * Es gibt keine Stimmen dafür die Vertrauensperson zu behalten.
    * Kandidat*innen
      * Antje Mönch "Fera(lia)"
      * Frauke Gellersen
    * Aufgrund der Abwesenheit der aktuell bestellten Personen stellt chrissi^ den Posten vor
      * Bei Problemen mit Vorstandsmitgliedern und Vereinsbezug ansprechbar sein
      * Klärende Gespräche suchen
    * Es wird überlegt, ob die MV 1 oder 2 Vertrauenspersonen berufen möchte.
    * Marie hört raus, dass 2 Vertrauenspersonen die präferierte Option sind.
    * Frauke: Ist es ein problem, dass Sie Wahlhelferin ist und gleichzeitig auszählt?
    * Wir überlegen, ob wir direkt im Openslides abstimmen.
    * Vorstellung der Kandidatinnen
      * Fera hat kein Mikrofon und wird von der Runde vorgestellt
        * Malkränzchen
        * Viel Zuspruch
      * Frauke:
        * Ist vermutlich bekannt.
        * Hat mit Antje zuammen gehäkelt.
        * Will Probleme vertraulich behandeln.
        * Viel Zuspruch
    * Abstimmung beginnt 17:31
      * Modus Zustimmungswahl: 2 Stimmen
      * Wiederholung aufgrund von technischen Problemen: Wahl nur einer Person nicht möglich
    * Wiederholung der Wahl um 17:35
      * Modus Zustimmungswahl: Maximal 2 Stimmen
      * Ergebnis um 17:37
      * Antje: 31 Pro
      * Frauke: 31 Pro
      * 3 generelle Enthaltungen
    * marie: Nehmt ihr die Wahl an?
      * Frauke: Ja
      * Antje: Ja

* larsan übernimmt 17:39
  * Larsan: Es gibt bald einen Brief mit der Briefwahl
  * Adressabgabe ist noch möglich! Geheimer Link kam per Mail, sonst bei chrissi^ melden
  * Adressen in Boxen im Space sind möglich.

TOP: Sonstiges:
* Termin nächste MV
  * 19.1.2025, 26.1.2025?
  * drc: Wollen wir die nächste MV wieder online machen?
    * Chrissi: Findet Online gut
    * Daniel: Regt an das wieder offline zu machen.
    * EmJee13: Wäre für hybrid
    * shoragan: Nimmt an, dass wir vor Ort effizienter sind.
    * Tim/dadada: Eventuell um größeren Raum kümmern?
    * Chrissi: Die BLSK bieter Räumlichkeiten für solche Versammlungen für Vereine an.
    * mjh: Mag Status Quo, weil Online-Teilnahme inklusiver ist
      Sonst muss man sich überlegen, wie man die MV streamen kann.
      Wir sollten überlegen, wie man das Tooling verbessern kann.
      Würde es nicht groß anders machen, als in diesem Jahr.
    * drc: Das erste Jahr, in dem das Openslides uns Probleme macht.
    * mjh: Hat jemand das Gefühl, dass sich jemand nicht ausreichend einbringen konnte? Die Versammlung anfechten zu wolllen?
    * lf: Remote ist "entspannter". Auch MVs vor Ort sind zum Teil umständig und chaotisch
      Sonst müssen wir uns größere Räumlichkeiten, vorzugsweise mit Schnittchen, suchen.
      Eventuell mit besserem Wetter und draußen?
    * Emantor: Jemand meinte, dass man Remote sein eigenes WC hat.
    * Es wird eine Umfrage im BBB gestartet, um ein vorläufiges Meinungsbild zu erhalten
       * Online: 6
       * Hybrid: 18
       * Offline: 0
       * Der Vorstand soll entscheiden: 6

  * 26.1.2025 wird für die nächste MV als Termin angedacht


* Weltherrschaft
  * Emantor beantragt den Punkt zu vertagen

* Weiteres
  * lf weist darauf hin, dass Mitglieder Hausrecht ausüben dürfen, falls das notwendig ist
  * Es wird über Spacekatzen diskutiert 🐈️

* Larsan schließt die Versammlung
  * vielen Dank an alle, die dabei waren
  
Ende: 18:01


Live-Protokoll der Auszählung:
Meta: Protokoll für die Vorstandswahlen
Pad Status: 2024-02-25,in progress
Contact: drc

Anwesend:
    * Marie – Wahlvorstand
    * dadada – Wahlhelfer
    * Frauke – Wahlhelferin
    * Emantor – Technik

    * Doom -  Beobachter
    * lf – Beobachter ab ca. 16:35 bis 16:55
    * drc – Protokoll

Beginn 16:20, Livestream auf
https://www.twitch.tv/doommeer

Briefe werden gezählt
* 29 Stimmzettel wurden nicht abgeholt
* 52 Briefumschläge sind eingegangen

* 97 Personen mit Stimmberechtigung
→ 16 Briefe nicht zurück

Überprüfung der Geheimnisse
* Briefe werden geöffnet, Marie und Frauke überprüfen die Geheimnisse
	* Zuordnung anhand des Zeichens
	* Je eine Box für Unterschriftszettel und Stimmumschlag
* Ein Brief in einem Nicht-Standard-Umschlag, aber mit gültigem Inhalt → wird normal gezählt
* Ein Brief mit 2 Umschlägen, aber gültigem Inhalt → wird normal gezählt
* Ungültige Stimmen werden in weitere Box aussortiert
	* Ohne Unterschriftzettel: 1
	* Kein Inhalt: 1
* Somit sollten 50 Unterschriftzettel und 50 Stimmzettelumschlänge vorhanden sein
	* Kontrollauszählung durch Emantor und dadada bestätigt das

Auszählung der Stimmen
* Marie + dadada öffnen Stimmumschläge und lesen vor
* Frauke + Emantor zählen Stimmen an Flipchart aus
* Auszählung beendet um 17:21

Ergebnisse
* 1V
	* larsan:  43 → wird von Frauke angerufen, nimmt die Wahl an
	* lf: 37
* 2V
	* lf: 43 → ist anwesend, nimmt die Wahl an
	* chrissi^: 42
* Schatzmeister*in
	* ktrask: 50 → wird von Frauke angerufen, nimmt die Wahl an

~~~~ Technische Unterbrechung wegen Streamproblemen, nach ca. 5 Minuten behoben ~~~~

* Beisitz (max. 3)
	* lf: 45 → ist 2V, kann nicht Beisitzer werden
	* chrissi^: 44 → wird von Emantor angerufen, nimmt die Wahl an
	* marie (emjee13): 43 → wird von Marie angerufen, nimmt die Wahl an
	* larsan: 41 → ist 1V, kann nicht Beisitzer werden
	* nele: 39 → wird von Frauke angerufen, nimmt die Wahl an
	* 53c70r: 36 → keine weiteren Posten frei
	* oleh: 27 → keine weiteren Posten frei

Stimmzettel werden archiviert

→ 17:35 Ende der Auszählung, Protokoll- und Streamende





