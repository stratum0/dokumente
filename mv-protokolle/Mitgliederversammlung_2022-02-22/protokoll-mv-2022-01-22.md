---
vim: ts=4 et sw=4 tw=90
build-me-with: make pdf
documentclass: s0minutes
papersize: a4
lang: de
toc: true
toc-depth: 2
links-as-notes: true
title: Mitgliederversammlung 2022-01-22
s0minutes:
  typeofmeeting: \generalassembly
  date: 2022-01-22
  place: "online; OpenSlides- und BigBlueButton-Instanzen des Stratum 0 e.V."
  startingtime: "14:00"
  endtime: "17:12"
  attendants: 32 akkreditierte ordentliche Mitglieder
  absentees:
  minutetaker: Chrissi\^, Tilman
---

\setcounter{section}{-3}
Vorbemerkungen
==============

Die Veranstaltung findet in einem Raum auf dem BigBlueButton-Videokonferenzssystem des
Vereins statt.
Abstimmungen werden über OpenSlides vorgenommen.
Beide Systeme werden auf Servern des Vereins von Mitgliedern betrieben und gewartet.

Die Mitglieder haben mit der Einladung zur Mitgliederversammlung an ihre letzte dem
Vorstand bekannte E-Mail Adresse einen zufälligen Akkreditierungs-Code mitgeteilt bekommen.
Die Akkreditierung findet über einen Video-Chat mit Chrissi^ (Vorstandsmitglied) und Kasa
(Nicht-Vorstandsmitglied) statt, in dem je ein einzelnes Mitglied zur Zeit nach Abfrage
seines Akkreditierungs-Codes im OpenSlides Stimmrechte eingeräumt bekommt.

Zugänge zum OpenSlides wurden vor der Mitgliederversammlung an alle aktiven Mitglieder
versendet.
Somit sind alle Mitglieder, unabhängig von ihrer Stimmberechtigung, in der Lage, an der
Mitgliederversammlung teilnehmen.

In diesem Protokoll wird bei jeder Wahl oder Abstimmung die Anzahl der
abgegebenen und möglichen Stimmen angegeben.
Die Anzahl der möglichen Stimmen ergibt sich dabei aus denen im OpenSlides
zum Zeitpunkt der Wahl oder Abstimmung auf "anwesend" gesetzten Mitglieder.
Verlassen akkreditiere Mitglieder die Versammlung oder kommen akkreditiere
Mitglieder hinzu, so verändert sich die Anzahl der möglichen Stimmen.


Technik-Test
============

* Nach einer kurzen Begrüßung durch larsan beginnt die Veranstaltung mit einem
  Technik-Test im OpenSlides.
* Hierzu wird im OpenSlides eine Probeabstimmung eröffnet.
* Zu diesem Zeitpunkt sind 30 von 32 akkreditierten Mitglieder im OpenSlides
  im Status *anwesend*.
* Der Wahlgang wird mit 30 abgegebenen von 30 möglichen Stimmen beendet.
* Die zwei fehlenden akkreditierten Mitglieder können auch nach einiger Zeit
  nicht identifiziert werden.
  Wir beschließen eventuelle Probleme bei der nächsten Abstimmungen zu lösen.

Protokoll-Overhead
==================

## Wahl der Versammlungsleitung

* larsan stellt sich selbst als Kandidat der Versammlungsleitung auf.
* Es gibt keine weiteren Kandidaten.
* Im OpenSlides wird eine Wahl durch Zustimmung gestartet.
* Der Wahlgang wird mit 30 abgegebenen von 31 möglichen Stimmen beendet.
* larsan erhält 30 von 30 Stimmen.
* larsan übernimmt die Versammlungsleitung.

## Wahl der protokollführenden Entitäten

* Chrissi^ und Tilman stellen sich zur Verfügung.
* Es gibt keine weiteren Kandidaten.
* Im OpenSlides wird eine Wahl durch Zustimmung gestartet.
* Der Wahlgang wird mit 31 abgegebenen von 31 möglichen Stimmen beendet.
* Chrissi^ erhält 31 von 31 Stimmen.
* Tilman erhält 29 von 31 Stimmen.
* Chrissi^ und Tilman übernehmen die Protokollführung.

## Zulassung von Gästen

* larsan schlägt vor Gäste zur MV zuzulassen.
* Im OpenSlides wird eine Abstimmung gestartet.
* Der Wahlgang wird mit 31 abgegebenen von 31 möglichen Stimmen beendet.
* Ergebnis:
    * Ja: 28 Stimmen
    * Nein: 1 Stimme
    * Enthaltung: 2 Stimmen
* Damit sind Gäste zugelassen.
* Wir lassen Gäste im BigBlueButton zu und verteilen den Link zum BigBlueButton
  und dem Live-Protokoll via IRC/Matrix und den Normalverteiler.

## Wahl der Wahlleitung

* Kasa stellt sich als Kandidat zur Verfügung.
* Es gibt keine weiteren Kandidaten.
* Im OpenSlides wird eine Abstimmung gestartet.
* Wir beenden die Wahl bei 32 abgegebenen von 32 möglichen Stimmen.
* Ergebnis:
    * Ja: 31 Stimmen
    * Nein: 0 Stimmen
    * Enthaltung: 1 Stimme
* Kasa übernimmt somit die Wahlleitung.

## Feststellung der Beschlussfähigkeit

* Chrissi^ stellt fest:
  Zum Zeitpunkt der MV hat der Verein 101 ordentliche Mitglieder.
* Davon sind 32 (31,7%) zur MV akkreditiert.
* Chrissi^ stellt daher die Beschlussfähigkeit fest.

## Annahme der Geschäftsordnung

* larsan führt das Thema ein:
  Die MV hat sich in der Vergangenheit eine Geschäftsordnung gegeben.
* Diese bestätigen wir auf jeder MV einmalig.
* Die vorgeschlagene Geschäftsordnung wird aus dem
  [Wiki](https://stratum0.org/mediawiki/index.php?title=Mitgliederversammlung/Gesch%C3%A4ftsordnung&oldid=27550)
  vorgelesen.
* Im OpenSlides wird eine Abstimmung über die unveränderte Annahme der
  Geschäftsordnung gestartet.
* Wir beenden die Stimmabgabe bei 31 abgegebenen von 32 möglichen Stimmen.
* Ergebnis:
    * Ja: 31 Stimmen
    * Nein: 0 Stimmen
    * Enthaltung: 0 Stimmen

### Für diese MV gilt daher die folgende Geschäftsordnung:
<!-- don't merge header and list -->

    == §0 Wahlen ==

    1. Als Wahlmodus wird die Zustimmungswahl festgelegt.
       (Jede stimmberechtigte Entität kann für jede kandidierende Entität eine
       Stimme abgeben, Stimmenkumulation ist nicht möglich.)
    2. Es wird in geheimer Wahl auf Stimmzetteln gewählt.
       Dabei dürfen nur von der Versammlungsleitung genehmigte Stimmzettel
       genutzt werden.
    3. Die Kandidaten mit den meisten Stimmen sind gewählt, sofern sie
       mindestens die Stimmen von 50% der Stimmberechtigten erhalten haben,
       solange genug Posten für das entsprechende Amt zu besetzen sind.
    4. Falls mehrere Posten für ein Amt zu vergeben sind (z.B. Beisitzer,
       Rechnungsprüfer), findet die Besetzung absteigend nach Stimmenanzahl
       statt, bis alle Posten des entsprechenden Amtes besetzt sind.
    5. Falls sich durch Stimmengleichheit keine eindeutige Besetzung ergibt,
       findet eine Stichwahl zwischen den entsprechenden Kandidaten mit der
       gleichen Stimmenanzahl statt.
       Eine Nachwahl findet jeweils für die Posten Schatzmeister, stellv.
       Vorsitzender, oder Vorsitzender statt, falls das entsprechende Amt
       nicht besetzt wurde.
    6. Die Auswertung der Wahl erfolgt in der Reihenfolge Vorstandsvorsitzender,
       stellv. Vorsitzender, Schatzmeister, Beisitzer, Rechnungsprüfer.
       Kandidaten, die schon für ein Amt gewählt worden sind und es angenommen
       haben, werden bei der Auswertung der nachfolgenden Ämter nicht mehr
       berücksichtigt.
    7. Gewählte Kandidaten können von der Wahl zurücktreten.
       Die Annahme der Wahl oder der Rücktritt von der Wahl ist auch
       fernmündlich möglich.

# Berichte

In diesem Tagesordnungspunkt berichtet der Vorstand,
die Kassenprüfer und
die Vertrauensentitäten
über die Zeit seit der letzten Mitgliederversammlung.

## Finanzbericht (Helena)

Helena war in der Zeit seit der letzten Mitgliederversammlung Schatzmeisterin und
stellt ihren Finanzbericht vor.
Die Slides sind als Bericht im
[Wiki](https://stratum0.org/mediawiki/images/9/92/Finanzbericht.pdf)
abrufbar.

Der Bericht deckt den Zeitraum vom 01.05.2021 bis 15.01.2022 ab.

* Helena teilt ihre Präsentation im OpenSlides, sodass diese von allen Anwesenden
  gesehen werden kann.

### Einleitung
<!-- don't merge header and list -->
* Helena zeigt die Übersicht der Konten:
    * Barkasse mit Pfandkasse
    * Erstattungskasse
        * Mitglieder können dort Anschaffungen bis 50 € mit Unterstützern selber
          erstatten.
        * Befindet sich in der Küche im Space.
    * Matekasse: Für Getränke & Snacks, bei Einkäufen Kassenzettel
      dort in der Kasse hinterlegen.
        * Befindet sich in der Küche im Space.
    * Girokonto mit Rückstellungen und Pfandgeld
        * Rückstellung: Eine Kasse um bei Ausfall von Mitgliedsbeiträgen unseren
          Verpflichtungen nachkommen zu können.
* Neuerungen:
    * Umlaufbeschlüsse werden auf
      [data.stratum0.org/finanz](https://data.stratum0.org/finanz/)
      aufgelistet.
    * Jahresabschluss 2021 ist erfolgt.
      Damit fallen Buchungen aus 2011 unter die 10-Jahres-Frist.
* Spenden
    * Es gab einige Privatspenden und regelmäßige Freifunk-Spenden.
    * Es gab seit der letzten MV keine Firmenspenden.
* Matekasse
    * Es gibt eine neue digitale Strichliste.
      http://strichliste.s0 im Spacenetz
    * Die Papier-Strichliste ist deprecated.
      Helena möchte die Papierstrichliste gern abschaffen.
      Da das neue System funktioniert wird das vermutlich bald passieren.

### Gewinn- und Verlustrechnung
<!-- don't merge header and list -->
* Zahlen sind für den Zeitraum seit der letzten MV bis zum 15.01.2022

* Ideeller Bereich: Allgemein
    * 15.000 € Mitgliedsbeiträge bekommen
    * 500 € Spenden bekommen
    * Dem gegenüber: rund 1.000 € Ausgaben

* Ideeller Bereich: Projekte
    * Aktuell im Plus.
    * Aber beim Laser sind Ausgaben für z.B. Strom nicht mit einberechnet.
    * Man solle daher die empfohlene Spende für Laserminuten beachten.

* Ideeller Bereich: Space
    * In der Regel in erster Linie Ausgaben für Miete.
    * Es gab eine Rückzahlung für Strom und Nebenkosten.
    * Ausgaben 11.000 €
    * Die Ausgaben sind aber deutlich unter den Einnahmen durch Mitgliedsbeiträgen.

* Im letzten Jahr gab es keine Buchungen in den Geschäftsbereichen
  Vermögensverwaltung und Zweckbetriebe.

* Wirtschaftlicher Geschäftsbetrieb:
     * Zur letzten MV waren Getränke abgelaufen.
       Die abgelaufenen Getränke wurden verschenkt.
     * Inzwischen wurden Getränke nachgekauft, dadurch ist die Matekasse
       im letzten Jahr etwas im Minus.
       Das bewegt sich aber in einem angemessenem Rahmen.

* Mankobuchungen:
    * Seit der letzten MV gab es keine Mankobuchungen.
    * Mankobuchungen werden notwendig, wenn Fehler in den Kassen auftauchen.
    * Alle Differenzen zwischen Kasse und Kassenbuch konnten aber geklärt werden ohne, dass
      Mankobuchungen notwendig waren.

* Umbuchungen:
    * Es wurden hauptsächlich Umbuchungen zwischen
      Matekasse und Barkasse vorgenommen.
      Des ist zu einem großen Teil wegen der Getränke, die nach dem HOA
      gekauft wurden.
    * Es gab aber auch Umbuchungen von der Barkasse in die Erstattungskasse.
    * Die Summe in diesem Geschäftsbereich ist natürlich 0 € :-)

* Gesamt:
    * Der Verein hat seit der letzten MV 4.000 € Gewinn erwirtschaftet.

Die folgende Tabelle fasst die Zahlen aus Helenas Präsentation noch einmal zusammen:

\begin{longtable}{lr>{\textcolor{red}\bgroup}r<{\egroup}r}
  \textbf{Bereich} & \textbf{Einnahmen [€]} & \textbf{Ausgaben [€]} & \textbf{Saldo [€]} \\
  \midrule
  \endfirsthead
  \multicolumn{3}{c}{\emph{(Fortsetzung von vorheriger Seite)}} \\
  \\
  \textbf{Bereich} & \textbf{Einnahmen [€]} & \textbf{Ausgaben [€]} & \textbf{Saldo [€]} \\
  \midrule
  \endhead
  \\
  \multicolumn{3}{c}{\emph{(Fortsetzung auf nächster Seite)}} \\
  \endfoot
  \endlastfoot
  Ideeller Bereich: Allgemein       & 16.014,72  &  -1.043.39 &  14.971,33 \\
   Mitgliedsbeitrag                 & 15.502,56  &      00,00 &  15.502,56 \\
   Pfandgeld Schlüssel              &     00,00  &      00,00 &      00,00 \\
   Spenden                          &    512,16  &       0,00 &     512,16 \\
   Verein allgemein                 &      0,00  &     -70,71 & \textcolor{red}{-70,71} \\
   Kontoführungsgebühren            &      0,00  &    -164,80 & \textcolor{red}{-164,80} \\
   Vereinsserver                    &      0,00  &    -807,88 & \textcolor{red}{-807,88} \\
  \midrule
  Ideeller Bereich: Projekte        &    847,00  &    -370,56 &     476,44 \\
   3D-Drucker                       &     30,00  &       0,00 &      30,00 \\
   Freifunk                         &    498,90  &    -295,89 &     203,01 \\
   Laser-Cutter                     &    282,50  &     -24,87 &     254,63 \\
   Maker vs Virus                   &      5,00  &     -49,80 &     -44,80 \\
   Schneidplotter                   &     30,60  &       0,00 &      30,60 \\
  \midrule
  Ideeller Bereich: Space           &    417,49  & -11.561,62 & \textcolor{red}{-11.144,13} \\
   Einrichtung                      &     00,00  &  -1.080,27 & \textcolor{red}{-1.080,27} \\
   Miete und Nebenkosten            &    417,49  &  -8.704,53 & \textcolor{red}{-8.287,04} \\
   Verbrauchsmaterial               &      0,00  &  -1.776,82 & \textcolor{red}{-1.776,82} \\
  \midrule
  Mankobuchungen                    &      0,00  &       0,00 &       0,00 \\
  \midrule
  Umbuchungen                       &  1.360,00  &  -1.360,00 &       0,00 \\
  \midrule
  Wirtschaftlicher Geschäftsbetrieb &  1.411,73  &  -1.528,38 & \textcolor{red}{-116,65} \\
  \midrule
  Zweckbetriebe                     &      0,00  &       0,00 &       0,00 \\
  \midrule
  \midrule
  \textbf{Gesamt}                   & \textbf{20.050,94} &  \textbf{\textcolor{red}{-15.863,95}} & \textbf{4.186,99} \\
\end{longtable}

### Vorhandenes Kapital
<!-- don't merge header and list -->

* Übersicht vorhandenes Kapital im letzten Jahr:
    * Helena weist darauf hin, dass man den Mitgliedsbeitrag wenn möglich nicht monatlich
      überweisen soll. Wenn das einem Mitglied allerdings nicht möglich ist, ist natürlich
      auch eine monatliche Zahlung sinnvoll.

* Übersicht vorhandenes Kapital seit Beginn des Vereins:
    * Man sieht stetiges Wachstum.
    * Es gibt eine Nachfrage nach starken *Ausschlägen* Anfang 2020.
    * Helena klärt auf: Das ist die Spendensammlung für den Laser,
      dann die Anschaffung des Lasers
      und danach Einnahmen und Ausgaben für Maker-vs-Virus.

### Monatliche Einnahmen und Ausgaben
<!-- don't merge header and list -->

* Monatliche Einnahmen (gemittelt) ca. 1.950 €.
* Monatliche Verpflichtungen (gemittelt) ca. 1.100 €.

### Mitgliederentwicklung
<!-- don't merge header and list -->

* Es gab einige Eintritte und einige Austritte.
* Bei Gründung des Vereins waren es 25 Mitglieder.
  Heute sind es 115 Mitglieder.

### Bestände
<!-- don't merge header and list -->

* Konten mit 0 € Bestand werden aktuell nicht mehr geführt.
* Barkasse: Rund 1.000 €
* Erstattungskasse enthält aktuell 140 €.
  Helena ruft uns auf, die Erstattungskasse zu nutzen,
  wenn wir Anschaffungen tätigen wollen.
* Girokonto: 21.800 €

Die folgende Tabelle fasst die Bestände aus Helenas Präsentation noch
einmal zusammen:

\begin{longtable}{llr}
  \textbf{Nr.} & \textbf{Name} & \textbf{Bestand zum 15.01.2022 [€]} \\
  \toprule
  \endfirsthead
  100          & Barkasse                              &    1.078,03 \\
   100-1       & 3D-Drucker Filamentspenden            &        0,00 \\
   100-2       & Pfand für physische Schlüssel         &      140,00 \\
   100-3       & Spenden für Plotter-Material          &        0,00 \\
   100-4       & Spenden für Stick-Material            &        0,00 \\
  \midrule
  101          & Erstattungskasse                      &      114,44 \\
  \midrule
  102          & Matekasse                             &      296,70 \\
  \midrule
  200024917    & Girokonto                             &   21.845,89 \\
   200024917-1 & Rückstellungen Girokonto              &    3.000,00 \\
   200024917-2 & Pfandgeld auf Girokonto               &      360,00 \\
  \bottomrule
               &  \textbf{Summe}                & \textbf{ 26.835,06} \\
\end{longtable}

### Aktuelle Rückstellungen
<!-- don't merge header and list -->

* Für Ausfall von Mitgliedsbeiträgen, für das Bedienen der Verpflichtungen aus Miete:
  3.000 €.
* Die Rückstellung für die Erhöhung der Mietsicherheit sollte schon länger aufgelöst werden.
  Das ist im letzten Jahr passiert.

### Ausblick
<!-- don't merge header and list -->

* Einnahmen sind immer noch höher als die Ausgaben.
  Helena ruft dazu auf mehr Geld auszugeben.
* Aufbewahrungsfrist: 10 Jahre sind um.
  Nach der MV müssen alte Dokumente vernichtet / gelöscht werden.
* Es wird eine Matekasseninventur geben.
  In dem Zuge soll auch die Papierstrichliste aufgelöst werden
* Helena rechnet für dieses Jahr mit einer Steuererklärung.

### Fragen
<!-- don't merge header and list -->

* mjh: Gibt es eine detaillierte Abrechnung bezüglich der Getränke vom HOA?
    * Helena: Ja, gibt es.
* mjh: Hat vorhin mitgenommen, dass ein Minus bei den Getränken aufgetaucht ist.
  Und das es auch noch eine weitere Kasse für Getränke gab, in die auch eingezahlt wurde.
    * Helena: Es gab nach dem HOA noch eine weitere Kasse neben den HOA-Getränken.
      Die wurde aber in die Matekasse eingebucht.
      Die Getränke vom HOA wurden der Veranstaltungs-UG abgekauft
      und dann weiterverkauft.
* Es gibt keine weiteren Fragen.

## Tätigkeitsbericht des Vorstandes (larsan)

larsan stellt die wesentlichen Punkte der Vorstandsarbeit seit der letzten MV am 
08.05.2021 vor.

Die Slides sind im
[Wiki](https://stratum0.org/mediawiki/images/6/69/Taetigkeitsbericht2021.pdf)
abrufbar.

### Übersicht
<!-- don't merge header and list -->

* larsan weißt zu Beginn darauf hin, dass die Auszählung der Wahl erst Anfang Juni 2021 war, 
  sodass der alte Vorstand nach der letzten MV noch eine Weile tätig war.
* larsan stellt die Funktionen im Verein vor:
    * Rechnungsprüfer
        * Werden von der MV bestellt.
        * Seit der letzten MV: Jan Lübbe und Angela Uschok
    * Vertrauensentitäten
        * Werden von der MV bestellt.
        * Seit der letzten MV: Mika Eichmann und Daniel Sturm
    * Vorstand
        * Wird von der MV gewählt.
        * Vorsitzender: Lars Andresen (larsan)
        * Stelv. Vorsitzender: Chris Fiege (Chrissi^)
        * Schatzmeisterin: Helena Schmidt (ktrask)
        * Beisitzerin: Linda Fliß (chamaeleon)
        * Beisitzer: Christopher Helberg (Fototeddy)
        * Beisitzer: Jonas Martin (Lichtfeind)

### Vorstandsarbeit 
<!-- don't merge header and list -->

* Von den Vorsitzenden + Schatzmeister sind jeweils zwei zusammen 
  vertretungsbrechtigt.
* Aufgabenverteilung im Vorstand:
    * Schatzmeisterin: ktrask
    * Vertretung der Schatzmeisterin: Chrissi^
    * Verwaltung von Schlüsseln: larsan
    * Mitgliederverwaltung: ktrask + Chrissi^
    * Schriftführung: Chrissi^
* Arbeitsweise: Der Vorstand nutzt mehrere Kommunikationswege, 
  um sich abzustimmen:
    * Wiki zur Planung der MV
    * Mailingliste vorstand@stratum0.org zur internen Kommunikation
    * Threema-Gruppe
    * IRC / Matrix

* Arbeitstreffen: am 1. Dienstag im Monat, sind öffentlich

* Vorstandssitzungen: 
    * Werden mindestens quartalsweise abgehalten.
    * Sind öffentlich, bis auf ein paar wenige Themen.
* Vorstand hat eine Geschäftsordnung:
    * Ist im Wiki hinterlegt.
    * Diese regelt wichtige Punkte der Arbeitsweise.

* Vorstandsarbeit in Zahlen:
    * Dieses Mal: nur 8 Monate
    * Der Vorstand davor war 16 Monate im Amt
    * Normalerweise sollte ein Vorstand ca. 12 Monate im Amt sein.
    * Die Verschiebungen der Dauer sind pandemiebedingt.
    * Durch die kürzere Zeit gab es nur vier Vorstandssitzungen.
    * 380 Mails auf der Mailingliste in 160 Threads. 
      Das ist wesentlich weniger als zuvor.
    * Nur 20 Umlaufbeschlüsse.
      Zuvor über 3x so viele.
    * Dazu viele Seiten Vorstandsprotokolle, die im Wiki verlinkt sind.

|
### Besondere Themen
<!-- don't merge header and list -->

* Corona
    * Nutzung des Spaces seit März 2020 eingeschränkt.
    * Ziel der Regelungen war und ist: 
      Space offen halten und gleichzeitig Teil des Infektionsschutz sein.
    * Die Corona-Regeln wurden vier mal an die Regeln des Landes angepasst:
        * Teilweise Pad mit Nutzungs-Registrierungen.
        * Die Nutzung des Pad wurden später wieder aufgehoben.
        * Jetzt CoronaWarnApp (oder Papierzettel) und 2G.

* Versicherungen
    * Der Verein hat seit 2014 eine Haftpflichtversicherung. 
      Die wollen wir jetzt auf den neusten Stand bringen.
      Chrissi^ hatte sich mit dem Versicherungsmenschen getroffen.
    * Jetzt soll eine Inhaltsversicherung dazu kommen:
        * Gegen Feuer, Leitungswaser, Sturm/Hagel
        * Einbruchdiebstahl wollen wir nicht aufnehmen, da der Space 
          eh meistens offen und jemand da ist. 
          Außerdem ergeben sich daraus Anforderungen an die Schließanlage.
        * Gesamtversicherungssumme: 100.000€
        * Kosten wären dafür 165€ / Jahr, das wäre nicht viel höher als die
          Haftpflichtversicherung.
        * Rückfragen laufen gerade zurück zum Versicherungsmenschen.

* Arbeitssicherheit
    * Das Thema wurde auch schon unter dem vorherigen Vorstand bearbeitet.
    * Großer Teil: Elektrische Sicherheit wurde in 2020 durchgeführt.
        * Prüfung der ortsfesten Elektroinstallation müsste durch den Vermieter erfolgen
          und steht noch aus.
        * Da gibt es aber wieder ein Problem mit der Hausverwaltung:
          Die Kommunikation ist schwierig.
    * Brandschutz:
        * Es gibt jetzt mehr Feuerlöscher und die sind beschildert worden.
        * Es gibt eine Zwischenfrage zum Feuer und der Inhaltsversicherung:
          Es wird gefragt, ob diese nur die Schäden durch das Feuer, oder auch die
          Schäden durch Löschwasser abdeckt.
          Der Vorstand kann bestätigen: Es werden auch Schäden durch Löschwasser
          abgedeckt.
        * Wir haben nun Rauchmelder und einen Wärmemelder in der Küche. 
          Die sind untereinander per Kabel vernetzt.
    * Es gibt einen Safe für LiPo-Akkus. Man sollte solche Akkus eher in dem Safe lagern.
      Der muss jetzt noch gefüllt werden.
      Defekte Akkus, sollten eher nicht im Space gelagert werden.
    * Geräte mit Brandgefahr (z.B. 3D-Drucker):
        * So lange jemand im Space ist können die Drucker laufen.
          Aber diese dürfen nicht unbeaufsichtigt weiterdrucken. 
          Die druckende Entität ist verantwortlich, dass die Drucker nicht 
          unbeaufsichtigt weiterlaufen.
        * Der Lasercutter darf nur unter permanenter Aufsicht genutzt werden.
    * Fluchtwege:
        * Hinter der Küche gibt es nun eine Leiter, sodass man hier einen 
          zweiten Fluchtweg vom Küchenfenster auf das nächste Dach hat.
          Es gibt Beschilderung der Fluchtwege in allen Räumen.
        * Es gibt jetzt eine neue Fluchtwegebeleuchtung im Space-Flur.
    * Verletzungsgefahr:
        * Es gibt jetzt eine Space-Apotheke und einen versiegelten Verbandskasten.
          Die Prüfintervalle des Verbandskasten werden durch den Vorstand
          regelmäßig kontrolliert.

### Große Ausgaben im letzten Jahr
<!-- don't merge header and list -->

* Im letzten Jahr wurden die folgenden große Ausgaben getätigt:
    * Zum HOA wurde Technik zum Streamen von Vorträgen für ca. 600 € angeschafft.
    * Es wurde ein Dickenhobel für ca. 510 € angeschafft.
    * Es wurden ca. 500 € für Brandschutzdinge ausgegeben.
    * Verbrauchsmaterialien (Zusammenfassung verschiedener Einzelposten),
      z.B. Schutzgas, Schleifmittel, Schrauben und Dübel für ca. 1000 € angeschafft.

### Fragen
<!-- don't merge header and list -->

* Fragen:
    * mjh: Wie viele Mitgliedsanträge wurden vom letzten Vorstand abgelehnt?
       * larsan:  Es wurde ein Mitgliedsantrag abgelehnt.
    * Chrissi^: 
      Weist beim Thema Arbeitssicherheit darauf hin, dass wir versuchen das 
      mit Augenmaß zu machen und freuen uns auf Hinweise/Diskussion zum Thema.
    * Es gibt keine weiteren Fragen.

## Jahresbericht (larsan)

larsan stellt in seinem Jahresbericht 2021 die wesentlichen Ereignisse
für den Verein vor.

Die Slides sind im
[Wiki](https://stratum0.org/mediawiki/images/e/e7/Jahresbericht2021.pdf)
abrufbar.
(Anmerkung der Protokollführung:
Da die Folien viele Fotos enthalten lohnt sich ein Blick auf jeden Fall!)

### Bürgermedallie
Dem Stratum 0 wurde im Jahr 2021 die Braunschweiger Bürgermedallie verliehen.

* Kandidaten für die Bürgermedallie werden dabei von den Fraktionen des Rates
  beim Bürgermeister vorgeschlagen.
* Wir waren bei der Verleihung im Garten des *Hauses der Braunschweigischen Stiftungen*
  mit zehn Entitäten vor Ort.
* Die Bürgermedallie wurde dieses Jahr an insgesamt vier Preisträger verliehen:
  Zwei Einzelpersonen, zwei Vereine.
* Infolgedessen wurden wir auch zum Neujahrsempfang-Sommmerfest der Stadt eingeladen.

### Gruppen / Veranstaltungen / Projekte im Space:
<!-- don't merge header and list -->

* Digitalcourage: Hauptsächlich online
* Malkränzchen: Hauptsächlich online
* CoderDojo: Gab es noch nicht wieder
* Coptergruppe: online / nicht im Space
* Bierbrauen: Es wurde ein mal Bier gebraut
* 1x Seilklettern in der Werkstatt
* Vorträge: aktuell noch online
* Freifunk:
    * War mit dem Event-Netz auf dem Südstadt Open Air
    * Umstellung des Netzes auf das neue Backend (Project Parker) war erfolgreich.
* Hey, Alter!
    * Seit kurz nach Pandemiebeginn: Laptops verteilen an Schüler die keine haben.
    * Sind jetzt ein eigener Verein. Haben ihre Räume im Torhaus an der Oker.
    * Es besteht personelle Überschneidung zum Stratum 0.
      larsan hätte nichts dagegen, wenn die noch größer würde.

### Neues im Space:
<!-- don't merge header and list -->

* Die Kammer des Schreckens (die Ecke mit der Schließtechnik für die Space-Tür)
  wurde überarbeitet: Es gibt einen neuen Pi, mit eigenem Hat,
  jetzt NodeRed als Software. Ist vorbereitet für weiteren Ausbau.
* Es gibt eine neue Matekasse, wie eben im Finanzbericht schon erwähnt.

* Gastrotechnik wurde im Zuge des HOA ausgelagert. 
  Lagert jetzt bei Reneger.
  Das Lager dort im Zuge des HOA ausgebaut.
* Ein Lager, z.B. eine Garage in Spacenähe, wäre trotzdem weiterhin gut.
* Hierzu wird gefragt, was dort eingelagert wurde.
  Chrissi^ klärt auf: Nur Eigentum der SMFW UG, das vorher im Space war.
  Es wurden keine Sachen dort eingelagert, die dem Space gehören.
  Wenn wir Material von dort brauchen können wir das aber auch zurück holen.

### Ongoing Projekte
<!-- don't merge header and list -->

* Werkstattüberarbeitung
* Druckluft
* Absaugung

### Rückkehr zur Vor-Corona-Nutzung der Räume im Space
<!-- don't merge header and list -->

* Stickecke wurde in den Frickelraum zurückgebaut.
* Es sind noch einzelne Maker-vs-Virus-Reste im Space übrig.

### HOA 2021
<!-- don't merge header and list -->

* Eines der wenigen großen Events in 2021.
* Es gab Planungsunsicherheit. 
  Wir waren aber klein und flexibel genug unsere Veranstaltung trotzdem durchführen
  zu können.
* Es gab vor Ort regelmäßige Corona-Tests und Kontrollen beim Einlass.
* Frage ist nun: Wird es ein HOA 2022 geben?

### Erwähnenswertes
<!-- don't merge header and list -->

* Die Versicherung ist nun in der Lage zur E-Mail-Kommunikation.
  Allerdings ist das Feld zum Angeben der E-Mail Adresse in einem der
  Formulare auf 20 Zeichen begrenzt, sodass *vorstand@stratum0.org* zu lang ist...
* RC3
    * Wir waren diese mal digital vertreten, dafür Dank an stew
* Hausverwaltung:
  larsan erzählt die Geschichte unserer Ansprechpartner.
  Jetzt haben wir das Problem mal wieder, dass unsere Hausverwaltung schlecht erreichbar ist.
* Stratum 0 wird 10 Jahre alt.
  Wir würden das Feiern wollen; aber das geht gerade nicht.
  Wir wollen das aber nachholen!

### Danke an Entitäten
<!-- don't merge header and list -->

* Server-Administratoren
* Vorbereitung der MV
* Getränkeeinkauf
* Vorträge vorbereiten, aufzeichnen und unseren Youtube-Kanal pflegen

|

* Fragen:
    * keine

## Bericht der Vertrauensentität

* Fia, als eine der Vertrauensentitäten, berichtet über die Tätigkeit.
* Zu Beginn wird erklärt, was die Aufgaben der Vertrauensentitäten sind.
* Aktuell sind Fia und dStulle Vertrauensentitäten.
* Fia berichtet, dass es nichts zu berichten gibt.
* Es gab eine Diskussion zu einem älteren Thema.
  Dort konnte aber keine Klärung herbeigeführt werden.

|

* Fragen:
    * Keine

## Bericht der Rechnungsprüfer

Angela übernimmt den Bericht der Rechnungsprüfer.

* Es gab keine Auffälligkeiten.
* Es wurden viele TODOs erledigt.
  Eins sogar aus 2016.
* Angela erklärt das Vorgehen für Prüfungen in diesem Jahr:
    * Es gab vier Treffen im letzten Jahr.
    * Durch Corona war Januar 2021 die Prüfung nur im Mumble.
    * Vor der Mitgliederversammlung 2021 waren die Kassenprüfer
      im Space und hatten somit Zugriff auf die Unterlagen.
      Chrissi und ktrask waren im Mumble dabei.
    * Ein weiteres Treffen fand im Oktober im Space statt.
    * Es wird stichprobenartig gerüft, ob die angeschafften Dinge
      tatsächlich im Space angekommen sind.
* Die Kassenprüfer bitten um folgende Änderungen:
    * Bei Sammelanträgen:
      In den Vorstandsprotokollen sollte dokumentiert werden,
      wozu die Dinge angeschafft wurden.
      Somit soll gut nachvollziehbar sein wozu Dinge angeschafft
      werden.
    * Mitgliedsbeiträge sollen, wenn möglich, mit wenigen Überweisungen
      bezahlt werden.
      Das erleichtert die Arbeit, da jede Buchung einzeln geprüft werden muss.
      Es werden aber auch einzelne Überweisungen weiterhin gerne geprüft,
      wenn einem Mitglied die Umstellung nicht möglich ist.

|

* Fragen:
    * keine

|

* Die Rechnungsprüfer empfehlen die Entlastung des Vorstandes, da sie keine Probleme entdeckt haben.

# Entlastung des Vorstands

* Ein Meinungsbild ergibt, dass niemand eine einzelne Entlastung des Vorstands wünscht.
* Im OpenSlides wird eine Abstimmung für eine gemeinsame Entlastung des Vorstands angelegt.
* Da im OpenSlides keine Vorkehrungen getroffen wurden, die verhindern, dass die Vorstände
  nicht abstimmen können, wird noch einmal dazu aufgerufen, dass die sechs bisherigen Vorstände
  nicht mit abstimmen sollen.
  Sollte die Abstimmung kein eindeutiges Ergebnis ergeben, so würde die Abstimmung noch
  einmal mit entsprechenden Maßnahmen wiederholt.
  Das entspricht dem Vorgehen vom letzten Jahr.

* Die Abstimmung wird bei 24 abgegebenen von 30 möglichen Stimmen beendet.
* Ergebnis:
    * Ja: 24 Stimmen
    * Nein: 0 Stimmen
    * Enthaltung: 0 Stimmen

* Der Vorstand ist damit entlastet.

# Änderungsanträge

* In der Einladung wurden keine Änderungsanträge aufgestellt.
* Damit gibt es hier keine Anträge abzustimmen.


# Glaskugelei, Feedback und Input für den zukünftigen Vorstand

Vor diesem TOP werden 15 Minuten Pause eingeschoben.
Die Veranstaltung geht um 15:55 weiter.

larsan bittet um Input für den neuen Vorstand, den Verein und
auch sonst alles.

* Space 3.0 oder Space 4.0
    * fototeddy: Fragt nach, wie der Stand bei dem möglichen neuen Raum ist, der uns
      angeboten wurde.
    * larsan: Baubeginn erst 2024.
      Aber vermutlich kann man dort mal nachhaken.
    * rohieb: Vermutlich sollte man eher früher als später nachfragen.
      Das gibt uns dann früher mehr Klarheit.

* Was ist aus dem Thema *Schrebergarten* geworden?
    * lichtfeind: Hatte damals telefoniert.
      Damals wäre bei uns in der Nähe nichts zu haben gewesen.
      Problem: Wir hätten einen festen Ansprechpartner nennen müssen.
      Durch Krankheit ist das Thema dann von der Agenda gekommen.
      Es gab damals aber keine Fristen. Man kann das also vielleicht noch einmal aufwärmen.

* Es wird vorgeschlagen, dass Corona weggehen soll.

* rohieb: Früher wurden viele Mitglieder über die Uni gewonnen.
  Das war jetzt nicht möglich.
  Wie sind neue Mitglieder jetzt dazu gekommen?
    * larsan: Einzelne sind über Maker-vs-Virus und Hey Alter! dazugekommen.
      Der Rest wurde vermutlich von bestehenden Mitgliedern mitgebracht.
    * Chrissi^: Es gab eine gewisse Stagnation währen der Pandemie, aber es wäre
      schön in Zukunft wieder mehr Aufmerksamkeit zu erzeugen.
      Zum Beispiel durch Coder-Dojo und ähnliche öffentlichkeitswirksame Veranstaltungen.
    * fototeddy: Hofft, dass mit mehr Space-Betrieb auch wieder mehr Interesse am Space entsteht.
      Dazu kommt, dass man gerade nicht viel von dem zeigen kann, was den Space in
      normalen Zeiten ausmacht.

* Tilman: Wünscht sich ein HOA 2022.
  Denkt, dass es an der Zeit ist in die Planung einzusteigen.
    * larsan: Einen Termin fürs HOA gibt es bereits.

* Monsieur Nitch: Greift das Thema Außenwirkung auf:
  Hat den Eindruck, dass sich mit der Pandemie Wissenschafts- und Technikfeindlichkeit ausgebreitet hat.
  Fragt sich, ob man sich da nicht engagieren könnte.
    * larsan: Frage ist: wie?
    * Chrissi^: Schlägt vor, sich dazu mal zusammenzusetzen und zu überlegen, wie es
      weiter gehen kann.

# Wahlen

## Wahl des Vorstands

* larsan gibt die Versammlungsleitung an Kasa ab.
* Kasa: Bedankt sich für das Vertrauen.

### Wahlverfahren
<!-- don't merge header and list -->

* Kasa stellt fest, dass die Wahl in diesem Jahr wieder als Briefwahl durchgeführt wird.
* Bei der Akkreditierung wurde bereits eine aktuelle Post-Adresse von allen akkreditieren
  Mitgliedern eingesammelt.
* Akkreditiere Mitglieder bekommen nach der Mitgliederversammlung einen Brief mit
    folgendem Inhalt:
    * Anschreiben mit Anleitung
    * Wahlschein: Muss unterschrieben und dann zusammengefaltet werden.
    * Stimmzettel in A5.
      Der Stimmzettel darf nicht besonders gekennzeichnet werden, da dieser sonst
      ungültig ist.
    * Stimmzettelumschlag:
      da gehört der Stimmzettel rein.
    * Frankierter Rückumschlag:
      Dort Stimmzettelumschlag und Wahlschein einlegen
      und zurücksenden.

|

* Kasa stellt den Wahlmodus *Zustimmungswahl* vor.
    * Man kann für jeden der aufgestellten Kandidaten stimmen.
    * Für weitere Details verweist Kasa auf die Geschäftsordnung.
    * Wahlhelfer: Kasa schlägt  Shim und drc vor.
    * Die Wahlleiter und Wahlhelfer versenden die Wahlunterlagen und zählen diese
      gemeinsam aus.
    * Auszählung wird am 05.02.2022 erfolgen.
    * Nach der Erfahrung aus dem letzten Jahr sollten 2 Wochen Postlaufzeit ausreichen.

|

* Kasa fragt, ob er oder die Wahlhelfer sich vorstellen sollen.
* Kasa fragt noch einmal, ob es eine Ablehnung gegen die Wahlhelfer gibt.
* Zu beiden Fragen gibt es keine Wortmeldungen.

### Kandidatenliste
<!-- don't merge header and list -->

* Kasa stellt die Kandidatenlisten zusammen.
  Es ergibt sich folgende Kandidatenliste:

|

* Vorstandsvorsitzender
    * larsan

* Stelv. Vorsitzender
    * Chrissi^

* Schatzmeister
    * ktrask

* Beisitzer
    * lf
    * Chrissi^
    * fototeddy

* Tilman weist darauf hin, dass die Beisitzer unterbesetzt sind,
  falls Chrissi^ als stellv. Vorsitzender gewählt wird.
    * Kasa: Wir müssen keine drei Beisitzer wählen. Unterbesetzt wäre also auch OK.
    * Tilman stellt sich als Beisitzer auf.

|

* Es gibt eine Vorstellrunde, in der sich die Kandidaten einzeln vorstellen und die Mitglieder
  Fragen stellen können.

* Kasa fragt noch einmal, ob sich weitere Kandidaten aufstellen lassen wollen.
  Das ist aber nicht der Fall.
  Kasa schließt die Kandidatenlisten.

## Wahl der Rechnungsprüfer

* Kasa fragt, ob die Rechnungsprüfer neu gewählt werden sollen.
    * Es gibt keine Wortmeldung
    * Kasa fragt sonnenschein und Sho, ob sie weiter machen wollen.
    * Beide wollen das weiter machen.
      Falls jemand anders möchte, würden die beiden aber auch den Vortritt lassen.
    * Das scheint nicht der Fall zu sein und es wurde keine Neuwahl gewünscht.
    * Die Rechnungsprüfer bleiben Sho und sonnenschein.

|

* Kasa gibt die Versammlungsleitung zurück an larsan.

## Wahl der Vertrauenspersonen

* larsan fragt, ob die Vertrauenspersonen neu gewählt werden sollen.
* Fia ist bereit das Amt weiter zu machen
* dStulle möchte das Amt gerne abgeben.
* Es wird überlegt, wie es jetzt weiter geht.
* Chrissi^ weist darauf hin, dass die MV  letztes Jahr beschlossen hat,
  dass es zwei Vertrauenspersonen geben soll.
  Daher sollte nun mindestens eine neue Person gewählt werden.
* Im OpenSlides wird eine Wahl angelegt.
  Es werden weitere Kandidaten gesucht.
* Neben Fia lässt sich keine weitere Person aufstellen.
  Die Mitgliederversammlung fährt mit einer Wahl für eine einzelne Vertrauenspersonen fort.
* Die Wahl wird bei 29 abgegebenen von 29 möglichen Stimmen beendet.
* Ergebnis:
    * Fia: 28 dafür, 0 dagegen, 1 Enthaltungen

# Sonstiges

## Meinungsbild zur Anschaffung "Shaper Origin"

* larsan fragt nach Meinungen zu dem Thema.
* fototeddy: Das Gerät ist eine handgeführte CNC-Fräse.
  Die Spindel kann sich halbwegs frei im Gerät bewegen.
  Damit bekommt man ca. 1/10 Millimeter Genauigkeit.
  Man benötigt ein Klebeband als Verbrauchsmaterial.
  Es ist aber kein alternatives Produkt bekannt.
  Fototeddy meint, dass das Gerät einfach zu bedienen sein wird, da der Zielmarkt
  weniger Computer-Nerds, sondern eher Handwerker sind.
  Fototeddy stellt zur Diskussion, ob man einen Teil crowdfunden möchte,
  oder ob man das aus der gut gefüllten Space-Kasse nehmen will.
* Björn Kolax (Chat): Man kann solche Abträge auch mit anderen Geräten mit vertretbarem
  Mehraufwand hinbekommen. Cool, aber der Kauf ist nicht gerechtfertigt.
* fototeddy: So genau Fräsen, wie der Shaper das kann, ist von Hand nicht möglich.
* Pecca (Chat): Kennt das Gerät von ihrer Arbeit. Vorteile sind:
  Man kann damit große Sachen bearbeiten und sie gut verstauen, da das Gerät selber klein ist.
* Doom (Chat): Für 4.000 € kann man sich auch einen CNC-Tisch kaufen.
  Allerdings braucht der entsprechend viel Platz.
* Fototeddy: Es braucht keinen generierten G-Code, sondern man kann da einfach SVGs hochladen.
  Den Rest macht das Gerät.
* Pecca (Chat): Ist auch für Laien einfach zu bedienen.
* Rohieb (Chat) fragt nach dem Online-Service und potentiellem Vendor-lock-in.
* Fototeddy: Der Online-Service ist nur zum Teilen von Designs.
  Das Gerät kommt ohne aus.
* Flaxo (Chat): Wie kommen die Grafiken darauf?
* Doom: Man kann das Gerät zu einem Account verbinden.
  Dann ist das bei denen integriert.
  Es gibt auch eine App.
  Doom kennt keinen Weg dort vom PC aus direkt Sachen drauf zu legen.
  Allerdings sind Dooms Erfahrungen auch schon länger her.
  Eine Rolle Klebeband (40-45m) kostet 15€, wie viel man braucht kommt drauf an wie
  eng man das klebt.
  Es sind verschiedene Spacings möglich.
  Man braucht schon relativ viel davon.
  Es gibt auch einen Tisch in den man den Shaper und kleine Werkstücke einspannen kann,
  dann braucht man kein Tape. Man kann aber nur am Rand fräsen
* Rohieb fragt, wie viele Leute dafür einen Anwendungsfall haben.
  Er selbst sieht nur so ein mal im Jahr eine Nutzung, was aktuell auch mit der Portalfräse geht.
* Im Chat sagen 4 bis 5 Personen, dass sie sich eine regelmäßige Anwendung vorstellen können.

* Sho: Was wären Projekt-Ideen?
    * Fototeddy: Fräsen von Verbindungen und Intarsien.

* Helena schlägt ein Crowdfunding über 33% oder 25% vor.
  Damit können die, die es viel benutzen einen größeren Anteil geben.
  Und man bekommt noch einmal ein Meinungsbild, wie groß die Unterstützung ist.
    * Es gibt dazu einige Pro-Meldungen im Chat.

* Sho: Welche Materialien kann man damit bearbeiten?
    * Doom: Eigentlich nur Holz oder harter Schaum
      Beworben wird es aber nur für Holz.
      Sieht das aber auch als größten Nachteil des Gerätes gegenüber einer "echten" CNC.

* rohieb: Schlägt statt einem prozentualen Limit eine Anzahl an finanziellen Unterstützern vor.

* larsan schließt die Feedbackrunde.
  Für weiteren Feedback verweist larsan auf die Diskussion auf dem Normalverteiler.
  larsan bittet noch eine Zusammenfassung der Diskussion auf den Normalverteiler zu senden.

## larsan: Lastenrad für den Space?

larsan fragt, ob es noch weitere sonstige Themen gibt.

* larsan: Da wurde schon nach gesucht.
  Aber gerade E-Lastenräder sind teuer.
* larsan fragt nach Meinungen.
* rohieb: Eventuell gibt es Steuerbegünstigungen und Förderungen von der Stadt und dem Land.
  Bis zu 1/3 des Preises für geschäftstreibende und Vereine.

* Chamaeleon: Wo würden wir das Lastenrad hinstellen?
* Es gab mal Pläne eine Rampe zum Fahhradkeller zu bauen.

* mjh:
  Findet Lastenräder immer sehr klobig.
  Regt an, das man auch Fahrradanhänger bauen und Kupplungen an Fahrrader bauen kann.

* Frage aus dem Chat: Kann man Lastenräder leihen?
    * rohieb: Es gibt den LASTENLÖWEN.
      Die Fahrrader wandern aber durch die Stadt.
      Man benötigt Vorlauf für Termine.

* Monsieur Nitch: Wie oft müssen wir Dinge von der Größe des Lastenrades besorgt werden?
* ktrask: Es wäre cool für Geränkeeinkäufe, das würde es für Leute ohne Auto leichter
  machen Getränke einzukaufen.
* rohieb: Verleiht sein Lastenrad auch tageweise. Man kann ihn einfach ansprechen.
* Zoyo: Fragt sich noch, was der wirkliche Vorteil eines Lastenrades gegenüber eines
  Anhängers ist.
  Vermutet, dass die meisten Lasten auch auf einen Anhänger passen.
* Im Chat wird eine Kombination aus E-Bike und Anhänger vorgeschlagen.
* Pecca: Haben wir aktuell ein funktionierendes Space-Fahrrad?
* Im Chat wird noch über Fahrradanhänger mit Antrieb diskutiert.
* Rohieb spricht noch einmal das Thema Versicherung an.

|

* Insgesamt gibt es bei diesem Thema aber keinen Konsens.
  Die Diskussion wird ohne Ergebnis beendet.

## Terminfindung nächste MV

* Chrissi^ schlägt eine hybride MV vor.
* rohieb hat den Eindruck, dass eine MV online länger braucht, da man mehr Wartezeiten hat.
* larsan: Den genauen Modus kann man sich dann vorher noch überlegen.
  Terminvorschlag: Samstag der 21.01.23
* Im Chat melden sich einige, die einen Sonntag bevorzugen.
  Es wird eine Abstimmung im OpenSlides eingerichtet.
* Im Chat wird für Präsenz ein Termin im Sommer vorgeschlagen.
* larsan: Die letzte MV hatte den Termin bewusst wieder in den Winter gelegt.
  Das hat auch den Vorteil, dass man einen schöneren Jahresabschluss bekommt.
* Die Abstimmung im OpenSlides wird gestartet.
* Es wird wie folgt abgestimmt: Samstag: Ja, Sonntag: Nein, Enthaltung ist möglich
* Die Abstimmung wird bei 26 abgegebenen von 28 möglichen Stimmen beendet.
* 26 von 28 Stimmen gehen ein:
    * Samstag: 6
    * Sonntag: 8
    * Enthaltungen:  13

* larsan: Das klingt eher nach Sonntag.
  Terminvorschlag: 22.01.2023
* Es gibt keine Wortmeldungen dazu.

## Weltherrschaft

* Dieser Tagesordnungspunkt wird auf die nächste MV verschoben.

## Keysigning-Party

* Dieser Tagesordnungspunkt wird auf die nächste MV verschoben.

## Ehrung aller (erschienenen?) Mitglieder

* larsan: Fragen uns, wo der Tagesordnungspunkt herkommt.
  Der TOP wurde von einer Person in die Vorlage geschrieben, die jetzt nicht anwesend ist.
  Wir wissen jetzt nicht, was wir damit jetzt tun müssen.
* Menschen im Chat fühlen sich geehrt.

|

* larsan bedankt sich noch einmal für die MV und schließt die Versammlung.


# Auszählung der zurückgelaufenen Stimmen

Die Auszählung findet am 05.04.2022 ab 15:00 Uhr im Space statt.

Dieser Abschnitt des Protokolls wird nur von Chrissi^ mitgeschrieben.

### Auszählung
<!-- don't merge header and list -->
* Kasa, Shim und drc sind im Space.
* Die Öffentlichkeit wird über einen BigBlueButton-Raum hergestellt.
* Im BigBlueButton sind drei Mitglieder anwesend.
  Im Space sind zwei weitere Mitglieder anwesend.
* 29 Umschläge sind zurückgekommen
* Zunächst werden die zurückgelaufenen Umschläge geöffnet.
* Die Gültigkeit der Wahlscheine wird geprüft.
* Bei gültigen Wahlscheinen werden die Stimmzettelumschläge von den Wahlscheinen getrennt.
* 29 Wahlscheine sind gültig.
* Anschließend werden die Stimmzettelumschläge geöffnet und die Stimmen werden gezählt.
* 29 Stimmzettel sind gültig.
* Die Ergebnisse werden von Kasa vorgelesen.
  Anschließend  wird die Person mit den jeweils meisten Stimmen gefragt, ob sie
  die Wahl annimmt.
* Einige Personen sind nicht direkt fernmündlich zu erreichen.
  In diesem Fall wird die Abfrage nach einiger Zeit wiederholt
  und bis dahin mit der Verlesung der Ergebnisse fortgefahren.
  Da die Wahl eindeutig ist, wird dies nicht als Problem angesehen.

|

### Stimmen
<!-- don't merge header and list -->
* Vorsitz:
    * larsan: 29 von 29 Stimmen
        * Ist im BigBlueButton, nimmt die Wahl an.
* Stelv. Vorsitz:
    * Chrissi^: 29 von 29 Stimmen
        * Ist im Raum, nimmt die Wahl an.
* Schatzmeisterin:
    * Helena: 29 von 29 Stimmen
        * Wird von Kasa angerufen, geht nicht ans Telefon.
        * Wir machen erst einmal mit den nächsten Posten weiter.
        * Später ruft Helena zurück und nimmt die Wahl an.
* Beisitzer:
    * Lichtfeind: 28 von 29 Stimmen
        * Wird angerufen und ist nicht erreichbar.
        * Kommt später ins BigBlueButton und nimmt die Wahl an.
    * Fototeddy: 26 von 29 Stimmen
        * Wird von Kasa angerufen, nimmt die Wahl an.
    * Tilman: 26 von 29 Stimmen
        * Wird von Kasa angerufen, ist nicht erreichbar.
        * Tilman ruft zurück und nimmt die Wahl an.
    * Chrissi: 20 von 29 Stimmen
        * Hat schon den Posten des Stelv. Vorsitzenden und
          steht damit nicht mehr zur Verfügung.

|

* Die Auszählung wird geschlossen.
