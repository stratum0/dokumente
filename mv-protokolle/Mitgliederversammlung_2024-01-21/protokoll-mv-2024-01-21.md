---
vim: ts=4 et sw=4 tw=90
build-me-with: make pdf
documentclass: s0minutes
papersize: a4
lang: de
toc: true
toc-depth: 2
links-as-notes: true
title: Mitgliederversammlung 2024-01-21
s0minutes:
  typeofmeeting: \generalassembly
  date: 2024-01-21
  place: "online; OpenSlides- und BigBlueButton-Instanzen des Stratum 0 e.V."
  startingtime: "14:18"
  endtime: "18:01"
  attendants: 30 bis 36 stimmberechtigte Mitglieder
  minutetaker: Chrissi\^, drc
---

\setcounter{section}{-3}

Vorbemerkungen
==============

Die Veranstaltung findet in einem Raum auf dem BigBlueButton-Videokonferenzsystem des
Vereins statt.
Abstimmungen werden über OpenSlides vorgenommen.
Beide Systeme werden auf Servern des Vereins von Mitgliedern betrieben und gewartet.

Zugänge zum OpenSlides wurden vor der Mitgliederversammlung an alle aktiven Mitgliedsentitäten
versendet.
Somit sind alle Mitgliedsentitäten, unabhängig von ihrer Stimmberechtigung, in der Lage, an der
Mitgliederversammlung teilnehmen.

Alle ordentlichen Mitgliedsentitäten wurden im Vorfeld der Mitgliederversammlung aufgefordert
eine aktuelle Post-Adresse abzugeben.
Diese Adressen werden später genutzt, um Briefwahlunterlagen zu versenden.
Darüber hinaus haben alle ordentlichen Mitgliedsentitäten eine Stimmberechtigung im Openslides erhalten
und können somit an Wahlen und Abstimmungen während der Mitgliederversammlung teilnehmen.

In diesem Protokoll wird bei jeder Wahl oder Abstimmung die Anzahl der
tatsächlich abgegebenen Stimmen angegeben.
Verlassen stimmberechtigte Mitgliedsentitäten die Versammlung oder kommen stimmberechtigte
hinzu, so verändert sich die Anzahl der möglichen Stimmen für weitere Abstimmungen.
Dazu kommt, dass stimmberechtigte Mitglieder eine Abstimmung aussetzen können, ohne sich
explizit zu enthalten.

Wie unter Hackern üblich werden häufig die Nicknamen von Personen verwendet.
Wo der tatsächliche Name der Person relevant ist, zum Beispiel weil diese Person zur Wahl steht,
wird bei der ersten Nennung zusätzlich der vollständige Name, sowie, wenn zutreffend,
weitere Nicknamen angegeben.

Technik-Test
============

* Wir testen, ob alle Mitgliedsentitäten funktionierenden
  Zugriff auf das OpenSlides haben.
* Dazu wird eine Test-Abstimmung im OpenSlides gestartet.

|

* Beim Test-Antrag werden 33 Stimmen abgegeben:
* Dies passt dazu, dass 33 stimmberechtigte Mitglieder anwesend sind.
* Wir stellen keine schwerwiegenden Probleme fest und fahren mit der Versammlung fort.


Begrüßung und Protokoll-Overhead
================================

Chrissi^ (Chris Fiege, Chrissi) begrüßt die Anwesenden und stellt die ersten TO-Punkte vor.

Wahl der Versammlungsleitung für diese MV
-----------------------------------------

* Die Kandidatenliste im Openslides wird eröffnet.
* Es melden sich die folgenden Kandidaten:
    * larsan (Lars Andresen)
* Es gibt keine weiteren Kandidaten.
* Die Abstimmung beginnt 14:24.

| 

* Es werden 27 Stimmen abgegeben.
  * larsan: 27 Stimmen
* larsan übernimmt die Versammlungsleitung und führt ab hier durch die Versammlung.

Wahl der Protokollführung
-------------------------

* Die Kandidatenliste im Openslides wird eröffnet.
* Es melden sich die folgenden Kandidaten:
  * Chrissi^
  * drc (Philipp Specht)
* Es gibt keine weiteren Kandidaten.
* Die Abstimmung beginnt 14:27.

|

* Es werden 33 Stimmen abgegeben.
  * Chrissi^ 31 von 33 Stimmen
  * drc 33 von 33 Stimmen

Abstimmung über die Zulassung von Gästen
----------------------------------------

* Chrissi^ beantragt die Zulassung von Gästen zur Mitgliederversammlung.
* Es wird mit (Ja, Nein, Enthaltung) abgestimmt.
* Die Abstimmung beginnt 14:29.
 
|

* Es werden 31 Stimmen abgegeben.
  * Ja: 28 von 31 Stimmen
  * Nein: 0 von 31 Stimmen
  * Enthaltung: 3 von 31 Stimmen

|

* Damit werden Gäste zur Mitgliederversammlung zugelassen.
* Entsprechende Links werden über IRC/Matrix, sowie den Normalverteiler veröffentlicht.
* Die Zutrittskontrolle für die BigBlueButton-Konferenz wird entfernt.

Wahl zur Wahlleitung
--------------------

* Zu Beginn dieses Tagesordnungspunktes kommt es zu technischen Problemen mit dem Openslides.
  * Die Versammlung pausiert daher ab 14:30.
  * Shoragan (Jan Lübbe) untersucht das Problem.
    Es stellt sich heraus, dass OpenSlides in diesem Jahr wesentlich mehr RAM braucht.
    Die entsprechende VM wird mit mehr Ressourcen neu gestartet.
  * Die Versammlung wird um 14:40 fortgesetzt.
  * Die *Anwesenheit* der stimmberechtigten Mitgliedsentitäten wird zurückgesetzt, um sicherzustellen,
    dass wieder alle Personen anwesend sind.
  * Es kommt zu einzelnen Problemen, die nach und nach gelöst werden.
  * Nach dieser Unterberechnung sind 32 Mitgliedsentitäten *anwesend*.
  
 * Chrissi erläutert Aufgaben der Wahlleitung: Suche nach Kandidaten und Durchführung der Wahl incl. der Auszählung.
 * Es finden sich keine Kandidaten. Die Suche wird auf später verschoben.

Feststellung der Beschlussfähigkeit
-----------------------------------

* Chrissi^ stellt fest, dass der Verein zur Mitgliederversammlung 139 Mitglieder hat.
  * Davon sind 13 Fördermitglieder.
  * Entsprechend hat der Verein 126 ordentliche Mitglieder
  * Die Satzung sieht ein Quorum von 23 % der ordentlichen Mitglieder vor.
  * Dies entspricht 29 ordentlichen und stimmberechtigten Mitgliedern.
* Um 14:45 sind 34 stimmberechtigte ordentliche Mitglieder im Openslides *anwesend*.
* Chrissi^ stellt daher die Beschlussfähigkeit der Mitgliederversammlung fest.

Geschäftsordnung der Mitgliederversammlung
------------------------------------------

* Chrissi^ beantragt die Geschäftsordnung der Mitgliederversammlung aus dem
  [Wiki](https://stratum0.org/wiki/Mitgliederversammlung/Gesch%C3%A4ftsordnung) als
  Geschäftsordnung für diese Mitgliederversammlung anzuerkennen.
* Dem Antrag ist der folgende Volltext beigelegt:


        In den letzten Jahren hat sich die Mitgliederversammlung eine 
        Geschäftsordnung gegeben.
        Die Fassung aus dem letzten Jahr lautet:

        == §0 Wahlen ==
        1. Als Wahlmodus wird die Zustimmungswahl festgelegt.
           (Jede stimmberechtigte Entität kann für jede kandidierende Entität 
           eine Stimme abgeben, Stimmenkumulation ist nicht möglich.)
        2. Es wird in geheimer Wahl auf Stimmzetteln gewählt.
           Dabei dürfen nur von der Versammlungsleitung genehmigte Stimmzettel
           genutzt werden.
        3. Die Kandidaten mit den meisten Stimmen sind gewählt, sofern sie
           mindestens die Stimmen von 50% der Stimmberechtigten erhalten haben,
           solange genug Posten für das entsprechende Amt zu besetzen sind.
        4. Falls mehrere Posten für ein Amt zu vergeben sind (z.B. Beisitzer,
           Rechnungsprüfer), findet die Besetzung absteigend nach Stimmenanzahl
           statt, bis alle Posten des entsprechenden Amtes besetzt sind.
        5. Falls sich durch Stimmengleichheit keine eindeutige Besetzung ergibt,
           findet eine Stichwahl zwischen den entsprechenden Kandidaten mit der
           gleichen Stimmenanzahl statt.
           Eine Nachwahl findet jeweils für die Posten Schatzmeister, stellv.
           Vorsitzender, oder Vorsitzender statt, falls das entsprechende Amt
           nicht besetzt wurde.
        6. Die Auswertung der Wahl erfolgt in der Reihenfolge Vorstands-
           vorsitzender, stellv. Vorsitzender, Schatzmeister, Beisitzer, 
           Rechnungsprüfer.
           Kandidaten, die schon für ein Amt gewählt worden sind und es 
           angenommen haben, werden bei der Auswertung der nachfolgenden 
           Ämter nicht mehr berücksichtigt.
        7. Gewählte Kandidaten können von der Wahl zurücktreten.
           Die Annahme der Wahl oder der Rücktritt von der Wahl ist auch
           fernmündlich möglich.


* larsan liest die Geschäftsordnung vor.
* Es wird mit (Ja, Nein, Enthaltung) abgestimmt.
* Abstimmung startet 14:49.

|

* Es werden 35 Stimmen abgegeben:
  * Ja: 35 von 35 Stimmen
  * Nein: 0 von 35 Stimmen
  * Enthaltung: 0 von 35 Stimmen
* Die Geschäftsordnung wird angenommen.

Berichte
========

Finanzbericht
-------------

Der Finanzbericht wird von ktrask (Helena Schmidt) vorgestellt.
Der Finanzbericht kann im [Wiki](https://stratum0.org/wiki/Datei:Finanzbericht_Stratum_0_MV_Januar_2024.pdf)
gelesen werden.
Die folgenden Punkte ergänzen lediglich Aussagen, die mündlich, aber nicht auf den Folien getroffen wurden.

* Zu Folie 2:
  * Wir haben in diesem Jahr zum ersten Mal seit mehreren Jahren wieder Schlüssel ausgegeben.
* Zu Folie 3:
  * Getränkepreise: Wenn an den Getränken nichts weiter dran steht, ist der Preis nun 1,50 €.
  * Die Matekasse hat zwischenzeitlich Verluste gemacht.
    Daher war die Anpassung des Basispreises notwendig.
* Zu Folie 6:
  * Die Matekasse ist für das Abrechnen von Getränken und Snacks da.
    * Helena erklärt, wie der Ablauf ist für einen Einkauf ist.
    * Falls der Betrag in der Kasse nicht mit dem Kassenbuch übereinstimmt, soll man der
      Schatzmeisterin Bescheid geben und nicht selbst eine Korrektur vornehmen. 
      In der Regel wurde sich verrechnet oder falsch abgelesen.
* Zu Folie 7:
  * Zur Erstattungskasse:
    * Der Workflow steht auch auf der Erstattungskasse selbst und im [Wiki](https://stratum0.org/wiki/Erstattungskasse)
    * Es gibt auch ein [Pad](https://pad.stratum0.org/p/nachschub), in denen diskutiert wird, was man anschaffen könnte.
* Zu Folien 8..10:
  * Große Beträge > 50 € müssen zuvor beim Vorstand beantragt werden.
    * Es gibt eine [Wiki-Seite](https://stratum0.org/wiki/How_To_Antrag), die das Vorgehen beschreibt.
    * Anträge sollen nur in Euro und nicht in Fremdwährungen gestellt werden.
    * Falls eine Anschaffung auf Rechnung notwendig ist, weist beim Antrag darauf hin.
      Wir geben uns dann Mühe die Zahlungsfristen einzuhalten.
    * Jeder Antrag bekommt eine Umlaufbeschluss-Nummer "[UB yyyy-mm-dd-nn]"
      Diese Nummern stehen auch im Wiki auf der jeweiligen Vorstandssitzungsseite.
      z.B. https://stratum0.org/wiki/N%C3%A4chste_Vorstandssitzung
    * E-Mails an vorstand@ sind am besten. Dann sieht das der gesamte Vorstand.
    * Kontodaten für die Erstattung bitte in der Mail angeben, um Probleme zu vermeiden.
    * Es kann dazu kommen, das man auf die Erstattung etwas warten muss. 
      In der Regel wird ein Mal im Monat erstattet.
* Zu Folie 12: Ideeller Bereich: Projekte
  * Helena stellt fest, dass Ausgaben für Filament nicht auf die richtige Buchungskostenstelle
    gebucht wurde.
  * Freifunk kann selbständig über Ausgaben entscheiden. Größter Posten: Kosten für Server
  * Der Lasercutter braucht viel Strom, der aber nicht getrennt abgerechnet wird.
  * MakerVsVirus: Das Projekt hatte noch ein zweckgebundenes Guthaben.
    Das MakerVsVirus-Projekt hatte vorher festgelegt, an wen das gespendet werden sollte.
    Das haben wir entsprechend weiter gespendet.
* Zu Folie 13: Ideeller Bereich: Space
  * Wir haben für Geräte und Einrichtung ca. 6k€ ausgegeben.
  * Dazu haben wir 15k€ für Miete und Nebenkosten ausgegeben.
* Zu Folie 14: Vermögensverwaltung 
  * Sowas machen wir nicht
* Zu Folie 15: Zweckbetriebe
  * Sowas haben wir auch nicht
* Zu Folie 16: Wirtschaftlicher Geschäftsbetrieb
  * Die Matekasse macht aktuell circa 3.500 € Umsatz p.a.
  * Leichter Verlust (<40€) im letzten Jahr.
    Daher war die Preiserhöhung wichtig und richtig. 
    Wir wollen nicht aus Mitgliedsbeiträgen den privaten Konsum quer finanzieren.
* Zu Folie 17: Manko-Buchungen
  * Auf der Mitgliederversammlung wurde hier ein falscher Wert für die Manko-Buchung gezeigt.
  * Korrekt ist ein Fehlbetrag von -9 € (Ausgabe).
  * Auf der Folie im Wiki ist dies bereits korrigiert.
  * Im letzten Jahr kam es zu einem Fehler von 9 €.
    Es war nicht mehr nachvollziehbar, woher der Fehler kam.
* Zu Folie 18: Umbuchungen
  * Hier wurde Geld von der Matekasse auf das Girokonto, bzw. vom Girokonto in die Erstattungskasse
    übertragen.
* Zu Folie 19: Übersicht
  * Einnahmen 38k€
  * Ausgaben: 35k€
  * -> Überschuss 3k€
* Zu Folien 20..21: Vorhandenes Kapital
  * Zu Beginn des Jahres gibt es immer viele Mitgliedsbeiträge.
  * Auf alle Jahre betrachtet ist der Trend aufsteigend.
  * Helena zieht das Fazit: Gebt Geld für coole Projekte oder Space 3.0 aus
* Zu Folie 22: Monatliche Einnahmen
  * Die Einnahmen kommen durch Mitgliedsbeiträge und kleinere Spenden.
* Zu Folie 23: Monatliche Verpflichtungen
  * Aktuell circa 1,2k€
  * Diese sind für Miete, Strom, Versicherungen und den Vereins-Server.
* Zu Folien 24..25: Mitgliederentwicklung
  * Auf der Mitgliederversammlung wurde nur die Mitgliederentwicklung bis 2023 gezeigt.
  * Dies ist auf den Folien im Wiki korrigiert.
  * Die Plots zeigen nur die Anzahl der ordentlichen Mitglieder.
* Zu Folie 26: Bestände
  * Knapp über 30k€ frei verfügbares Kapital.
* Zu Folie 27: Rückstellungen
  * Wir haben immer noch 3000 € Rückstellung bei Einnahmeausfällen die Miete weiter zahlen zu können.
* Zu Folie 28: Ausblick
  * Wir nehmen deutlich mehr Geld ein, als wir ausgeben.
    Wir brauchen gute Ideen, wie wir das ausgeben können.

|

* Es wird dazu aufgerufen Fragen zu stellen.
  Es gibt aber von den Anwesenden keine Fragen.

Tätigkeitsbericht des Vorstands
-------------------------------

Der Tätigkeitsbericht des Vorstandes wird von lf (Jonas Martin, lichtfeind) vorgestellt.
Der Tätigkeitsbericht des Vorstandes kann im [Wiki](https://stratum0.org/wiki/Datei:T%C3%A4tigkeitsbericht_Stratum0_2023.pdf)
gelesen werden.
Die folgenden Punkte ergänzen lediglich Aussagen, die nicht auf den Folien getroffen wurden.

* Der Bericht soll zeigen, welchen Tätigkeiten der Vorstand im Jahr 2023 nachgegangen ist.
* Zu Folie 2: Vorstellung der AG Geschäftsführung
  * Die Vertrauensentität ist Ansprechpartner bei Problemen im Vereinsleben, besonders wenn der 
    Vorstand nicht involviert werden kann oder soll.
  * Aktuell ist die Person schlecht zu erreichen.
    Die Mitgliederversammlung sollte überlegen, ob sie eine neue Vertrauensperson bestellen möchte.
* Zu Folie 4:
  * Die relevanten Informationen der Geschäftsordnung sind im Fließtext.
  * Die Schlüsselverwaltung (physisch und SSH) wird von larsan übernommen.
  * Die Schriftführung übernimmt chrissi^.
* Zu Folie 5: Arbeitsweise
  * Im Chat-Raum (Matrix/IRC) ist aktuell wenig los. Ist aber offen für jede*n.
  * Geschäftsordnung kann im [Wiki](https://stratum0.org/wiki/Vorstand/Gesch%C3%A4ftsordnung)
    nachgelesen werden.
* Zu Folie 6: Zahlen
  * In dieser Vorstandsperiode gab es mehr UBs.
    Insgesamt waren es aber viele kleine Beschlüsse.
* Zu Folie 8: Glasfaser \\o/
  * Jetzt eventuell möglicherweise vielleicht Glasfaser über 1&1,
    aktuell aber noch hin-und-her mit der Hausverwaltung zum Kabelweg.
* Zu Folie 9: Licht
  * Mehr LED-Licht in der Küche.
    Damit hat der Space fast nur noch LEDs
  * Es fehlen noch ein paar letzte Röhren in der Werkstatt, die noch nicht ersetzt wurden.
    Da fehlt noch jemand, der Lust hat sich zu kümmern.
* Zu Folie 10: Motten
  * Lebensmittelmotten bekämpft. 
    Haben Schlupfwespen eingesetzt. 
  * Ist ein wiederkehrendes Problem.
  * Gelagerte Lebensmittel bitte luftdicht verschließen
* Zu Folie 12: Solar
  * Haben zwei Module für ein "Balkonkraftwerk" angeschafft.
  * Das soll "oben" an den Turm angebracht werden. Soll passieren, wenn es nicht mehr so kalt ist.
* Zu Folie 14: Sammlung von Links am Ende der Präsentation
  * data.stratum0.org zeigt auch welche UBs noch freie Finanzmittel haben.

|

* Es wird dazu aufgerufen Fragen zu stellen.
  Es gibt aber von den Anwesenden keine Fragen.

Bericht der Rechnungsprüfer
---------------------------

Emantor (Rouven Czerwinski) stellt den Bericht der Rechnungsprüfer vor.

* Es gab in diesem Jahr weniger Zwischenprüfungen. Die Prüfungen haben aber trotzdem gut funktioniert.
* Die Kasse wird gut geführt.
* Bei der Prüfung wurde ein Fehler gefunden.
  * Es wurde ein Vertipper zwischen "0" und "9" in der letzten Stelle gefunden.
  * Dies ist aber nicht schwerwiegend.
* Emantor bittet die Mitglieder die Mitgliedsbeiträge halbjährlich oder sogar jährlich zu überweisen.
  * Das senke die Kontoführungsgebühren.
  * Wir zahlen aktuell auch für eingehende Überweisungen.
  * Dann verdient die Bank weniger an uns.
* Beim Einkauf sollten Kassenzettel, wenn möglich, mit Zwischensumme oder komplett getrennt ausgeführt werden.
  * Es gibt immer wieder Kassenzettel mit handschriftlichen Notizen, um private Dinge von Space-Einkäufen 
    zu trennen.
  * Das macht das Prüfen dieser Kassenzettel aufwändig.
  * Eine Zwischensumme hilft private Einkäufe von Sachen für den Verein zu trennen und macht 
    weniger Arbeit bei der Kassenprüfung.
* Emantor empfiehlt den Vorstand zu entlasten.

Jahresbericht
-------------

Der Jahresbericht wird von larsan vorgestellt.
Der Jahresbericht kann im [Wiki](https://stratum0.org/wiki/Datei:Jahresbericht2023.pdf)
gelesen werden.
Die folgenden Punkte ergänzen lediglich Aussagen, die nicht auf den Folien getroffen wurden.

|

* Zu Folie 2 und 3:
  * Wieder mehr Uptime (Prozentuale Zeit in der der Space "offen" ist)
  * Die Uptime ist fast wieder auf Vor-Corona-Niveau: 85%
  * Letztes Jahr war man auch noch mal alleine im Space.
    Jetzt ist es voller.
* Zu Folie 4: Vorträge:
  * Es gab ein paar. Die Menge stagniert aber.
  * Redet gern über eure coolen Projekte!
* Zu Folie 5ff: HOA (Hacken Open Air)
  * Dieses Jahr in den Schulferien (in NDS).
    Damit sind jetzt andere Personen in der Lage dabei zu sein.
* Zu Folie 13ff: Resinlab
  * Der Raum war das Flurklo unten, wenn man die 1. Treppe hochgeht. 
  * Der Raum ist nur 2 m² groß. 
  * Wir haben den Raum renoviert - und Sticker versteckt.
  * Fenster / Belüftung muss noch gemacht werden.
  * Das WC ist für Feiern weiterhin nutzbar.
* Zu Folie 18: Hausverwaltung
  * Ja, wir haben eine neue (die 4.!) Hausverwaltung.
  * Die neue hat ihren Sitz jetzt auf dem Gelände und antwortet sogar auf E-Mails.
* Zu Folie 20: Freifunk Braunschweig Jahresbericht
  * Emantor übernimmt das Wort und stellt den Freifunk Braunschweig Jahresbericht vor.
  * Zu Folie 21: 
    * Wir haben coole Sticker!
    * Freifunk Braunschweig trifft sich jede Woche mittwochs ab 19:00 Uhr im Space.
  * Zu Folie 22:
    * Wir haben das Fest der Kulturen am 01.05.2023 mit Internet versorgt und wollen das auch 2024 wieder tun.
  * Zu Folie 23 und 24:
    * Wir haben wieder mehr Kontakt zur Stadt Braunschweig.
    * Wir sind im Gespräch für Freifunk für mehrere Projekte.
    * Marco steht viel im Austausch mit der Stadt.
    * Es gab ein Treffen Anfang November.
      Jetzt gab es dazu das Protokoll der Stadt.
      Die Mühlen mahlen langsam weiter.
    * Dank an Marco.
  * Zu Folie 25 und 26:
    * Wir stehen in Kontakt zur Stadt Wolfenbüttel.
    * Ein Mitarbeiter ist von BS nach WF gewechselt. Der macht dort in der Stadt aktiv Freifunk.
    * In der Innenstadt gab es schon einmal ein WLAN. Eventuell können wir das übernehmen.
    * Da wird in diesem Jahr was passieren. 
      Fortschritt wird eher von uns, als von der Stadt gebremst.
      Das ist ungewohnt.
  * Zu Folie 27:
    * Waren auf dem HOA und haben Internet gemacht.
    * Starlink ist zuverlässiger als LTE mit Richtantenne.
  * Zu Folie 28: Parker-Wochenenden
    * Wir betreiben unser Netz etwas anders als andere Communities.
    * Hilfe gerne gesehen, Ankündigung der Treffen im Stratum0-Wiki.
    * Ist eigentlich nur ein Tag; kein ganzes Wochenende :-)
    * Könnten wir noch besser ankündigen.
* Zu Folie 32..37: Digital Courage
  * (Ab hier übernimmt larsan wieder.)
  * Trifft sich jeden 2. Donnerstag im Space und Online.
  * Machen regelmäßig Vorträge.
  * Haben einen Citizen Science Workshop an der HBK veranstaltet.
  * Weitere Leute immer willkommen!
* Zu Folie 38: CTF-Team "CyberTaskForce Zero"
  * peace-maker erzählt, larsan zeigt Folien
  * Das Team ist weiterhin aktiv.
  * Von 3..4..5 Leuten auf heute circa 8 Leute gewachsen.
  * Haben an über 25 CTFs in 2023 teilgenommen.
  * Letztes Jahr ziemlich erfolgreich: DE: Platz 6, weltweit Platz 65
  * Treffen sich jetzt auch alle 14 Tage Dienstags ab 18:00 und nicht nur zu den CTFs.
  * Vernetzen sich innerhalb von DE:
    Kooperation mit RedRocket, PeaceMaker ist dort auch im Vorstand.
    Sind auch Teil der Sauercloud.
  * Haben sich (mit Sauercloud) qualifiziert nach Las Vegas zu fliegen und einen echten Satelliten im All zu hacken.
  * Termine stehen im Kalender.
  * Kommt vorbei - auch gerne mit wenig oder keinem Vorwissen.
* Zu Folie 39..42: Hey Alter!
  * Folie zeigt ein altes Foto ohne die neue Flagge. (Das ist schade!)
  * Im letzten Jahr gab es eine Nikolausaktion.
    * Es wurden Desktoprechner an Schülerinnen und Schüler verteilt.
    * Es gab sogar eine Schlange vor dem Gebäude
    * Es wurden 130 Rechner verteilt, 50 sind auf der Warteliste.
* Zu Folie 43..44: Malkränzchen
  * Pecca berichtet.
  * Gibt es seit sieben Jahren.
  * Regelmäßiges Treffen für kreative Tätigkeiten
  * Pecca bittet größere Treffen (z.B. Schrei-Wettbewerbe) und Küchenorgien an anderen Tagen zu planen.
  * Es ist auch schade, wenn Menschen zu den Treffen nicht kommen, weil es ihnen zu laut ist.
  * Auf die nächsten sieben Jahre!
* Zu Folie 45: Bierbrauen
  * Es wurde gebraut und es wird wieder gebraut werden.
* Zu Folie 47: Spacekatze
  * Wir hatten für einen Tag eine Spacekatze.
  * Diese kam über Umwege in den Space und wurde dann von einem Mitglied mit nach Hause genommen, bevor sie an
    den Eigentümer übergeben wurde.
* Zu Folie 48: 37C3
  * Es waren Menschen von uns auf dem 37C3.
* Zu Folie 49..50: Regenbogenimperium
  * Über das Jahr gab es viele neue Regenbogendinge.
* Zu Folie 51..52: Energie
  * LED-Panels für den Space wurden getestet und in der Küche und im Resinlab verbaut.
* Zu Folie 53..55: Kühlschränke
  * Es gab Veränderungen bei den Kühlschränken.
  * Der alte Gefrierschrank wurde gegen einen neuen, effizienteren und größeren getauscht.
  * Außerdem gibt es nun einen Mini-Getränkekühlschrank.
* Zu Folie 56: Solarstrom
  * Solarstrom in Aussicht, es fehlt besseres Wetter.
* Zu Folie 57: Silvester
  * Es wurde Silvester im Space gefeiert.
* Zu Folie 58: Danke (Teil 1)
  * Danke an alle, die: 
    * Müll rausbringen, 
    * sich um den Space kümmern, 
    * die putzen, 
    * Getränke kaufen, 
    * Dinge nachkaufen, ...
* Zu Folie 59: Danke (Teil 2)
  * Danke an alle, die Vereinsinfrastruktur betreiben.
  * Besonders Shoragan, aber auch die anderen 14 Admins.
* Zu Folie 60: Wünsche für 2024:
  * Unsere 10-Jahres-Feier fehlt noch. Eventuell können wir die in 2024 nachholen?
  * Das Coderdojo ist wieder in Planung.
  * Wir sollten wieder mehr Blogposts schreiben. Die letzten waren 2022!
  * Macht mehr coole Projekte!

Bericht der Vertrauensperson
----------------------------

Die Vertrauensperson ist nicht anwesend.
Der Bericht entfällt.

Entlastung des Vorstands
========================

dadada (Tim Schubert) beantragt die Entlastung des Vorstandes im Block.

* Es ist technisch nicht prüfbar, ob die Vorstände an dieser Abstimmung teilnehmen.
* Da aktuell 36 stimmberechtigte Mitglieder anwesend sind, dürfen maximal 30 stimmberechtigte
  Mitglieder an der Abstimmung teilnehmen.
* Es wird mit (Ja, Nein, Enthaltung) abgestimmt.
* Abstimmung startet 16:03.

|

* Die Stimmabgabe endet um 16:06.
* Es werden 29 Stimmen abgegeben:
  * Ja: 29 von 29 Stimmen
  * Nein: 0 von 29 Stimmen
  * Enthaltung: 0 von 29 Stimmen
* Der Vorstand wird somit im Block entlastet.

Änderungsanträge
================

* Zur Einladung zur Mitgliederversammlung lagen keine Satzungsänderungsanträge vor.
* Es liegen keine weiteren Änderungsanträge vor.

Feedback, Input für den nächsten Vorstand
=========================================

* chrissi^: Wir könnten unsere aktuellen guten finanziellen Möglichkeiten nutzen, um Vorträge mit externen
  Speakern zu interessanten Themen zu organisieren.
  Diesen Speakern könnten wir dann z.B. Fahrt und Hotel bezahlen.
  Es müssten sich allerdings Personen finden, die die Organisation übernehmen würden, da Chrissi^ dafür
  aktuell nicht ausreichend Zeit hat.
  * Tim: Eventuell könnten wir nicht nur Vorträge, sondern auch Workshops finanzieren?
    Man braucht aber die passenden Ideen.
  * larsan: Der Stratum 0 betreibt ein Pretix, dass hier bei Bedarf zur Verfügung steht.
  * Darüber hinaus werden die folgenden Ideen für Workshops genannt:
    * NelienE: Erste-Hilfe-Kurs
    * Fera per Chat: "Mental Health First Aid"
      Fera bietet sich an, den vorzubereiten.
    * lf: Exkursionen
    * chrissi^: Feuerlöschen für Amateure
      * Daniel: Haben intern vermutlich genug Feuerwehr-Menschen; brauchen wir nicht extern anzufragen.

|

 * Aus der Runde wird Dank an den Vorstand für seine bisherige Arbeit ausgesprochen.

|

* Daniel: Macht es eventuell Sinn das HOA von der UG zum Verein zu übertragen?
  * Tim: Wurde auf der letzten Orga-Runde bereits angesprochen.
    Wir können uns das prinzipiell vorstellen - aber nicht mehr vor diesem HOA.
  * Chrissi^ findet die Idee als Vorstand spannend.
    Wir sollten eine solche Änderung aber gut durchdenken, bevor wir das durchführen.

|

* Tim: Hätte Interesse die Vegan Academy (vegane Kochrunde) wiederbeleben.
  Wer mitmachen möchte, soll Tim anschreiben.
  Die alte Vegan Academy ist vor vielen Jahren eingeschlafen.

|

* Shoragan: Wir haben noch Kapazitäten auf dem Vereinsserver.
  Falls jemand Dienste für eine größere Gruppe betreiben möchte: Es gibt noch Möglichkeiten.
  Eventuell gibt es neben den aktuellen Diensten noch Nischen, die man abdecken könnte.
  Shoragan hilft auch gern bei der Inbetriebnahme.

Nach diesem Block wird eine Pause von 16:18 bis 16:28 eingeschoben

Wahlen
======

## Wahl der Wahlleitung: Erster Wahlgang

* Es wird weiterhin nach einer Wahlleitung gesucht.
* Chrissi weist darauf ihn, dass eigentlich alles Tooling für die Briefwahl vorhanden ist.

|

* 53c70r (Silvan Nagl) wird vorgeschlagen
* Es wird mit (Ja, Enthaltung) abgestimmt.
* Ergebnis:
  * Ja: 29 von 32 Stimmen
  * Enthaltung: 3 von 32 Stimmen
* 53c70r nimmt die Wahl nicht an und wird somit nicht Wahlleitung.

## Wahl der Wahlleitung: Zweiter Wahlgang

* Die Suche nach Kandidaten wird erneut gestartet.
* Marie Wellhausen bietet sich an.
* Es wird mit (Ja, Enthaltung) abgestimmt.
* Ergebnis:
  * Ja: 29 von 30 Stimmen
  * Enthaltung: 1 von 30 Stimmen
* Marie nimmt die Wahl an.
* Marie übernimmt das Wort.
* Marie beruft die folgenden Wahlhelfer*innen:
  * Emantor
  * Frauke Gellersen
  * dadada
  * Nux (Luca Gabriel)
* mjh fragt, ob Marie nicht fragen wolle, ob jemand mit den Wahlhelfern unzufrieden sei.
  * Marie gibt die Frage in die Runde weiter.
  * Es gibt keine Gegenstimmen gegen die Wahlhlfer*innen.

## Vorstellung der Ämter

* Im Folgenden werden Kandidaten für die Ämter gesucht.
* Da sich die Suche als schwierig herausstellt, werden —- während der Suche -- die Ämter wie folgt vorgestellt:

* lf stellt Beisitzerposten vor.
  * Aufgaben der Beisitzer sind:
    * Entlastung der anderen Vorstandmitglieder
    * Mails lesen, UBs bearbeiten
  * Arbeitsvolumen gut anpassbar
  * Gute Gelegenheit sich einzuarbeiten und das "Vorstand sein" auszuprobieren.
  * Zeitaufwand circa:
    * 4 Sitzungen im Jahr
    * 1x / Monat Arbeitstreffen
  * Remote teilnehmen ist möglich

* Schatzmeister*in:
  * Marie: Dieses Jahr die Gelegenheit das kennenzulernen.
  * Emantor: Aus Kassenprüfungssicht: Es ist heute weniger Arbeit, als noch vor einigen Jahren.
    ktrask hat über ihre Amtszeiten viel automatisiert.

* chrissi^ stellt den Posten des 2V vor
  * Nicht mehr Aufwand als der des Beisitzers
  * Für die Ämter 1V, 2V und Schatzmeister*in ist ein einmaliger Besuch bei der Bank und beim Notar 
    notwendig.

* larsan stellt kurz Posten des 1V vor
  * Aufgaben wie Beisitzer*innen
  * Aktuell zusätzlich repräsentative Aufgaben. Die könnten aber auch anders verteilt werden.

## Kandidaten

Es werden die folgenden Kandidaten aufgestellt:

* 1V
  * larsan (Lars Andresen)
  * lf (Jonas Martin)
* 2V
  * chrissi^ (Chris Fiege)
  * lf (Jonas Martin)
* Schatzmeister*in
  * ktrask (Helena Schmidt)
* Beisitzer*in
  * lf (Jonas Martin)
  * NelienE (Nele Warnecke)
  * 53c70r (Silvan Nagl)
  * Oleh Paliy
  * EmJee13 (Marie Goetz)
  * chrissi^ (Chris Fiege)
  * larsan (Lars Andresen)

Die Kandidaten stellen sich im Folgenden kurz vor:

* lf
  * lf möchte sich nicht vorstellen und lässt sich daher von anderen beschreiben.
* Nele:
  * Nele hat Audio-Probleme und wird auch von den Anwesenden beschrieben.
  * Im Chat trifft sie darüber hinaus folgende Aussage:
    Ich hatte das Gefühl gerade ein paar Aufgaben für mich gefunden zu haben 
    und würde das noch mal weiter machen.
* 53c70r:
  * mjh beschreibt 53c70r wie folgt:
    Er ist schon länger dabei und hat auch andere ehrenamtliche Arbeiten. 
  * mjh hält ihn für geeignet andere Ansichten zu vertreten.
* Oleh:
  * Kommt gern in den Space. Ist dankbar für die Infrastruktur und möchte dafür etwas zurückgeben.
* Marie:
  * Ist seit circa 1.5 Jahren im Space und macht im Moment viel HOA-Orga.
* chrissi^:
  * Ist seit vielen Jahren im Vorstand.
  * Kümmert sich aktuell um das Schreiben von Protokollen
    und putzt immer mal wieder hinter den E-Mails her.
  * Ist aber auch gern bereit den Posten nach so vielen Jahren abzugeben.
* larsan:
  * Macht das jetzt "seit ein paar Jahren".
  * Hat mal als Beisitzer begonnen und ist irgendwann mal 1V geworden und wurde dann immer wieder gewählt.
  * Ist auch gern bereit zu beraten und steht für Fragen zur Verfügung.
* ktrask:
  * Würde das Amt des Schatzmeisters noch einmal machen.
  * Muss dazu sagen, dass sie jetzt nach Hannover gezogen ist - sie wird aber trotzdem immer wieder in BS sein.
  * Sie bräuchte aber eine weitere Person im Vorstand, die vor Ort unterstützt.
    Am besten eine beisitzende Entität.

## Berufung der Kassenprüfer*innen

* Marie fragt in die Runde, ob die Kassenprüfer*innen neu berufen werden sollen.
* Angela und Emantor geben an, dass sie wieder zur Verfügung stehen.
* Marie fragt, ob jemand hierüber neu abstimmen möchte.
* Es meldet sich niemand.
  Die Kassenprüfer werden daher nicht neu gewählt.
* Angela und Emantor sind somit weiterhin zu Kassenprüfer*innen berufen.

## Berufung der Vertrauensperson

* Marie: Die Vertrauensperson kann bei Bedarf auch neu gewählt werden, wenn dies gewünscht ist.
  * Dies ist aber kein Vorstandsposten. Die Vertrauensperson wird von der Mitgliederversammlung ernannt.
* Mehrere Personen sind dafür die Vertrauenspersonen neu zu wählen,
  da die alten sich nicht mehr gemeldet haben.
  * Es gibt darüber hinaus keine Stimmen dafür die bisherigen Vertrauenspersonen zu behalten.
* Es melden sich die folgenden Kandidat*innen:
  * Fera(lina) (Antje Mönch)
  * Frauke Gellersen
* Aufgrund der Abwesenheit der aktuell bestellten Personen stellt chrissi^ den Posten vor:
  * Die Vertrauenspersonen sind Ansprechpartner für Mitglieder des Vereins, wen diese Probleme mit
    Vorstandsmitgliedern oder andere Probleme mit vereinsbezug haben.
  * Aufgabe ist es klärende Gespräche mit den Parteien zu suchen.
* Es wird überlegt, ob die Mitgliederversammlung ein oder zwei Vertrauenspersonen berufen möchte.
  * Marie hört heraus, dass zwei Vertrauenspersonen die präferierte Option ist.
* Frauke: Ist es ein Problem, dass sie Wahlhelferin ist und gleichzeitig auszählt?
  * Die anwesenden Personen sehen hierin kein Problem.
* Es wird überlegt, ob die Abstimmung direkt im Openslides durchgeführt werden soll.
* Vorstellung der Kandidatinnen:
  * Fera hat kein Mikrofon und wird von der Runde vorgestellt
    * Sie ist beim Malkränzchen aktiv.
    * Darüber hinaus erhält sie viel positiven Zuspruch aus der Runde.
  * Frauke stellt sich vor:
    * Sie geht davon aus, dass sie im Space bekannt ist.
    * Sie hat mit Antje zusammen gehäkelt.
    * Sie gibt an, dass sie Probleme vertraulich behandeln werden wird.
    * Sie erhält ebenfalls viel positiven Zuspruch aus der Runde.

|

* Die Abstimmung wird als Personenwahl im Openslides durchgeführt.
* Der Wahlmodus ist Zustimmungswahl. Jedes stimmberechtigte Mitglied erhält somit bis zu 2 Stimmen.
* Die Abstimmung beginnt um 17:31.
  * Bei diesem Wahlgang kommt es zu technischen Problemen:
    Mitglieder hatten nicht die richtige Anzahl Stimmen.
    Der Wahlgang wird für ungültig erklärt und abgebrochen.
* Wiederholung der Wahl um 17:35.

|

* Um 17:37 liegt das Ergebnis vor:
    * Antje: 31 von 34 Stimmen
    * Frauke: 31 von 34 Stimmen
    * Es gibt drei generelle Enthaltungen.
* Marie fragt Antje und Frauke, ob diese die Wahl annehmen.
  * Frauke nimmt die Wahl an.
  * Antje nimmt die Wahl an.

## Ende der Wahl

* Marie übergibt das Wort um 17:39 zurück an larsan.
* larsan weist darauf hin, dass stimmberechtigte Mitglieder bald einen Brief für die Briefwahl erhalten werden.
* chrissi^ weist auf die folgenden Punkte hin: 
  * Die Adressabgabe ist aktuell noch bis zum Ende der Mitgliederversammlung möglich.
  * Alle ordentlichen Mitglieder sollten eine E-Mail mit einem Link zur Adressabgabe bekommen haben.
    Falls jemand die nicht bekommen hat, solle sich die Person bei chrissi^ melden.
  * Die Adressierung an Boxen im Space ist auch möglich.

Sonstiges
=========

## Terminfindung für die nächste Mitgliederversammlung

* Es gibt etwas Diskussion über mögliche Termine.
  Es stehen der 19.01.2025, sowie der 26.01.2025 zur Auswahl.
* Der 26.1.2025 wird als Termin für die nächste Mitgliederversammlung angedacht.

## Diskussion: Die nächste Mitgliederversammlung Online, Offline oder hybrid?

* drc fragt, ob wir die nächste Mitgliederversammlung wieder online machen wollen.
* chrissi: Findet online gut
* Daniel: Regt an das wieder offline zu machen.
* EmJee13: Wäre für hybrid
* shoragan: Nimmt an, dass wir vor Ort effizienter sind.
* Tim/dadada: Müssen wir uns dann eventuell um einen größeren Raum kümmern?
* chrissi: Die BLSK bietet Räumlichkeiten für solche Versammlungen für Vereine an.
* mjh: Mag Status Quo, weil Online-Teilnahme inklusiver ist.
  Sonst muss man sich überlegen, ob/wie man die Mitgliederversammlung streamen kann.
  Wir sollten überlegen, wie man das Tooling verbessern kann.
  Würde es nicht groß anders machen, als in diesem Jahr.
* drc meint, dass dies das erste Jahr war, in dem Openslides Probleme gemacht hat.
* mjh fragt: Hat jemand das Gefühl, sich nicht ausreichend einbringen zu können?
  Und daher die Versammlung eventuell anfechten zu wollen?
  * Niemand meldet sich auf diese Frage.
* lf: Remote ist "entspannter".
  Mitgliederversammlungen vor Ort sind zum Teil umständlich und chaotisch.
  Für eine Offline-Mitgliederversammlung müssen wir uns größere Räumlichkeiten, 
  vorzugsweise mit Schnittchen, suchen.
  Eventuell einen Termin mit besserem Wetter suchen und draußen durchführen?
* Emantor: Jemand meinte, dass man Remote sein eigenes WC habe.

|

* Es wird eine Umfrage im BBB gestartet, um ein vorläufiges Meinungsbild zu bekommen.
* Die Umfrage wird mit folgendem Ergebnis abgeschlossen:
  * Online: 6
  * Hybrid: 18
  * Offline: 0
  * Der Vorstand soll entscheiden: 6



## Weltherrschaft

* Emantor beantragt den Punkt zu vertagen.
* Es gibt keine anderen Wortmeldungen.

## Weiteres

* lf weist darauf hin, dass Mitglieder das Hausrecht ausüben dürfen, falls das notwendig sein sollte.
* Es wird über Spacekatzen diskutiert.

## Abschluss der Versammlung

* larsan schließt die Versammlung um 18:01.
* Er bedankt sich bei allen, die bei der Mitgliederversammlung dabei waren.


Auszählung der Wahlen
=====================

Dieser Teil des Protokolls wird nur von drc geschrieben.
Die Auszählung findet am 25.02.2024 ab 16:20 Uhr im Space statt.
Anwesend sind:

* Marie Wellhausen – Wahlvorstand
* dadada – Wahlhelfer
* Frauke – Wahlhelferin
* Emantor – Wahlhelfer und Technik

* Doom -  Beobachter
* lf – Beobachter ab ca. 16:35 bis 16:55
* drc – Protokoll

Die Auszählung wird von Doom als Livestream zur Verfügung gestellt.
Im Stream sind wechselnd unterschiedliche Personen anwesend.

## Vorbemerkung

Die Wahl hat dieses Jahr als Briefwahl stattgefunden.
Hierzu wurden im Vorfeld der Mitgliederversammlung ordentliche Mitglieder
aufgefordert eine aktuelle Post-Adresse in einem Online-Tool anzugeben.
Bei der Wahl stimmberechtigt waren dann all die ordentlichen Mitglieder,
die zum Zeitpunkt der Mitgliederversammlung ein ausgeglichenes Mitgliedskonto hatten.

Die Briefe wurden per Post an die angegebene Adresse gesendet.
Wurde keine Adresse angegeben, so wurden die Briefe im Space hinterlegt.
Die Briefe hatten folgenden Inhalt:

* Anschreiben mit Anleitung
* Wahlschein: Muss unterschrieben werden.
  Auf dem Wahlschein befindet sich ein für jede Person individuelles Geheimnis,
  welches bei der Auszählung überprüft wird.
* Stimmzettel:
  Der Stimmzettel darf nicht besonders gekennzeichnet werden, da dieser sonst
  ungültig ist.
* Stimmzettelumschlag:
  da gehört der Stimmzettel rein.
* Frankierter Rückumschlag:
  Dort Stimmzettelumschlag und Wahlschein einlegen
  und zurücksenden.

## Auszählung

Die Auszählung beginnt damit die vorliegenden Briefe zu zählen:

* 97 Personen waren prinzipiell stimmberechtigt
* 29 Stimmzettel wurden aus dem Space nicht abgeholt
* 52 Briefumschläge sind eingegangen
* 16 Briefe sind nicht zurückgekommen

|

* Anschließend werden die Briefe geöffnet.
  Marie und Frauke gleichen die Geheimnisse auf dem Anschreiben mit den
  versendeten ab.
* Die Briefe werden dabei anhand des Merkmals "Unser Zeichen" zugeordnet.
* Wird das Geheimnis erfolgreich abgeglichen, so werden Anschreiben und
  Stimmzettelumschlag in unterschiedliche Boxen getrennt.
* Auffälligkeiten:
  * Ein Brief ist in einem Nicht-Standard-Umschlag zurückgekommen.
    Der Inhalt ist aber gültig und die Stimme wird daher trotzdem gezählt.
  * Ein weiterer Brief enthält zwei Umschläge.
    Der Inhalt ist aber gültig und die Stimme wird daher trotzdem gezählt.
  * Es werden zwei ungültige Briefe identifiziert.
    Diese werden in eine separate Box sortiert.
    Die Gründe sind:
    * Ohne Unterschriftzettel: 1
    * Kein Inhalt: 1

* Somit sollten 50 Unterschriftzettel und 50 Stimmzettelumschläge 
  vorhanden sein.
  Eine Kontrollauszählung durch Emantor und dadada bestätigt das.

Die Auszählung erfolgt in folgendem Modus:

* Marie und dadada öffnen die Stimmzettelumschläge und lesen die Stimmen vor.
* Frauke und Emantor zählen die Stimmen auf einem Flipchart.
* Die Auszählung wird um 17:21 beendet.

## Ergebnis der Wahl

Das Ergebnis der Wahl wird in der angegebenen Reihenfolge bekannt gegeben.
Die Personen werden jeweils angerufen und gefragt, ob sie die Wahl annehmen.

* 1. Vorsitzender
  * larsan: 43 Stimmen
    * larsan wird von Frauke angerufen und nimmt die Wahl an.
  * lf: 37 Stimmen
* 2V
  * lf: 43 Stimmen
    * lf ist anwesend und nimmt die Wahl an.
  * chrissi^: 42 Stimmen
* Schatzmeister*in
  * ktrask: 50 Stimmen
    * ktrask wird von Frauke angerufen und nimmt die Wahl an.
  
An dieser Stelle kommt es zu technischen Problemen mit dem Stream.
Diese sind nach circa 5 Minuten behoben.

* Beisitzende (max. 3)
  * lf: 45 Stimmen 
    * Ist bereits 2V und kann somit nicht Beisitzer werden.
  * chrissi^: 44 Stimmen
    * chrissi^ wird von Emantor angerufen und nimmt die Wahl an.
  * Marie (emjee13): 43 Stimmen
    * Marie wird von Marie angerufen und nimmt die Wahl an.
  * larsan: 41 Stimmen
    * Ist bereits 1V und kann somit nicht Beisitzer werden.
  * Nele: 39 Stimmen
    * Nele wird von Marie angerufen und nimmt die Wahl an.
  * 53c70r: 36 Stimmen
    * Keine weiteren Posten frei.
  * oleh: 27 Stimmen
    * Keine weiteren Posten frei.

Die Stimmzettel werden anschließend beim Vorstand archiviert.
Auszählung beendet um 17:35.
Protokoll und Stream werden beendet.
