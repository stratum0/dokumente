---
vim: ts=4 et sw=4 tw=90
build-me-with: make pdf
documentclass: s0minutes
papersize: a4
lang: de
toc: true
toc-depth: 2
links-as-notes: true
title: Mitgliederversammlung 2021-05-08
s0minutes:
  typeofmeeting: \generalassembly
  date: 2021-05-08
  place: "online; OpenSlides- und BigBlueButton-Instanzen des Stratum 0 e.V."
  startingtime: "14:00"
  endtime: "18:28"
  attendants: 44 ordentliche Mitglieder, 0 Fördermitglieder, keine Gäste
  absentees: 
  minutetaker: rohieb, Chrissi\^
---

Die Veranstaltung findet in einem Raum auf dem BigBlueButton-Videokonferenzssystem des
Vereins statt, in dem sich schon viele Entitäten eingefunden haben.
Abstimmungen werden über OpenSlides vorgenommen, das auf einem Vereinsserver läuft.
Die Mitglieder haben mit der Einladung zur Mitgliederversammlung an ihre letzte dem
Vorstand bekannte Adresse einen zufälligen Akkreditierungs-Code mitgeteilt bekommen.
Die Akkreditierung findet über einen Video-Chat mit Chrissi^ (Vorstandsmitglied) und Kasa
(Nicht-Vorstandsmitglied) statt, in dem je ein einzelnes Mitglied zur Zeit nach Abfrage
seines Akkreditierungs-Codes im OpenSlides Stimmrechte eingeräumt bekommt.
Zugänge zum OpenSlides wurden vor der Mitgliederversammlung an alle aktiven Mitglieder
versendet.
Somit sind alle Mitglieder, unabhängig von ihrer Stimmberechtigung, in der Lage, an der
Mitgliederversammlung teilnehmen.


\setcounter{section}{-2}
Technik-Test
============

Nachdem der Andrang in der Akkreditierungs-Warteschlange abgeflaut ist,
ruft Chrissi^ alle Mitglieder auf, sich im OpenSlides als "anwesend" zu markieren,
damit sie an Abstimmungen teilnehmen können.
Larsan eröffnet eine Probeabstimmung im OpenSlides.
Von 44 akkreditierten Entitäten stimmen 42 Entitäten bei der Probeabstimmung ab.
Da es keine Beschwerden gibt, entscheiden wir uns, trotz der zwei fehlenden Stimmen
fortzufahren.
Die Versammlung beginnt um 14:06 Uhr.

Protokoll-Overhead
==================

### Beschlussfähigkeit
Chrissi^ und Kasa haben 44 von 102 ordentlichen Mitgliedern (43.1%) nach obigem Verfahren
akkreditiert.
Die Versammlung ist damit beschlussfähig, weil mehr als 23% der ordentlichen Mitglieder
anwesend sind.

### Versammlungsleitung
Larsan bietet sich als Versammlungsleitung an. 
Niemand anders schlägt eine andere Entität vor.
Es gibt im OpenSlides volle Zustimmung für larsan.
larsan leitet somit die Versammlung.

### Protokollführung
rohieb und Chrissi^ bieten sich als Team für die Protokollführung an.
Niemand anders möchte das Zepter an sich reißen.
Im OpenSlides wird abgestimmt: 39 Pro, 0 Contra, 3 Enthaltungen für diese Kombination.
rohieb und Chrissi^ führen Protokoll.

### Wahl der Wahlleitung für die folgende Briefwahl

mist hat mit Chrissi und restlichem Vorstand schon im Vorfeld ein Konzept für die
Briefwahl des Vorstandes und der Rechnungsprüfer vorbereitet,
und bietet sich als Wahlleitung an.
Alle restlichen Abstimmungen im OpenSlides sind keine Wahlen, und dementsprechend auch
nicht Aufgabe der Wahlleitung.

reneger wirft auf, ob man noch einmal abstimmen möchte, ob wir überhaupt Briefwahl machen
wollen, oder ob uns eine Wahl im OpenSlides reichen würde.
mist stellt fest, dass man das während des TOPs Wahlen machen könnte.

larsan wirft ein, dass nach der aktuellen Geschäftsordnung die Personenwahlen geheim
stattfinden müssen.
Eine weitere Frage ist, ob OpenSlides Personenwahlen unterstützt, und wie anonym das ist.
Offenbar hat sich diese Option niemand im Vorfeld genauer angeschaut.

Chrissi^: da wir als Nerds ja chronisch allen Computern misstrauen, haben wir die
Briefwahl-Option vorbereitet.

Es gibt keine weiteren Kandidaten für die Wahlleitung außer mist.
Die Abstimmung findet im OpenSlides statt: 41 von 43 Stimmen für mist als Wahlleitung.

### Zulassung von Gästen
Mitglieder sind in jedem Fall berechtigt, an der Versammlung teilzunehmen, auch wenn sie nicht akkreditiert sind.
Zusätzlich kann man OpenSlides und den BBB-Raum auch für weitere Gäste öffnen.
Es gibt die Anmerkung, dass Gäste in den einzelnen Haushalten möglicherweise mithören können wollen.

Im OpenSlides wird über die Zulassung von Gästen abgestimmt:
38 von 42 Stimmen sind für die Zulassung von Gästen, es gibt 4 Enthaltungen.
Der [Link zum OpenSlides](https://mv.stratum0.org/autopilot) wird an interessierte Gäste
weiterverbreitet.

### Abstimmung über die Geschäftsordnung
Es wird der Antrag gestellt, die bisherige [Geschäftsordnung] wie bisher weiter zu nutzen.
Es wird im OpenSlides abgestimmt: 40 von 43 Stimmen für die Annahme der bisherigen
Geschäftsordnung.
Die Geschäftsordnung ist damit unverändert angenommen.

[Geschäftsordnung]: https://stratum0.org/mediawiki/index.php?title=Mitgliederversammlung/Gesch%C3%A4ftsordnung&oldid=27550

Berichte
========

Finanzbericht
-------------

Helena berichtet als Schatzmeisterin vom vergangenen Geschäftsjahr.
Ihre Präsentation ist im 
[Internet](https://stratum0.org/wiki/Datei:Finanzbericht_Stratum_0_Januar_2020_bis_April_2021.pdf)
zum Nachlesen verfügbar.

Alle Angaben in diesem Kapitel beziehen sich, wenn nicht explizit anders angegeben,
auf den Zeitraum 01.01.2020 bis 30.04.2021.

### Übersicht und Neues

Helena hat mit dieser MV ihre erste Amtszeit als Schatzmeisterin abgeschlossen.
Problematisch war, dass es bis zwei Wochen vor die MV gedauert hat, bis sie 
endlich Zugriff auf das Girokonto hatte.

In diesem Jahr wurden die Konten 
Barkasse mit dem Unterkonto Pfandkasse, 
Erstattungskasse (offene Kasse),
Matekasse (offene Kasse) und 
Girokonto mit den Unterkonten Rückstellungen und Pfandgeld
geführt.

Während ihrer Amtszeit haben sich folgende Änderungen im Bereich der 
Schatzmeisterei ergeben:

* Die Matekasse kann nun per Überweisung bezahlt werden kann.
  Dies wurde aufgrund der besonderen Umstände während der Corona-Pandemie eingeführt.
  Auf Helenas Sicht kann dies aber so beibehalten werden.
* Darüber hinaus werden nun Spenden von Mitgliedern den Mitgliedskonten zugeordnet,
  sodass diese automatisiert in die Zuwendungsbescheinigungen aufgenommen werden können.
  Die Zuwendungsbescheinigungen selbst können ebenfalls automatisiert erzeugt werden.
* Die E-Mail Adresse schatzmeister@stratum0.org wurde eingerichtet.
  Diese Adresse ist als geteiltes Postfach umgesetzt.
  Somit können darauf auch andere Vorstände, sowie nachfolgende Schatz-Entitäten,
  zugreifen.
* In diesem Jahr wurden diverse Altlasten beseitigt und TODOs erledigt.
* Die [Online-Übersicht](https://data.stratum0.org/finanz) listet nun auch
  Umlaufbeschlüsse und wie viel Geld davon bereits abgrufen wurde.
* Helena berichtet, dass sie viel Python2-Code auf Python3 portiert hat.
  Darüber hinaus hat sie einige in Java geschriebene Tools ersetzt.
  
### Sponsoren/Spender

In diesem Jahr ist es, besonders wegen der Anschaffung des Laser-Cutters und 
Maker-vs-Virus zu einem hohen Spendenaufkommen gekommen.
Große Spenden sind von Pengutronix, PROSPER-X und quuxlogic eingegangen.

Es sind insgesamt über 6.000 € Spenden für den Laser-Cutter zusammen gekommen.
Dem gegenüber stehen Ausgaben von 11.789 € für die Anschaffung des Lasers, sowie
des Zubehörs.
Es wird darauf hingewiesen, dass es eine Spendenempfehlung von 0,50 € je verbrauchte
Minute Laser gibt, um die laufenden Kosten der Maschine zu decken.
Eine Spendenkasse ist in der Nähe des Gerätes aufgestellt.

Die Initiative Maker-vs-Virus hat, besonders zu Beginn der Pandemie, Gesichtsschilde
hergestellt und kostenlos verteilt.
Dies ist zum Teil mit dem Laser-Cutter, sowie unseren 3D-Druckern geschehen.
Der Verein hat für diese Initiative Spenden entgegengenommen.
Diese kamen von unterschiedlichen Institutionen, Ärzten und 
anderen Einrichtungen.
Insgesamt hat die Initiative so über 11.000 € bekommen.
Der größte Teil hiervon wurde bereits für die Initiative wieder ausgegeben.
Der übrig gebliebene Rest von 2.910 € wird an andere, gemeinnützige
Projekte weitergespendet.
Die Projekte wurden dabei von den Maker-vs-Virus-Mitarbeitenden Anfang des letzten
Jahres ausgesucht.

### Matekassen-Inventur

Im letzten Jahr wurde eine Inventur der Matekasse durchgeführt.
Hierbei wurden Bargeld, Warenbestand, sowie Guthaben auf den Papierlisten und in
der Online-Matekasse gezählt.
Der Warenbestand wurde finanziell bewertet.
Insgesamt ergab sich hierbei ein Saldo von 476,10 € (also ein Überschuss) in der 
Matekasse.
Zugleich wurden aber insgesamt auch 292 € Schulden in den Strichlisten gezählt.
Entitäten mit höheren Schuldbeträgen wurden benachrichtigt.

Helena weist darauf hin, dass die Guthaben in der Matekasse auch vertrunken werden
können. Entsprechend müsse die Summe der Guthaben in der Matekasse auch weiterhin
(in Warenbestand oder Bargeld) vorhanden sein.

Es ist nun auch möglich, Guthaben per Überweisung in die Matekasse einzuzahlen.
Hierzu muss bei der Überweisung der Verwendungszweck "*Matekasse*", sowie
der Name angegeben werden.

Aktuell wird eine neue Software für die Online-Matekasse entwickelt.
Diese ist fast fertig und soll bald eingesetzt werden.
In diesem Zug soll ebenfalls das defekte Tablet ersetzt werden.
Helena stellt fest, dass die Papierstrichlisten sehr chaotisch sind und dies die
Inventur schwierig macht.
Aus diesem Grund sollten die Papierlisten möglichst bald abgeschafft werden.

Im Februar 2021 haben Getränke im Wert von circa 50 € das Mindesthaltbarkeitsdatum
überschritten.
Darüber hinaus laufen in Kürze weitere Getränke ab;
diese sind aber nicht Teil dieses Finanzberichtes.

### Gewinn- und Verlustrechnung

Die Gewinn- und Verlustechnung wird, wie in jedem Jahr, nach Geschäftsbereichen
getrennt vorgestellt.

\begin{longtable}{lr>{\textcolor{red}\bgroup}r<{\egroup}r}
  \textbf{Bereich} & \textbf{Einnahmen [€]} & \textbf{Ausgaben [€]} & \textbf{Saldo [€]} \\
  \midrule
  \endfirsthead
  \multicolumn{3}{c}{\emph{(Fortsetzung von vorheriger Seite)}} \\
  \\
  \textbf{Bereich} & \textbf{Einnahmen [€]} & \textbf{Ausgaben [€]} & \textbf{Saldo [€]} \\
  \midrule
  \endhead
  \\
  \multicolumn{3}{c}{\emph{(Fortsetzung auf nächster Seite)}} \\
  \endfoot
  \endlastfoot
  Ideeller Bereich: Allgemein       & 28.194,36  &  -1.981,80 &  26.212,56 \\
   Bekleidung                       &     31,00  &       0,00 &      31,00 \\
   Mitgliedsbeitrag                 & 27.207,32  &     -20,00 &  27.187,32 \\
   Pfandgeld Schlüssel              &     40,00  &     -20,00 &      20,00 \\
   Spenden                          &    916,04  &       0,00 &     916,04 \\
   Verein allgemein                 &      0,00  &    -271,77 & \textcolor{red}{-271,77} \\
   Kontoführungsgebühren            &      0,00  &    -294,82 & \textcolor{red}{-294,82} \\
   Vereinsserver                    &      0,00  &  -1.375,21 & \textcolor{red}{-1.375,21} \\
  \midrule
  Ideeller Bereich: Projekte        & 18.621,05  & -20.958,11 & \textcolor{red}{-2.337,06} \\
   3D-Drucker                       &     60,10  &       0,00 &      60,10 \\
   Bandsäge                         &    810,00  &       0,00 &     810,00 \\
   CoderDojo                        &      0,00  &     -49,50 &     \textcolor{red}{-49,50} \\
   Freifunk                         &  1.091,55  &    -789,01 &     302,54 \\
   Laser-Cutter                     &  5.762,30  & -11.798,99 & \textcolor{red}{-6.036,69} \\
   Maker vs Virus                   & 10.877,00  &  -7.966,18 &   2.910,82 \\
   Schneidplotter                   &      6,00  &       0,00 &       6,00 \\
   Stickmaschine                    &     14,10  &    -261,45 & \textcolor{red}{-247,35} \\
   Bastelmaterial                   &      0,00  &     -92,98 & \textcolor{red}{-92,98} \\
  \midrule
  Ideeller Bereich: Space           &    708,94  & -21.534,44 & \textcolor{red}{-20.825,50} \\
   Einrichtung                      &     56,27  &  -3.952,72 & \textcolor{red}{-3.896,45} \\
   Miete und Nebenkosten            &    652,67  & -17.319,04 & \textcolor{red}{-16.666,37} \\
   Verbrauchsmaterial               &      0,00  &    -262,68 & \textcolor{red}{-262,68} \\
  \midrule
  Mankobuchungen                    &      0,28  &       0,00 &       0,28 \\
  \midrule
  Umbuchungen                       &  1.738,30  &  -1.738,30 &       0,00 \\
  \midrule
  Wirtschaftlicher Geschäftsbetrieb &  2.871,60  &  -2.227,11 &     644,49 \\
  \midrule
  Zweckbetriebe                     &      0,00  &       0,00 &       0,00 \\
  \midrule
  \midrule
  \textbf{Gesamt}                   & \textbf{52.134,53} &  \textbf{\textcolor{red}{-48.439,76}} & \textbf{3.694,77} \\
\end{longtable}

Im allgemeinen ideellen Bereich sind hauptsächlich Einnahmen durch 
Mitgliedsbeiträge (27.207 €) zu verbuchen.
Dazu kommen nicht zweckgebundene Spenden (also nicht für den Laser-Cutter oder
Maker-vs-Virus) in Höhe von 916 €.
Besonders ist, dass es in diesem Jahr eine Ausgabe bei den Mitgliedsbeiträgen gab:
Hier wurde ein zu viel bezahlter Mitgliedsbeitrag zurück überwiesen.
Weitere Ausgaben in diesem Bereich sind der Vereinsserver mit 1.375 €,
sowie Kontoführungsgebühren mit 294 €.
Insgesamt wurde in diesem Bereich ein Überschuss von 26.212 € erreicht.

Im projektbezogenen ideellen Bereich sind große Einnahmen im Crowdfunding
für die Banssäge (810 €), sowie den Laser-Cutter (5.762 €) zu nennen.
Dem gegenüber steht besonders die Ausgabe für den Laser-Cutter (11.789 €).
Das Projekt CoderDojo hat in diesem Jahr nicht stattgefunden;
dort ist es daher nicht zu Einnahmen oder Ausgaben gekommen.
Das Projekt Freifunk hat zeitweise ein finanzielles Defizit aufgebaut.
Nach einem Spendenaufruf ist dies nun wieder ausgeglichen.
Insgesamt ist hier festzustellen, dass der projektbezogene ideelle Bereich trotz der
großen Ausgaben nur ein Minus von ca. 2.000 € aufweist.
Helena stellt noch einmal fest, dass wir gut darin geworden sind
Geld auszugeben, aber auch neues zu bekommen.

Im space-bezogenen ideellen Bereich haben wir eine Rückzahlung von 
650 € von unserem Stromanbieter erhalten.
Dem gegenüber stehen Ausgaben von ca. 17.300 € für Miete und Nebenkosten (inkl. Strom).

Die Bereiche Vermögensverwaltung, sowie Zweckbetriebe sind in diesem Jahr
ohne Umsatz.

Im wirtschaftlichen Geschäftsbetrieb wurde ein Überschuss von 644 €
durch Einnahmen aus der Matekasse erreicht.

Im Bereich Mankobuchungen ist es zu einem Überschuss von 0,28 € gekommen.
Mankobuchungen werden immer dann durchgeführt, wenn das Kassenbuch einer
Barkasse nicht zum tatsächlichen (gezählten) Bestand passt.

Die Summe des Bereiches Umbuchungen beträgt, wie erwartet, 0 €.

Insgesamt wurden somit 52.134 € Einnahmen, sowie 48.439 € Ausgaben gebucht.
Dies entspricht einem Überschuss von 3.690 €.
In diesem Überschuss sind zweckgebundene Spenden von Maker-vs-Virus und 
Freifunk noch enthalten.

### Vorhandenes Kapital

Helena zeigt eine Übersicht des vorhanden Kapitals über alle Konten im Verlauf
des Jahres.
In diesem sind die größeren Ausgaben wie Laser-Cutter, Maker-vs-Virus und
Bandsäge zu erkennen.
Es ist zu erkennen, dass der Bestand trotz der großen Ausgaben nach oben
geht.

Ansschließend wird eine Übersicht des vorhandenen Kapitals seit Anfang des
Vereins gezeigt.
Hier ist wieder zu erkennen, dass das vorhandene Kapital über die Zeit
ansteigt.
Helena weist darauf hin, dass dies nicht Zweck eines gemeinnützigen Vereins
ist.

### Mittlere monatliche Einnahmen und Ausgaben

Aktuell hat der Verein mittlere monatliche Einnahmen durch Mitgliedsbeiträge
in Höhe von 2.265 €.
Dazu kommen mittlere monatliche Einnahmen in Höhe von 101 € durch Spenden.
Das Projekt Freifunk erreicht circa 90 € mittlere monatliche Spenden.

Dem gegenüber stehen monatliche Verpflichtungen in Höhe von
730 € für Miete und Nebenkosten,
281 € für Strom,
ca. 100 € für den Vereinsserver, sowie
39 € für das Internet im Space.

### Mitgliederentwicklung

Trotz der Pandemie sind in Summe neue Leute dazu gekommen und weniger ausgetreten.
Im Moment hat der Verein 108 (ordentliche und Förder-) Mitglieder.
Während der Pandemie ist aber eine Abflachung der Mitgliederentwicklung zu erkennen.

### Bestände

Zum 30.04.2021 hatte der Verein Bestände in Höhe von
926 € in der Barkasse, sowie
18.742,03 € auf dem Girokonto.

\begin{longtable}{llr}
  \textbf{Nr.} & \textbf{Name} & \textbf{Bestand zum 30.04.2021 [€]} \\
  \toprule
  \endfirsthead
  100          & Barkasse                              &      926,83 \\
   100-1       & 3D-Drucker Filamentspenden            &        0,00 \\
   100-2       & Pfand für physische Schlüssel         &      140,00 \\
   100-3       & Spenden für Plotter-Material          &        0,00 \\
   100-4       & Spenden für Stick-Material            &        0,00 \\
  \midrule
  101          & Erstattungskasse Verbrauchsmaterial   &       98,41 \\
  \midrule
  102          & Matekasse                             &      261,45 \\
  \midrule
  200024917    & Girokonto                             &   17.701,38 \\
   200024917-1 & Rückstellungen Girokonto              &    3.160,00 \\
   200024917-2 & Pfandgeld auf Girokonto               &      360,00 \\
  \bottomrule
               &  \textbf{Summe}                & \textbf{ 22.648,07} \\
\end{longtable}

Helena weist noch einmal darauf hin, dass wir aus ihrer Sicht zu viel Geld haben.

### Rückstellungen

Die Rückstellung über 160 € für eine eventuelle Erhöhung der Mietsicherheit aus 2016
wurde endlich aufgelöst.
Der Verein hat weiterhin eine Rückstellung von 3.000 € für den Fall, dass
viele Mitglieder gleichzeitig austreten.
Mit dieser Rückstellung könnten dann offene Forderungen (z.B. Miete) beglichen werden.

### Ausblick

Im folgenden Geschäftsjahr sollte der Verein mehr Geld ausgeben.

Darüber hinaus wird der Verein in wenigen Wochen 10 Jahre alt.
Mit Ablauf des Jahres erreichen wir daher die 10-Jahres Frist, mit der die ersten
Unterlagen nicht mehr aufbewahrt werden dürfen.
Eine Aufgabe des neuen Vorstandes wird es daher sein, herauszufinden welche
Unterlagen nun vernichtet werden müssen, welche weiter aufbewahrt werden dürfen
und welche davon wir auch wirklich aufbewahren wollen.
Hierzu wird unter anderem ein Umbau des aktuellen Buchhaltungssystems notwendig
sein, um die abgelaufene Vergangenheit datenschutzkonform wegzuwerfen.

Im folgenden Jahr sollen die Papierstrichlisten abgeschafft und durch eine
Digital-Version ersetzt werden.

### Fragen

Die Anwesenden stellen folgende Fragen:

* Wie sieht die Fluktuation der Mitglieder in anderen Jahren aus?
    * Helena: Hat diese Information nicht vorbereitet und verweist auf den Plot 
      mit dem Verlauf der Mitgliederzahlen.
    * Chrissi ergänzt, dass wir aktuell bei der Mitgliedsnummer 200 sind.
    * Reneger wift ein, dass die Fluktuationsrate besonders mit Hinblick
      auf eine Finanzierung eines eventuellen neuen Spaces interessant.
    * Die Mitgliederversammlung beauftragt den neuen Vorstand, anonymisierte
      Statistiken über ein- und ausgetretene Mitglieder in die
      Vorstandssitzungsprotokolle aufzunehmen.
* Es wird gefragt, ob es *Karteileichen* gibt, die noch Mitgliedsbeiträge zahlen.
    * Helena: Mitglieder, die noch ihren Beitrag zahlen werden nicht als 
      Karteileichen betrachtet.
    * Es wird angemerkt, dass man diese *Karteileichen* ja auch nicht aufschrecken
      möchte.
    * Helena ergänzt, dass diese, wie alle Mitglieder, einmal pro Jahr einen
      Kontoauszug bekommen.
* Helena regt an, darüber nachzudenken, die Beiträge zu senken, falls es aufgrund
  der hohen Immobilienpreise nicht zu einem neuen Space kommt.
* Reneger fragt, ob wir für den aktuellen Space bereits aus der garantierten
  Mietoption des Mitvertrags herausgewachsen sind.
    * Es wird festgestellt, dass wir aus der sicheren Verlängerungsoption
      herausgewachsen sind, und dass wir aktuell im jährlichen Verlängerungsmodus sind.
* Helena merkt an, dass die Grenze für frei verfügbares Guthaben bei kleinen Vereinen
  seit einer aktuellen Gesetzesänderung auf 40.000 € (exkl. Rücklagen) angehoben wurde.
* Reneger: Ist es richtig, dass wir ohne die großen Ausgaben (Lasercutter, Bandsäge und 
  Maker-vs-Virus) einen Überschuss von circa 8.000 € erreicht hätten?
    * Helena: Ja, das ist richtig.
      Nur durch die großen Ausgaben haben wir eine schwarze Null erreicht.

Tätigkeitsbericht
-----------------

Chrissi^ berichtet über die Tätigkeiten des Vorstands während der letzten Vorstandsperiode.
Ausgenommen sind dabei *Finanzthemen*, da diese bereits von Helena vorgestellt wurden.

Seine Präsentation ist im 
[Internet](https://stratum0.org/wiki/Datei:Stratum0_Vorstand_Taetigkeitsbericht_2020.pdf)
verfügbar.

### Von der MV gewählte Funktionen im Verein

Chrissi beginnt mit einer Übersicht der Aufgaben und Personen, die im letzten Jahr
aktiv waren.

Außerhalb des Vorstandes:

* Rechnungsprüfer: Jan Lübbe (shoragan) und Angela Uschok (sonnenschein)
* Vertrauensperson: Daniel Sturm (dStulle)

Im Vorstand:

* Vorsitzender: Lars Andresen (larsan)
* Stellvertretende Vorsitzende: Felicitas Jung (Feli)
* Schatzmeisterin: Helena Schmidt (ktrask)
* Beisitzer\_in: Chris Fiege (Chrissi^), Linda Fliß (chamaeleon), Roland Hieber (rohieb)

Vertretungsberechtigt sind laut Satzung immer zwei Personen von Vorsitzender, Stellv.
Vorsitzende und Schatzmeisterin.

Weitere Aufgaben wurden im Vorstand wie folgt aufgeteilt:

* Schatzmeister-Gehilfin: chamaeleon
* Verwaltung von Schlüsseln und Ländereien: larsan
* Mitgliederverwaltung: ktrask, Chrissi^
* Schriftführung (Protokolle): Chrissi^

### Kommunikation im Vorstand

Kommunikation im Vorstand geschieht über mehrere, zum Teil öffentliche, Wege:

* Im Wiki: Auf der Wiki-Seite [\[\[Nächste Vorstandssitzung\]\]](https://stratum0.org/wiki/N%C3%A4chste_Vorstandssitzung)
  wird die Tagesordnung für die nächste Sitzung zusammengestellt und im Nachhinein das
  öffentliche Protokoll der jeweiligen Sitzung veröffentlicht.
* Die Mailingliste *vorstand@stratum0.org* ist von außen offen für z.B. Anträge, und wird
  intern vom Vorstand zur Diskussion genutzt.
* IRC: `#stratum0v` auf LiberaChat, auch als `#stratum0:stratum0.org` ins Matrix gebrückt
* Monatliche Arbeitstreffen: Jeweils am 1. Dienstag des Monats
* Quartalsweise Vorstandssitzungen

Weiter stellt Chrissi^ Statistiken zur Kommunikation im Vorstand vor.
Er weist hierbei darauf hin, dass die Zahlen aufgrund der längeren Vorstandsperiode entsprechend
höher sind.

* Es wurden 1818 E-Mails in 470 Threads auf der Vorstandsmailingliste ausgetauscht.
* Es wurden 7 Vorstandssitzungen durchgeführt.
* Es wurden 67 Umlaufbeschlüsse gefasst.
* Es wurden 64 Seiten öffentliche Sitzungsprotokolle erzeugt.
  Es wird darauf hingewiesen, dass manche nicht-öffentliche Dinge, wie z.B. Mitgliedsangelegenheiten,
  aus Datenschutzgründen nicht veröffentlicht werden.

Darüber hinaus werden seit diesem Jahr maschinenlesbare Umlaufbeschlüsse zur Verfügung gestellt.
Diese erlauben einen automatischen Abgleich mit der Finanzbuchhaltung.
Technisch sind diese als YAML in Markdown umgesetzt und werden zusammen mit den Sitzungsprotokollen
auf [data.stratum0.org](http://data.stratum0.org) veröffentlicht.

### Besondere Themen

Folgende erwähnenswerte Themen wurden in diesem Jahr bearbeitet:

* Inhaltsversicherung gegen Elementarschäden und Diebstahl:
  Dieses Thema stellt sich als nicht trivial heraus, da vieles im Space nicht dem Verein gehört.
  Darüber hinaus gab es dieses Jahr durch Corona weniger Aktivität im Space, was ein wenig
  den Druck aus diesem Thema genommen hat.
  Aktuell warten wir auf Unterlagen vom Versicherungsmakler.
  Wir haben weiterhin eine Vereins-Haftpflichtversicherung.
* Lasercutter:
  Der Umgang mit diesem teuren Gerät hat sich als schwierig herausgestellt.
  Dazu waren in diesem Jahr Einweisungen auf das Gerät durch Corona nur schwer möglich.
  Aktuell wird ein verpflichtendes analoges Logbuch geführt und 
  das Gerät ist über ein Zahlenschloss gesichert.
  larsan und Pecca geben Einweisungen, nach denen Personen den Code zum Schloss bekommen.
  Wir sind uns bewusst, dass das Schloss einfach zu überwinden ist.
  Allerdings würden Personen dann vorsätzlich handeln.
* Arbeitssicherheit:
  Dieses Thema wurde auf dem Normalverteiler aufgebracht.
  Offene Fragen sind: Fluchtwegsituation und -beschilderung, Feuerlöscher.
  Im letzten Jahr im Sommer wurde eine Prüfung von Elektrogeräten durchgeführt.
  Hierbei wurden Geräte aussortiert und zum Schrott gefahren.
  (Leider lagert der Schrott zur Zeit noch bei Chrissi^ im Keller, da ELPRO
  coronabedingt geschlossen hat.)
  Es gibt im Space nun einen Verbandskasten (für größere Verletzungen)
  und eine Space-Apotheke (für kleinere Verletzungen).
  Für wiederkehrende Themen gibt es nun Wiedervorlagen im Vorstandssitzungsprotokoll.
* Durch Corona war die Nutzung des Spaces stark eingeschränkt.
  Über den Sommer gab es eine Besserung dieser Situation.
  Aber mittlerweile ist der Space wieder *sehr* geschlossen.
  Der Vorstand hat bei diesem Thema immer versucht, ein sinnvoller Teil des
  Infektionsschutzes zu sein.
  Daher erlauben wir zur Zeit auch nur eine Person bzw. einen Haushalt zu einer
  Zeit im Space.
  
Chrissi^ weist noch darauf hin, dass sich der Vorstand immer über Input aus dem Verein
und über Beteiligung an Vorstandssitzungen und Arbeitstreffen freut.

### Fragen

Von den Anwesenden werden folgende Fragen gestellt:

* Jemand sucht einen Hohlstecker für ein Projekt und fragt, ob dieser eventuell beim
  Elektroschrott dabei ist.
    * Chrissi^ will nach der Sitzung danach suchen.
* Es wird gefragt, wie es mit Privilegien für Corona-Geimpfte aussieht.
    * Chrissi^ sagt, dass dieser Punkt vermutlich ein Thema für die nächsten 
      Vorstandssitzungen sein wird.

Jahres(abschnitts)bericht
-------------------------

Der Jahresbericht wird von chamaeleon unter dem Titel '*Das längste Jahr ever*' gehalten.
Der vollständige Bericht ist im 
[Internet](https://stratum0.org/wiki/Datei:Stratum0_Jahresbericht_2020.pdf)
verfügbar.

### Corona

Der Space wurde im März 2020 für Besucher und Mitglieder komplett geschlossen.
Allerdings konnte kurz darauf eine zentrale Produktionsstätte für 
Maker-vs-Virus im Space eingerichtet werden.
Über den Sommer wurden dann unterschiedliche Regelungen mit 5 bis 10 Personen im Space,
abhängig von den Regelungen des Landes Niedersachsens, umgesetzt.
Seit dem November gibt es nun ein Reservierungssystem in einem Etherpad.
Dies funktioniert einigermaßen: Personen tragen vor dem Besuch ein, wann sie den Space
nutzen wollen und er steht dann in diesem Zeitraum für die Nutzung zur Verfügung.
Kontaktnachverfolgung wird aktuell über datensparsame Kontaktnachverfolgungszettel
durchgeführt.
Als Hackerspace haben wir uns bewusst gegen offene Aktenordner oder LUCA-Apps entschieden.

### Gruppen und regelmäßige Veranstaltungen

Durch Corona sind Veranstaltungen in Person im Wesentlichen ausgefallen.
Immerhin einige wurden online weitergeführt.
Dies sind nach unserem Wissen: 
Freifunk (jeden Mittwoch im Mumble),
Digitalcourage,
Malkränzchen (Mittwochs im Jitsi),
Vorträge (im BigBlueButton) mit Aufzeichnungen und einem Stream zu YouTube.

Bei den Vorträgen haben 10 von 12 Terminen online stattgefunden.
Der erste Termin war dabei spontan als interner Ersatz für die Chemnitzer Linuxtage
(bei denen Leute trotzdem ihre Vorträge halten wollten) entstanden.
Insgesamt gibt es bei den Vorträgen rege Beteiligung.

Bei Freifunk Braunschweig wird weiterhin am *Project Parker* gearbeitet, dass bald hoffentlich
schnelleres und besseres freies WLAN liefern soll.
Das Projekt ist jetzt in eine öffentliche Beta-Phase gewechselt.
Freifunk Braunschweig ist darüber hinaus zwei große Kooperationen eingegangen:
30 Feuerwehren in BS haben ihre Feuerwachen mit Freifunk ausgestattet.
Dazu kommen 8 Kirchen im Pfarrverband Südstadt.
Über beides wurde in der Braunschweiger Zeitung berichtet.

Der Vereinsserver wurde auf neue dedizierte Hardware umgezogen.
Uns stehen nun mehr RAM, mehr CPU, und jetzt auch SSDs zur Verfügung.
Der Umzug auf die neue Maschine wurde von Shoragan durchgeführt.
Linda dankt Shoragan, aber auch allen anderen Admins für die Betreuung unserer Dienste.

### Events und Veranstaltungen

Durch Corona haben Events in diesem Jahr hauptsächlich digital stattgefunden.
Dennoch wurden folgende Events durchgeführt:

* Digitalcourage Opt-Out-Day:
  Hat noch vor Corona im Space stattgefunden.
  Es wurden gemeinsam DSGVO-Anträge bei unterschiedlichen Diensten gestellt.
* Wurst-Workshop:
  Wurde live gestreamt und mit wenigen Leuten vor Ort im Space durchgeführt.
* Unboxing und Aufbau neuer 3D-Drucker mit Live-Stream.
* Remote Lasercutter-Workshop bei den Chemnitzer Linuxtagen 2021 durch Pecca.
* Es gab eine dezentrale Silvesterfeier im Jitsi.
* RC3 - Remote Chaos Expericence
    * Online-Ersatz für den Chaos Communication Congress.
    * Es gab Vorträge von Mitgliedern, z.B. vom Freifunk Braunschweig.
    * Im Space gab es einen Sticker-Exchange:
      Es wurden Briefe mit Rückumschlägen und Stickern entgegen genommen.
      Auf die Rückumschläge wurden die vorhandenen Sticker dann wieder verteilt
      und zurück gesendet.
      Insgesamt wurden so hunderte Briefe mit mehreren Kilogramm Stickern gefüllt.
    * Neben Stickern wurden uns auch Schokolade und Spenden zugesendet.
* Maker-Vs-Virus:
    * Dieses Projekt wird von larsan und reneger vorstellt.
    * Am Anfang der Pandemie gab es einen Engpass an Schutzausrüstung für ärtztliche Einrichtungen.
    * Mit Lasercutter und 3D-Druck wurden daraufhin im Space Gesichtsschilde
      für lokale Ärtzte und Apotheken produziert. 
      Ein Hauptverteilzentrum für die Region war bei uns im Space.
      Die Initiative selbst kam dabei von außen, wurde bei uns aber gut aufgenommen.
      Insgesamt waren viele Leute aus der Region mit ihren 3D-Druckern dabei.
      Zeitweise gab es einen organisierten Fahrdienst.
      Es wurden sogar Gesichtsschilde aus Kunststoff-Spritzguss hergestellt.
    * Insgesamt wurden in der Region so 6.500 Gesichtsschilde hergestellt und an 330 bis 340
      Einrichtungen in der Region verteilt.
      Es stehen sogar noch restliche Gesichtsschilde im Fahrradkeller und warten auf Verwendung.
      Besonderheiten waren die Sendung einer Palette mit 500 Gesichtsschilden nach Lesbos
      (Griechenland), sowie ein paar tausend an die äthiopische Botschaft in
      Deutschland.
    * Nachdem die Versorgungslage bei Gesichtsschilden sich gebessert hatte, wurden noch
      Earsaver hergestellt, von denen mehr als 10.000 Stück über Braunschweiger Apotheken
      verteilt wurden.
* Hey Alter:
    * Dieses Projekt wird ebenfalls von larsan vorgestellt.
    * Bei Hey Alter handelt es sich nicht direkt um ein Projekt im Stratum 0.
      Allerdings grenzt dieses nah an unsere Tätigkeiten an.
      Das Projekt ist aus dem Sandkasten der Uni und dem Trafo-Hub entstanden.
      Larsan ist dort aktuell sehr aktiv.
    * Die Idee ist, alte Rechner an junge Leute (in der Regel Schüler) für Homeschooling zu verteilen.
      Mittlerweile hat sich in Braunschweig ein eigener e.V. hierfür gegründet und es wurden
      etwa 1.000 Geräte verteilt.
      Es gibt Spin-Offs in ganz Deutschland. Aktuell sind es 25 Standorte.
      Das Projekt hat mittlerweile mehrere Preise bekommen: 
      Den Corona-Sonderpreis des Landes Niedersachsen und den "Gemeinsam-Preis" der Stadt Braunschweig.
    * Frage aus dem Publikum: Was macht der Stratum 0 dabei?
      Larsan stellt fest, dass der Stratum 0 dabei Infrastruktur für Kommunikation und 
      Build-Infrastruktur in unserem GitLab zur Verfügung stellt.

### Anschaffungen

Im letzten Jahr wurden folgende Dinge angeschafft:

* Eine Overlock-Nähmaschine
* Ein neuer Kompressor für die Werkstatt
* Ein neuer 3D-Drucker in Verbindung mit Maker-Vs-Virus
* Eine Bandsäge
* Spaceapotheke (im Flur neben dem Verbandskasten)
* Der Chat im BigBlueButton ergänzt:
  Eine Entlötstation und neue Netzteile für die Lötecke
* Der Lasercutter:
  Hierzu war eine größere Aufräumaktion im Frickelraum notwendig.
  Darüber hinaus war die Türzarge zum Frickelraum nicht breit genug
  und musste ausgebaut werden.

Nach diesem TOP wird eine Pause von 16:01 Uhr bis 16:16 Uhr eingelegt.

Bericht der Vertrauensentität
-----------------------------

dStulle berichtet, dass keine Themen an ihn herangetragen wurden.
Er schließt daraus, dass entweder auch nichts passiert ist, oder er es nicht
mitbekommen hat.

Er weist darauf hin, dass er als 
Vertrauensperson Ansprechpartner für Probleme ist, die sich nicht mit dem Vorstand lösen lassen.

### Fragen

Von den anwesenden wurden folgende Fragen gestellt:

* Es wird gefragt, wie man ihn erreichen kann.
    * dStulle verweist auf die Kontaktwege, die er im 
      [Wiki](https://stratum0.org/wiki/Benutzer:DStulle)
      angegeben hat.
      Dies sind: Mail und Twitter (*@dStulle*).


Bericht der Rechnungsprüfer
---------------------------

Angela (sonnenschein) berichtet:

Da es eine neue Schatzmeisterin gab, wurden Kassenprüfungen quartalsweise
durchgeführt.
Die erste Zwischenprüfung im Mai 2020 wurde remote durchgeführt.
Hierbei wurden die Barkassen im Space nicht gezählt.
Die Zwischenprüfung im September 2020 wurde im Space durchgeführt, da zu diesem 
Zeitpunkt Gremien von Vereinen tagen durften.
Die Zwischenprüfungen im Januar und April 2021 wurden wieder remote durchgeführt.

Die Hauptprüfung am 01.05.2021 wurde hybrid durchgeführt:
Angela und Jan waren im Space und konnten so Barkassen und Buchführung prüfen.
Helena und Chrissi^ waren online dabei.

Angela berichtet, dass viele Altlasten erledigt wurden und lobt Helena dafür.
Die maschinenlesbaren Umlaufbeschlüsse mit der Finanzübersicht werden
positiv erwähnt.

Angela berichtet, dass für Maker-vs-Virus viele Spenden eingangen sind, und fragt,
wie diese am besten ausgegeben werden können.

Außerdem wird berichtet, dass die Zuordnung von Beitragszahlungen zu den jeweiligen
Mitgliedskonten in den Kassenprüfungen von Hand geprüft wird.
Bei über 100 Mitgliedern kommen hier viele Buchungen zusammen.
Die Kassenprüfer bitten daher die Mitglieder, ihre Beiträge lieber jährlich, halbjährlich
oder quartalsweise zu überweisen.

Zum Ende der Hauptprüfung sind nur noch Kleinigkeiten übrig geblieben, die sich
gut durch das Remote-Arbeiten erklären lassen.

Die Kassenprüfer empfehlen daher den Vorstand zu entlasten.

Entlastung des Vorstandes
=========================

Larsan fragt, ob es Wortmeldung zu diesem Tagesordnungspunkt gibt.
Es gibt keine Wortmeldungen.

Larsan fragt, ob eine Einzelentlastung des Vorstands gefordert wird.
Es gibt hierzu ebenfalls keine Wortmeldungen.

Shoragan legt daher eine Abstimmung nur Entlastung es Vorstands im
OpenSlides an.
Hierbei kommt die Frage auf, die wie Abstimmung technisch umgesetzt wird:
Bei dieser Abstimmung darf sich der Vorstand nicht beteiligen.
Dies können wir aber im OpenSlides auf die Schnelle nicht abbilden.
Es wird vorgeschlagen, eine nicht-namentliche Abstimmung durchzuführen,
bei welcher der Vorstand nicht mit abstimmen *soll*.
Sollten die Mehrheitsverhältnisse am Ende der Abstimmung nicht eindeutig
sein, würden wir überlegen, was wir als nächstes tun.

Es wird abgestimmt:
*Die Versammlung möge beschließen, dass der Vorstand als ganzes entlastet wird.*
Die nicht-namentliche Abstimmung im OpenSlides ergibt:

* 36 Stimmen dafür
* 0 Stimmen dagegen
* 1 Enthaltung

Der Vorstand wird somit als Ganzes entlastet.

Abstimmung über Änderung der Beitragsordnung (aus Versehen vorgezogen)
======================================================================

(Dieser TOP war eigentlich nach der Wahl des Vorstands und der Rechnungsprüfer auf der 
Tagesordnung und wurde aus Versehen vorgezogen.)

Es steht ein Änderungsantrag für die Beitragsordnung zur Abstimmung.
Dieser war Teil der Einladung und ist ebenfalls im 
[Internet](https://gitli.stratum0.org/stratum0/dokumente/-/merge_requests/3/diffs)
zu finden.
Diese Änderung will in §0 Beitragssätze, Abs. 2 den Text 
*"Schüler, Studenten, Auszubildene, Empfänger von Sozialgeld"*
durch
*"Schüler, Studenten, Auszubildene (einschließlich Referendaren), Empfänger von Sozialgeld"*
ersetzen.
Larsan zeigt den Änderungsvorschlag auf dem Projektor im OpenSlides.

Hierzu ergibt sich folgende Diskussion:

* Pecca fragt, ob diese Änderung so explizit geregelt werden müsse.
* Feli weist darauf hin, dass in Berlin z.B. Referendare nicht als Auszubildene gelten.
  Feli fände es gut, wenn es deutschlandweit so wäre, und schlägt daher vor, damit
  im Space zu beginnen.
* Mehrere Anwesen äußern Zweifel daran, ob dies so sinnvoll wäre.
  Es wird gefragt, ob man dann nicht auch explizit FSJler und ähnliches mit aufnehmen solle.
* dStulle: Es geht bei einer solchen Änderung aber auch um die Außenwirkung.
* Feli: Weist darauf hin, dass es dazu keine zentrale Regelung gibt.
  Man könne aber z.B. Regelungen von Verkehrsbetrieben als Beispiele heranziehen.
* Chrissi^ schlägt vor, die Änderung erst einmal zu übernehmen und den Abschnitt später
  anzupassen, wenn sich Bedarf ergibt.
* Feli und Reneger weisen darauf hin, dass man die Unterscheidung zwischen einer individuellen
  und einer nicht-individuellen Regelung gut herausstellen müsse.
* Larsan: Der Merge-Request stand nun seit 11 Monaten zur Diskussion und war Teil der Einladung
  zur Mitgliederversammlung.
  Er schlägt ebenfalls vor die Änderung so zu übernehmen und dann zur nächsten MV noch einmal
  anzupassen.
* Rohieb fragt, ob eine einfache Mehrheit der akkreditierten Mitglieder ausreicht.
  Larsan bestätigt dies.

Es wird eine Abstimmung im OpenSlides angelegt und abgestimmt.
Es ergeben sich:

* 30 Stimmen dafür
* 2 Stimmen dagegen
* 9 Enthaltungen

Der Antrag wird somit angenommen.

Larsan stellt zum Schluss noch einmal fest, dass angeregt wurde, die Passage noch einmal zu 
überarbeiten.
Wenn dies geschehe, solle dies im GitLab und auf der Mailingliste dokumentiert werden.


Glaskugelei, Feedback und Input für zukünftigen Vorstand
========================================================

Larsan ruft zu Wortmeldungen auf, was der zukünftige Vorstand anders machen sollte.

* Helena: Es muss geklärt werden muss, was mit Daten und Dokumenten
  passiert, wenn dieses Jahr die 10-jährige Aufbewahrungsfrist endet.
* Larsan: Es wird zu klären sein, wie es im Space mit "2D-" bzw. "normalen" Druckern
  weiter geht.
  (Anmerkung des Protokollführers:
  Das vorhandene Multifunktionsgerät funktioniert aktuell nicht.)
* Larsan zu Space 3.0:
  Im Jahr 2020 kam die Nibelungen-Wohnbaugesellschaft auf uns zu.
  Sie bauen östlich von BS Energy im Neubaugebiet.
  Es wird dort ein größeres Gebäude am Luftschifferweg, Ecke Ringgleis, geplant.
  Es gab von Nibelungen Überlegungen, was dort hinein kommen soll.
  Eine Überlegung war, ob wir da gut reinpassen.
  Es gab daher ein Treffen mit Nibelungen, auf dem wir uns, auch über Anforderungen,
  ausgetauscht haben.
  Das Gebäude befindet sich aktuell in der Planungsphase und wird nicht vor 2024 fertig.
  Diese Gedanken sind daher eher langfristig.
  Uns würden dort circa 300 m² zur Verfügung stehen.
  Offen ist die Frage, wie man das finanzieren könnte.
  Aktuell können wir uns das nicht leisten.
  Allerdings gehört Nibelungen zu 100% zur Stadt Braunschweig, sodass da vielleicht
  eine Unterstützung möglich sein könnte.

Larsan fragt noch einmal, ob noch weitere Themen für den zukünftigen Vorstand
offen sind?

* Larsan: Stellt mehr Anträge!
    * Pecca weist darauf hin, dass sie noch einen laufenden Antrag für Toner für den
      2D-Drucker hat.
    * Chrissi^ findet es immer gut, wenn Anträge von Mitgliedern kommen und diese
      in CC auch immer auf den Normalverteiler gehen, da das die Transparenz
      erhöht.
      Außerdem falle es dem Vorstand schwerer, einem Antrag nicht zuzustimmen,
      wenn es dazu eine gute Diskussion gab.
    * Larsan weist bei dieser Gelegenheit noch einmal auf die [Erstattungskasse] hin,
      die bei 3 bis 4 Unterstützer bis zu 50 EUR freigibt.

Es gibt keine weiteren Wortmeldungen. 

[Erstattungskasse]: https://stratum0.org/wiki/Erstattungskasse

Wahlen des Vorstands und der Rechnungsprüfer
============================================

Larsan übergibt die Versammlungsleitung an den Wahlleiter mist.
mist bedankt sich für das Vertrauen und berichtet, dass er sich schon im Vorfeld
mit dem Vorstand abgestimmt hat.
Es gibt mehrere Mitglieder, die auf geheime Wahlen bestehen würde.
Außerdem gebe es vermutlich durch ein Gewohnheitsrecht auch einen Anspruch
auf eine geheime Wahl.

Reneger stellt im OpenSlides einen Antrag, dass die Wahl als Online-Wahl
durchgeführt werden soll.
Gleichzeitig beantragt rohieb im BigBlueButton-Chat eine geheime Briefwahl
im Chat.
Wir gehen durch rohiebs Antrag davon aus, dass wir eine geheime Briefwahl
durchführen.

### Wahlverfahren

mist erklärt vor seiner Webcam (im BigBlueButton) den Briefwahlmodus:

* Jedes akkreditierte Mitglied bekommt per Post einen Briefumschlag mit 
  folgendem Inhalt:
    * Anschreiben mit Informationen und einer Liste der Unterlagen
    * Einen frankierten Rückumschlag
    * Einen Stimmzettelumschlag
    * Einen Wahlschein (inkl. einem persönlichem Geheimnis)
    * Einen Stimmzettel im Design wie in den Vorjahren
* Der Stimmabgabe den Stimmzettel falten und in den Stimmzettelumschlag tun.
* Den Stimmzettelumschlag verschließen, verkleben, und in der Mitte falten.
* Den Wahlschein unterschreiben.
* Den Wahlschein und den Stimmzettelumschlag in den Rückumschlag geben.
  Ob eine Absenderadresse abgegeben wird, ist egal.
* Der Rückumschlag muss bis Freitag 21.05.2021 im Stratum 0-Briefkasten
  angekommen sein.
  Dies kann per Post oder persönlich geschehen.
  Relevant ist, dass der Brief angekommen ist, und nicht der Poststempel.
* Am Samstag den 22.05.2021 wird es eine Live-Auszählung geben.
* Falls man bis zum 13.05.2021 noch keinen Umschlag erhalten hat, soll man
  sich bei mist melden.

mist eröffnet Wortmeldungen zum Wahlverfahren:

* DooM und Reneger halten die eingeplanten Postlaufzeiten für zu gering,
  da sie aktuell durchaus Laufzeiten von 3 bis 7 Tagen für einen Brief von 
  Braunschweig nach Gifhorn beobachtet haben.
  Shim ergänzt, dass in der Zeit auch noch ein Feiertag liegt.
* mist schlägt vor, die Wahl um zwei Wochen zu verlängern.
  Das endgültige Rückgabedatum steht dann im Anschreiben.
  Falls es keine Stichwahl gibt, wäre dann in vier Wochen ein neuer
  Vorstand bestimmt.
* Zu diesem Vorgehen gibt es keine weiteren Wortmeldungen.

mist schlägt Kasa und Fia als Wahlhelfer vor.
Feli bittet Fia um eine kurze Vorstellung.
Fia stellt sich per Webcam im BigBlueButton vor.

Anschließend werden im OpenSlides Kandidaten für die Ämter gesucht.
Mitglieder können sich dort als Kandidaten eintragen.

Nachdem die Listen geschlossen wurden stellen sich die Kandidaten, 
nach Ämtern sortiert, vor.
Es gibt folgende Kandidaten:

* Vorsitzende Entität: 
    * larsan

|

* Stellv. Vorsitzende Entität
    * Chrissi^
    * Reneger

|

* Geld verwaltende Entität
    * ktrask

|

* Beisitzende Entität
    * Lichtfeind / Jonas
    * Chrissi^
    * Josha
    * Reneger
    * Fototeddy
    * Linda / chamaeleon

Anschließend stellen sich die Kandidaten für das Amt der
Rechnungsprüfer vor.
Es wird kurz besprochen, ob hier eine Neuwahl überhaupt
notwendig ist, und ob diese in geheimer Wahl stattfinden muss.
mist schlägt vor, die Wahl für die Rechnungsprüfer
einfach mit über die geheime Wahl durchzuführen.
Hierzu gibt es keine Gegenstimmen.

* Rechnungsprüfer
    * Shoragan / Jan 
    * Sonnenschein / Angela

Es gibt keine weiteren Kandidaten.

Zum Amt der Rechnungsprüfer gibt es Fragen:

* Was sind die Aufgaben der Kassenprüfer?
    * Shoragan antwortet:
      Die Kassenprüfer sehen sich alle Kassen und die Buchungen darauf an.
      Sie prüfen, ob die Buchführung sinnvoll ist, und ob die nötigen
      Beschlüsse des Vorstands vorliegen, dass Mitgliedsbeiträge
      richtig zugeordnet sind, und das die Prozesse drum herum sinnvoll
      sind.
* Benötigt man dafür eine Vorbildung?
    * Shoragan antwortet:
      Nein, braucht man nicht.
      Erfahrung macht es einfacher.
      Falls sich jemand dafür interessiert, kann man bestimmt zu einer
      Zwischenprüfung dazu kommen.
    * Sonnenschein ergänzt:
      Da es auch zwei Prüfer sind, kann ein Prüfer einen neuen einlernen.

mist erklärt noch einmal den Wahlmodus:

* Pro Kandiat auf der Liste hat man je eine Stimme.
  Man kann also je Kandidat ein Kreuz machen oder eben 
  weglassen.
* Am Ende gewinnt die Person mit den meisten Stimmen.
* Die Auszählung der Ämter findet in der Reihenfolge auf dem 
  Stimmzettel
  (Anmerkung des Protokollführers: Entspricht der Reihenfolge
  der Kandidaten) statt.
  Hat jemand bereits ein Amt angenommen kann kein weiteres 
  übernommen werden.
  
Es gibt keine weiteren Wortmeldungen und die Versammlungsleitung
geht zurück an larsan.

Sonstiges
=========

Bestätigung der Vertrauensperson
--------------------------------

Larsan fragt nach Wortmeldungen zur Bestätigung der Vertrauensperson.

* Helena fragt, ob die aktuelle Vertrauensperson für die Mitglieder greifbar genug
  ist, um die Rolle gut auszufüllen?
  Aus ihrer Perspektive wäre das nicht so.
    * dStulle beschreibt seine Rolle eher als ein Grashalm, nachdem man greifen kann.
    * Er geht nicht aktiv auf Personen zu, sondern ist ein Ansprechpartner.
* Reneger findet es gut, wenn die Vertrauensperson jemand ist, der etwas weiter
  vom Space entfernt ist und somit von außen darauf blicken kann.
* Im Chat wird vorgeschlagen, dass die MV auch mehr als eine Vertrauensperson benennen
  könne.
* DooM: Im Zweifel kann jedes Mitglied auf andere zugehen und die Entität als
  Streitschlichter dazu ziehen.
  DooM findet es aber trotzdem gut, wenn man dort jemand externes hat.
  DooM hat eher das Gefühl, dass die Rolle selbst präsenter sein sollte.
* Claudia fände es gut, wenn man die Rolle und die Person an einer zentralen Stelle
  bekannt machen würde, und damit das Wissen um die Existenz leichter zu finden ist.
* mist: Findet es persönlich schwer, seine Probleme an unbekannte Personen zu kommunizieren,
  und fände es daher gut, wenn es auch jemanden gibt, den man kennt.
  
larsan schlägt an dieser Stelle vor,
die Rolle im Wiki bekannter zu machen, und 
eine weitere Person zu benennen.
dStulle findet beide Vorschläge gut.

Im OpenSlides wird eine Abstimmung angelegt:
Die MV möge beschließen, dass es zwei Vertrauenspersonen geben soll.

Die Abstimmung wird wie folgt geschlossen:

* 29 Stimmen dafür
* 4 Stimmen dagegen
* 9 Enthaltungen

Der Vorschlag wird somit von der MV angenommen.

Im OpenSlides wird eine Kandidatenliste eröffnet.
Es tragen sich folgende Kandidaten ein, die sich an dieser Stelle kurz
vorgestellt haben:

* Mika Eichmann / Fia
* Feli
* Tim / dadada
* dStulle
* mist
* Thole Goesmann / lisstem

Es wird im OpenSlides eine Wahl durch Zustimmung durchgeführt.
Die Abstimmung ergibt folgendes Ergebnis:

1. Fia (31 von 40 Ja-Stimmen, nimmt die Wahl an)
2. dStulle (29 von 40 Ja-Stimmen, nimmt die Wahl an)
3. Feli (19 von 40 Ja-Stimmen)
4. mist (18 von 40 Ja-Stimmen)
5. dadada (14 von 40 Ja-Stimmen)
6. Thole (17 von 40 Ja-Stimmen)

Fia und dStulle wollen sich absprechen und das Wiki überarbeiten.

Hacken Open Air
---------------

Reneger schlägt das Thema vor und wirft ein paar Gedanken ein:

* Die HOA-Orga ist gerade dabei herauszufinden, ob wir ein HOA durchführen
  können.
* Aktuell wollen wir das Pandemiegeschehen noch ein paar Wochen abwarten,
  um dann die rechtlichen Rahmenbedingungen zu kennen.
* Es soll Ende Mai oder Anfang Juni eine große Orga-Runde geben.

Terminfindung nächste Mitgliederversammlung
-------------------------------------------

Es wird vorgeschlagen die nächste Mitgliederversammlung wieder im Januar
durchzuführen.
Es entsteht eine kurze, aber ergebnislose Diskussion darüber, ob die Satzung
nun eine Mitgliederversammlung pro Jahr oder Kalenderjahr fordert.
Am Ende ergeht aus der Diskussion der Auftrag an den nächsten Vorstand, wieder eine
Mitgliederversammlung im Januar durchzuführen.

Sonstiges
---------

Folgende Punkte werden aus Zeitmangel vertagt:

* Weltherrschaft
* Anerkennung des RZL als Außenstelle des Stratum 0
* PGP-Key-Signing-Party

Auszählung der Briefwahl
========================

Nachtrag 2021-06-05 nach der Auszählung der Briefwahlunterlagen:

Die Auszählung der Briefwahl findet am 5. Juni 2021 im Stratum 0 statt.
Anwesend sind: mist (Wahlleitung), Kasa, und zeitweise Fia.
Die Öffentlichkeit wird via Stream im BigBlueButton hergestellt.
Chrissi^ führt Protokoll.
Das Protokoll wird nach der Auszählung über den Normalverteiler herumgeschickt und auch
von der Wahlleitung bestätigt.

Die Auszählung ergibt:

* 42 von 44 Stimmzetteln sind zurück gekommen.
* Unter den Rückläufern gab es keine ungültigen Wahlzettel.
* Unter den Rückläufern gab es keine ungültigen Stimmzettel.
* Unter den Rückläufern gab es auch keine ungültigen Stimmen.

Die Auszählung ist zu folgendem Ergebnis gekommen:

### Wahl der vorsitzenden Entität
<!-- don't merge header and list -->
* Lars Andresen (larsan): 42 von 42 Stimmen (100%)
* keine ungültigen Stimmen

Larsan wird in der BBB-Konferenz gefragt, ob er die Wahl annimmt.
Larsan nimmt die Wahl an.

### Wahl der stellvertretenden vorsitzenden Entität
<!-- don't merge header and list -->
* Chris Fiege (Chrissi^): 39 von 42 Stimmen (92.9%)
* René Stegmaier (reneger): 8 von 42 Stimmen (19.1%)
* keine ungültigen Stimmen

Chrissi^ wird in der BBB-Konferenz gefragt, ob er die Wahl annimmt.
Chrissi^ nimmt die Wahl an.

### Wahl der Geld verwaltenden Entität (Schatzmeister\*in)
<!-- don't merge header and list -->
* Helena Schmidt (ktrask): 42 von 42 Stimmen (100%)
* keine ungültigen Stimmen

ktrask wird telefonisch gefragt, ob sie die Wahl annimmt.
Das Gespräch wird im BBB mitgehört.
ktrask nimmt die Wahl an.

### Wahl der beisitzenden Entitäten
<!-- don't merge header and list -->
* Linda Fliß (chamaeleon): 39 von 42 Stimmen (92.9%)
* Christopher Helberg (fototeddy): 33 von 42 Stimmen (78.6%)
* Jonas Martin (lichtfeind): 31 von 42 Stimmen (73.8%)
* Josha von Gizycki: 25 von 42 Stimmen (59.5%)
* René Stegmaier (reneger): 15 von 42 Stimmen (35.7%)
* keine ungültigen Stimmen

Chrissi^ hat schon das Amt des stellv. Vorsitzenden angenommen, daher wird er hier
übersprungen.
Linda, fototeddy und lichtfeind werden in der BBB-Konferenz gefragt, ob sie die Wahl
annehmen wollen.
Alle drei nehmen die Wahl an.

### Wahl der der Geld verwaltenden Entität auf die Finger schauenden Entitäten (Rechnungsprüfer)
<!-- don't merge header and list -->
* Jan Lübbe (shoragan): 39 von 42 Stimmen (92.9%)
* Angela Uschok (sonnenschein): 39 von 42 Stimmen (92.9%)
* keine ungültigen Stimmen


TODO: haben die die Wahl angenommen?!?!?
