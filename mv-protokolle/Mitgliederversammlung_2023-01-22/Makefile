#!/usr/bin/make -f

# add destination files that should be built
OBJ = \
	protokoll-mv-2023-01-22.pdf

# add potential images and other sources
SRCS = \
	$(OBJ:pdf=md)

# filters that should be applied to the pandoc input
LUA_FILTERS = \
	../paragraph-headers.lua

#
# usually, nothing from here on needs to be changed.
#
TEMPLATE = pandoc-template.latex

pandoc = pandoc --template=$(TEMPLATE:.$(1)=) $(foreach f,$(LUA_FILTERS),--lua-filter $(f) ) -s -f markdown+grid_tables -i $< -s -t $(1) -o $(2)
pdflatex = xelatex -interaction=nonstopmode $<

all: pdf latex

forever:
	@echo Re-building objects whenever they change on disk.
	@echo Press Ctrl-C to quit.
	@echo -------------------------------------------------
	@echo
	ls -1 $(SRCS) | entr make --no-print-directory $(OBJ)

s0minutes.cls s0artcl.cls: ../../.gitmodules
	if [ ! -h stratum0-latex ]; then \
		git submodule update --init ; \
		ln -s ../stratum0-latex/ . ; \
	fi
	${MAKE} -C stratum0-latex
	if [ ! -h s0artcl.cls   ]; then ln -s stratum0-latex/s0artcl.cls   . ; fi
	if [ ! -h s0minutes.cls ]; then ln -s stratum0-latex/s0minutes.cls . ; fi
	touch stratum0-latex/s0artcl.cls stratum0-latex/s0minutes.cls

pdf: $(OBJ)
%.pdf: %.latex s0minutes.cls s0artcl.cls $(SRCS)
	$(call pdflatex) \
	&& $(call pdflatex) \
	&& $(call pdflatex)

.PRECIOUS: %.latex
latex: $(OBJ:pdf=latex)
%.latex: %.md $(TEMPLATE) $(FILTERS) $(SRCS)
	$(call pandoc, latex, $@) --pdf-engine=xelatex
latex.ast: $(OBJ:pdf=latex.ast)
%.latex.ast: %.md $(TEMPLATE) $(FILTERS)
	$(call pandoc, json, -) | jq . > $@

clean:
	rm -f $(OBJ) $(OBJ:pdf=latex) $(OBJ:pdf=aux) $(OBJ:pdf=log) $(OBJ:pdf=out) $(OBJ:pdf=toc)
	rm -f s0artcl.cls s0minutes.cls
